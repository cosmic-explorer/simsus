function[H1, H2]=generateCF(W1,W2,args)
 %createWeight
%
 %Syntax:[H1,H2]=generateCF(W1,W2,args)
 %
 %Inputs:
 %-W1-WeightingFunctionforH1
 %-W2-WeightingFunctionforH2
 %-args:
 %-method-H-Infinitysolver('lmi' or 'ric')
 %-display-Displaysynthesisresults('on'or 'off')
 %
 %Outputs:
 %-H1-GeneratedH1Filter
 %-H2-GeneratedH2Filter
 %%Argumentvalidation
 arguments
 W1
 W2
 args.method char {mustBeMember(args.method,{'lmi', 'ric'})}='ric'
 args.display char {mustBeMember(args.display,{'on', 'off'})}='on'
 end
 %%Thegeneralizedplantisdefined
 P=[W1-W1;
 0 W2;
 1 0];
 %%ThestandardH-infinitysynthesisisperformed
 [H2,~,gamma,~]=hinfsyn(P,1,1,'TOLGAM',0.001,'METHOD',args.method, 'DISPLAY',args.display);
 %%H1isdefinedasthecomplementaryofH2
 H1=1-H2;