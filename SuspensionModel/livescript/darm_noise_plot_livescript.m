if hold_status ==1
    hold on
else
    hold off
end



if exist('last_t')
else
    last_t = 'firstdef';
end
if exist('last_d')
else
    last_d = 'firstdef';
end

if exist('last_source')
else
    last_source = 'firstdef';
end
if exist('last_darm')
else
    last_darm = 'firstdef';
end





if ismember(input_plot_noise_theta,{'None'})
elseif eval(input_plot_noise_theta)==0
elseif ismember(input_plot_noise_theta,last_t)
else    
plot_noise_label(eval(input_plot_noise_theta),input_plot_noise_theta)
grid on;
legend()
end


if ismember(input_plot_noise_d,{'None'})
elseif eval(input_plot_noise_d)==0
elseif ismember(input_plot_noise_d,last_d)
else    
plot_noise_label(eval(input_plot_noise_d),input_plot_noise_d)
grid on;
legend()
end



if ismember(input_plot_noise_source,{'None'})
elseif eval(input_plot_noise_source)==0
elseif ismember(input_plot_noise_source,last_source)
else    
plot_noise_label(eval(input_plot_noise_source),input_plot_noise_source)
grid on;
legend()
end

if ismember(input_plot_noise_darm,{'None'})
elseif eval(input_plot_noise_darm)==0
elseif ismember(input_plot_noise_darm,last_darm)
else    
plot_noise_label(eval(input_plot_noise_darm),input_plot_noise_darm)
grid on;
legend()
end



last_t = input_plot_noise_theta;
last_d = input_plot_noise_d;
last_source = input_plot_noise_source;
last_darm = input_plot_noise_darm;
