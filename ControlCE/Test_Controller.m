clc;close all;
%con = zpk(-[pair(5,80) pair(5,80) pair(5,80) pair(130,10) pair(130,10) pair(130,10) pair(130,10)],-[0 0 0 0 pair(20,80) pair(20,80) pair(20,80) pair(20,80)],1);
%con = zpk(-[pair(0.5,10) pair(0.5,10) pair(10,10) pair(10,10) pair(10,10) pair(10,10) pair(10,10) pair(10,80) pair(10,80) pair(400,10) ],-[0 0 0 pair(15,10) pair(15,10) pair(15,10) pair(15,30) pair(15,30) pair(15,30) pair(15,30) pair(15,10) pair(15,10) pair(15,10)],1);
ch=zpk(-[pair(2,80) pair(15,50) pair(15,50) pair(300,15) ],-[ 5 pair(25,55) pair(25,55) pair(25,55) pair(25,75) pair(25,75) pair(25,75)],1);
%ch = zpk(-[],-[pair(15,15) pair(15,15) pair(15,15)],1);
cs = 1e4*zpk(-[pair(0.5,45) pair(1,75)],-[0.2 0.2 0.8 0.8  pair(15,75)],1);
%ch = 1;
ps = minreal(Hglobsh(23,29));
ph = minreal(Hglobsh(47,59));
OPs = cs*ps;
OPh = ch*ph;
cofs = 0.5;
cofh = 18;
cogs = 1;%abs(freqresp(OPs,cofs));
cogh = abs(freqresp(OPh,cofh));
OPs=OPs/cogs;
OPh=OPh/cogh;
cs = cs/cogs;
ch = ch/cogh;

figure(1)
margin(OPs)
hold on
bode(cs,ps);
legend('OPs','con','plant')
xlim([0.01,200])
xline(2*pi*10)
xline(2*pi*30)

figure(2)
margin(OPh)
hold on
bode(ch,ph);
legend('OPh','con','plant')

xline(2*pi*10)
xline(2*pi*30)
xlim([0.01,200])