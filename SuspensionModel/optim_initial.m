function darm = optim_initial()
%tic
%close all%;clc;
load('LP.mat');
load('BP.mat');
load('HP.mat');
load('Gglob.mat');
load('BHQS.mat')
load('Cloc.mat')
load("some_var.mat")
load('Lsc_var.mat')

%[darm1,darm2,darm]
%[alpha,beta,M1,M2,M3]= deal(x(1),x(2),x(3),x(4),x(5));
%[nu1,nu2,phi1,phi2,mu1,mu2,psi1,psi2,gh,nus1,nus2,phis1,phis2,mus1,mus2,psis1,psis2,gs]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10),x(11),x(12),x(13),x(14),x(15),x(16),x(17),x(18));
%disp(Ktop);
%K=0;
%Mtot=300;
%bhqs = BHQS();
%Kopt_eig = bhqs.Kopt;
%S = bhqs.S;

%Ktop=0;


%LOCAL CONTROLER
%run('Clocal.m')
%Cloc = Clocal(Gglob);
%run("createcomplementaryfilters.m");
%ASC CONTROLER




Hglobsh = Sp2*Hglob2*Sp1inv;

run('ascP_init.m');




%CAVITY MODEL 




%[A,B,C,D]=get_SSBHQS_M(bhqs,100,100,100,100);



% FULL SYSTEM

Hglob = feedback(Gglob,Kopt+Cloc+Clsc*M+CascP);%inv(I48+Gglob*(Kopt+Cloc+Clsc*M+CascP))*Gglob;

Hglobfsh = Sp2*Hglob*Sp1inv;
%CONTROL_OPT Summary of this function goes here
%   Detailed explanation goes here

run('get_TF.m')
run('freq.m')
run('ISI_inputs.m')
run('Noise_inputs.m')
run('load_inputs.m')

%Execute Noise_inputs.m


%[GP02Ptop,GF2Ptop] = closeloop(HP02Ptop,HF2Ptop,Ctop);
%[GPtop2Puim,GF2Puim] = closeloop(HPtop2Puim,HF2Puim,Cuim);


%Cavity tf from angle without cavity to with cavity

%HF2Ptst_mat = HF2Ptst*eye(2); 
%Kdamphs = [[KdampS,0];[0,KdampH]];


%Hcav = minreal(zpk(inv((eye(2)+HF2Ptst*inv(S)*(Kopt_eig+Kdamphs)*S))));

%H_ISIi_i = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,1));%+Hcav(1,2));%+Hcav(2,1));
%H_ISIi_e = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,1));%+Hcav(2,2));

%H_ISIe_i = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,2));%+Hcav(1,2));%+Hcav(2,1));
%H_ISIe_e = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,2));

%H_n_topi_i = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,1));%+Hcav(1,2));
%H_n_topi_e = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,1));%+Hcav(2,2));

%H_n_tope_i = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,2));%+Hcav(1,2));
%H_n_tope_e = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,2));%+Hcav(2,2));

%H_n_uimi_i = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(1,1));%+Hcav(1,2));
%H_n_uimi_e = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(2,1));%+Hcav(2,2));

%H_n_uime_i = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(1,2));%+Hcav(1,2));
%H_n_uime_e = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(2,2));%+Hcav(2,2));

%S_theta_i = sqrt(multiply(Hii,w,d_ISIP).^2+multiply(Hei,w,d_ISIP).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn1ei,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hn2ei,w,N_shot).^2);
%S_theta_e = sqrt(multiply(Hie,w,d_ISIP).^2+multiply(Hee,w,d_ISIP).^2+multiply(Hn1ie,w,N_shot).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ie,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2);


%S_d_i =abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*S_theta_i.^2+S_theta_e.^2) ;
%S_d_e =abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(S_theta_i.^2+bhqs.gi^2*S_theta_e.^2) ;
%S_theta_e = sqrt(multiply(H_ISI_e,w,d_ISIP).^2+multiply(H_n_top_s,w,N_shot).^2+multiply(H_n_uim_s,w,N_shot).^2);
%S_theta_e = sqrt(multiply(H_ISIi_e,w,d_ISIP).^2+multiply(H_ISIe_e,w,d_ISIP).^2+multiply(H_n_topi_e,w,N_shot).^2+multiply(H_n_tope_e,w,N_shot).^2+multiply(H_n_uimi_e,w,N_shot).^2+multiply(H_n_uime_e,w,N_shot).^2);
%theta_i_RMS = RMS(S_theta_i);
%theta_e_RMS = RMS(S_theta_e);

%d_i_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_RMS^2+theta_e_RMS^2);
%d_e_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_e_RMS^2+bhqs.gi^2*theta_e_RMS^2);


%theta_i_RMS = RMS([multiply(Hii,w,d_ISIP);multiply(Hei,w,d_ISIP);multiply(Hn1ii,w,N_shot);multiply(Hn1ei,w,N_shot);multiply(Hn2ii,w,N_shot);multiply(Hn2ei,w,N_shot)],f,.1,50);
%theta_e_RMS = RMS([multiply(Hie,w,d_ISIP);multiply(Hee,w,d_ISIP);multiply(Hn1ie,w,N_shot);multiply(Hn1ee,w,N_shot);multiply(Hn2ie,w,N_shot);multiply(Hn2ee,w,N_shot)],f,.1,50);

%darm = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi)*((bhqs.gi*bhqs.R^2+2*bhqs.R+bhqs.ge))*theta_s_RMS*S_theta_s+abs((bhqs.ge*bhqs.R^2-2*bhqs.R+bhqs.gi))*theta_h_RMS*S_theta_h);
%darm = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*((abs(bhqs.ge))*theta_i_RMS*S_theta_i+abs(bhqs.gi)*theta_e_RMS*S_theta_e);
%darm1 = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*((abs(bhqs.ge))*theta_i_RMS*S_theta_i);
%darm2 = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*abs(bhqs.gi)*theta_e_RMS*S_theta_e;
%darm = sqrt(2*(d_i_RMS^2*S_theta_i.^2+d_e_RMS^2*S_theta_e.^2));
%S_P = NoiseBudgetP(Hglob);


run('NoiseBudgetP.m');

%run('NoiseBudgetL0.m')

darm = S_P;%sqrt(S_P.^2+S_L0.^2);
%darm_RMS = -RMS_log(darm,f,10,30);
h_stable = pole(minreal(Hglobfsh(47,59),[],false));
s_stable = pole(minreal(Hglobfsh(23,29),[],false));

unstable_h_index = h_stable > 0;
unstable_h = h_stable(unstable_h_index);


unstable_s_index = s_stable > 0;
unstable_s = s_stable(unstable_s_index);

stability_penalty = (sum(unstable_s)+sum(unstable_h))*1e4;

[fr,z,q]=damp(minreal(Hglobfsh(47,59),[],false));

Qm = 1./(2*z)-30;

Qm_index = Qm > 0;
Qm_large = Qm(Qm_index);

darm_RMS = -RMS_log(darm,f,10,30)+stability_penalty%+sum(Qm_large);
%toc
%disp('DARM= '+string(darm_RMS));
%disp('time elapsed '+string(toc));

%disp(Ktop);
%disp(darm_RMS);
%res_top_ISI = multiply(GL02Ltop,w,d_ISIL);
%res_noise_top = multiply(Ktop*GF2Ltop,w,N_shot);

%res_top = res_top_ISI+res_noise_top;

%darmL = multiply(HLtop2Ltst,w,res_top+res_noise);
%darm_RMS = -RMS_log(darmL,f,10,30) ;


%toc
end