function [Gx2x,GF2x] = closeloop(Hx2x,HF2x,Controller)
%FIRSTSTAGE Summary of this function goes here


%Execute Noise_inputs.m

%   Detailed explanation goes here


K = CreateController(Controller.parameters,Controller.type);

Gx2x = Hx2x/(1+K*HF2x);
GF2x = HF2x/(1+K*HF2x);



end

