function CtopL = localdampController(G0)



[f,~,~] = damp(G0);

fm = 16.5;%f(end);
f1 = f(1);
Cop = G0*Controller([1,10,fm*1.3,f1],'leadcut').K;
g_cross = abs(freqresp(Cop,fm));
K = 1/g_cross;

CtopL = Controller([K*5,10,fm*1.3,f1],'leadcut').K;



end

