figure();
hold on

load('SS_BHQS\A_star_dic_ap01.mat')
load('SS_BHQS\B_star_dic_ap01.mat')
M10 = 117;
M20 = 83;
M30 = 100;
M40 = 100;
bhqs = BHQS(1.5e6,0,0,0,0);
Glow = [];
for i=1:50



Gperfect = minreal(ss(A,B,C,D),[],false);
A_res = A_star_dic_ap{1,1};
B_res = B_star_dic_ap{1,1};

fields_A = fieldnames(A_res);
fields_B = fieldnames(B_res);


fieldName_A = fields_A{i}; %%%TAKE THE FIRST non perfect suspension generated
    A_res_i = A_res.(fieldName_A);
    fieldName_B = fields_B{i};
    B_res_i = B_res.(fieldName_B);
    A_res_full = zeros(48, 48);
    A_res_full(25:48, 1:24) = A_res_i;
    B_res_full = zeros(48, 30);% Lower left block of A is A_star_BHQS
    B_res_full(25:30,1:6) = B_res_i(1:6,1:6).';
    %B_star(1:6,1:6)
    [A,B,C,D]=get_SSBHQS_M(model,M10,M20,300-M10-M20);
    G = minreal(ss(A+A_res_full,B+B_res_full,C,D),[],false);
bode(G(23,1));
Glow1 = freqresp(G(23,1),1e-2);
Glow = [Glow,Glow1];
end