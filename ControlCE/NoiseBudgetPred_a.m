%function S_P = NoiseBudgetP(Hglob)
%run('Clocal.m');
%tic
p0i = zeros(20,1);
p0i(2) = 1;

p0e = zeros(20,1);
p0e(12) = 1;

%%particular TF

%Effect on i
%tic
L = Hglob2red(15,19)*ClscMred(19,15);
Lf = minreal(L/(1+L),[],false);
Hi = Hglobred*p0i*Lf;
He = Hglobred*p0e*Lf;
Hii = minreal(Hi(8),1e-12,false);
Hei = minreal(He(8),1e-12,false);
%toc
n1i = zeros(16,1);
n1i(2) = 1;


n1e = zeros(16,1);
n1e(10) = 1;


Hn = Hglobred*Clocred*Lf;

nASCs = zeros(16,1);
nASCs(8) = 1;
nASCh = zeros(16,1);
nASCh(16) = 1;
%tic
HnASC = Hglobred*CascPred*Sp2invred*Lf;

HnASCs = HnASC*nASCs;
%toc
%HnASCsi = minreal(HnASC(23,23),[],false);

%HnASCse = minreal(HnASC(47,23),[],false);
%HnASChi = minreal(HnASC(23,47),[],false);
%HnASChe = minreal(HnASC(47,47),[],false);
%tic
HnASCsi = minreal(HnASCs(8),[],false);

HnASCse = minreal(HnASCs(16),[],false);

HnASCh = HnASC*nASCh;
%toc

%tic
HnASChi = minreal(HnASCh(8),[],false);
HnASChe = minreal(HnASCh(16),[],false);
%toc
Hn1i = Hn*n1i;

Hn1e = Hn*n1e;

 
%tic
Hn1ii = minreal(Hn1i(8),1e-12,false);%,[],false);
Hn1ei = minreal(Hn1e(8),1e-12,false);%,[],false);
%toc

n2i = zeros(16,1);
n2i(4) = 1;


n2e = zeros(16,1);
n2e(14) = 1;

Hn2i = Hn*n2i;
Hn2e = Hn*n2e;
%tic
Hn2ii = minreal(Hn2i(8),[],false);
Hn2ei = minreal(Hn2e(8),[],false);
%toc
%effect on e

%tic
Hie = minreal(Hi(16),1e-12,false);
Hee = minreal(He(16),1e-12,false);

Hn1ie = minreal(Hn1i(16),1e-12,false);%,[],false);
Hn1ee = minreal(Hn1e(16),1e-12,false);%,[],false);

Hn2ie = minreal(Hn2i(16),[],false);
Hn2ee = minreal(Hn2e(16),[],false);
%toc
%tic
S_Pisii=multiply(Hii,w,d_ISIP);

S_Pisei=multiply(Hei,w,d_ISIP);

S_Pnii=multiply(Hn1ii,w,N_shot);

S_Pnei=multiply(Hn1ei,w,N_shot);


S_Pascsi=multiply(HnASCsi,w,N_ASC);
S_Paschi=multiply(HnASChi,w,N_ASC);

S_Pascse=multiply(HnASCse,w,N_ASC);
S_Pasche=multiply(HnASChe,w,N_ASC);
%toc
%tic
S_theta_i = sqrt(multiply(Hii,w,d_ISIP).^2+multiply(Hei,w,d_ISIP).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn1ei,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hn2ei,w,N_shot).^2+multiply(HnASCsi,w,N_ASC).^2+multiply(HnASChi,w,N_ASC).^2);



S_theta_e = sqrt(multiply(Hie,w,d_ISIP).^2+multiply(Hee,w,d_ISIP).^2+multiply(Hn1ie,w,N_shot).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ie,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2+multiply(HnASCse,w,N_ASC).^2+multiply(HnASChe,w,N_ASC).^2);
%toc
%tic
theta_i_RMS = RMS(S_theta_i,f,0.001,300);
theta_e_RMS = RMS(S_theta_e,f,0.001,300);
%toc
%tic
d_i_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_RMS^2+theta_i_RMS^2)+0.001;
d_e_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_RMS^2+bhqs.gi^2*theta_e_RMS^2)+0.001;

S_P = sqrt(2*(d_i_RMS^2*S_theta_i.^2+d_e_RMS^2*S_theta_e.^2));
%toc
%toc

%a=a;

%end

