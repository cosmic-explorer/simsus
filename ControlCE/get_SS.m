function [A, B, C, D] = get_SS(M1, M2, M3, M4, Ixx1, Iyy1, Izz1, Ixx2, Iyy2, Izz2, Ixx3, Iyy3, Izz3, Ixx4, Iyy4, Izz4, L1, L2,L3,L4, K1,K2,K3,K4,n1,n3,n5,n7,w1,w3,w5,w7)
g= 9.81;
M1234 = M1+M2+M3+M4;
M234 = M2+M3+M4;
M34 = M3+M4;

% Calculate forces Fx1, Fx2, Fx3, Fx4
Fx1 = g/L1 * M1234;
Fx2 = g/L2 * M234;
Fx3 = g/L3 * M34;
Fx4 = g/L4 * M4;

% Calculate forces Fy1, Fy2, Fy3, Fy4
Fy1 = g/L1 * M1234;
Fy2 = g/L2 * M234;
Fy3 = g/L3 * M34;
Fy4 = g/L4 * M4;

% Calculate forces Fz1, Fz2, Fz3, Fz4
Fz1 = 4 * K1;
Fz2 = 4 * K2;
Fz3 = 4 * K3;
Fz4 = 4 * K4;

% Calculate torques Tx1, Tx2, Tx3, Tx4
Tx1 = 4 * K1 * (n1^2);
Tx2 = 4 * K2 * (n3^2);
Tx3 = 4 * K3 * (n5^2);
Tx4 = 4 * K4 * (n7^2);

% Calculate torques Ty1, Ty2, Ty3, Ty4
Ty1 = 4 * K1 * (w1^2);
Ty2 = 4 * K2 * (w3^2);
Ty3 = 4 * K3 * (w5^2);
Ty4 = 4 * K4 * (w7^2);

% Calculate torques Tz1, Tz2, Tz3, Tz4
Tz1 = M1234 * g * (n1^2 + w1^2) / L1;
Tz2 = M234 * g * (n3^2 + w3^2) / L2;
Tz3 = M34 * g * (n5^2 + w5^2) / L3;
Tz4 = M4 * g * (n7^2 + w7^2) / L4;


A_star = zeros(24, 24);

% Mass1
A_star(1, 1) = -Fx1/M1 - Fx2/M1;
A_star(1, 7) = Fx2/M1;

A_star(2, 2) = -Fy1/M1 - Fy2/M1;
A_star(2, 8) = Fy2/M1;

A_star(3, 3) = -Fz1/M1 - Fz2/M1;
A_star(3, 9) = Fz2/M1;

A_star(4, 4) = -Tx1/Ixx1 - Tx2/Ixx1;
A_star(4, 10) = Tx2/Ixx1;

A_star(5, 5) = -Ty1/Iyy1 - Ty2/Iyy1;
A_star(5, 11) = Ty2/Iyy1;

A_star(6, 6) = -Tz1/Izz1 - Tz2/Izz1;
A_star(6, 12) = Tz2/Izz1;


% Mass2
A_star(7, 1) = Fx2/M2;
A_star(7, 7) = -Fx2/M2 - Fx3/M2;
A_star(7, 13) = Fx3/M2;

A_star(8, 2) = Fy2/M2;
A_star(8, 8) = -Fy2/M2 - Fy3/M2;
A_star(8, 14) = Fy3/M2;

A_star(9, 3) = Fz2/M2;
A_star(9, 9) = -Fz2/M2 - Fz3/M2;
A_star(9, 15) = Fz3/M2;

A_star(10, 4) = Tx2/Ixx2;
A_star(10, 10) = -Tx2/Ixx2 - Tx3/Ixx2;
A_star(10, 16) = Tx3/Ixx2;

A_star(11, 5) = Ty2/Iyy2;
A_star(11, 11) = -Ty2/Iyy2 - Ty3/Iyy2;
A_star(11, 17) = Ty3/Iyy2;

A_star(12, 6) = Tz2/Izz2;
A_star(12, 12) = -Tz2/Izz2 - Tz3/Izz2;
A_star(12, 18) = Tz3/Izz2;


% Mass3
A_star(13, 7) = Fx3/M3;
A_star(13, 13) = -Fx3/M3 - Fx4/M3;
A_star(13, 19) = Fx4/M3;

A_star(14, 8) = Fy3/M3;
A_star(14, 14) = -Fy3/M3 - Fy4/M3;
A_star(14, 20) = Fy4/M3;

A_star(15, 9) = Fz3/M3;
A_star(15, 15) = -Fz3/M3 - Fz4/M3;
A_star(15, 21) = Fz4/M3;

A_star(16, 10) = Tx3/Ixx3;
A_star(16, 16) = -Tx3/Ixx3 - Tx4/Ixx3;
A_star(16, 22) = Tx4/Ixx3;

A_star(17, 11) = Ty3/Iyy3;
A_star(17, 17) = -Ty3/Iyy3 - Ty4/Iyy3;
A_star(17, 23) = Ty4/Iyy3;

A_star(18, 12) = Tz3/Izz3;
A_star(18, 18) = -Tz3/Izz3 - Tz4/Izz3;
A_star(18, 24) = Tz4/Izz3;


% Mass4
A_star(19, 13) = Fx4/M4;
A_star(19, 19) = -Fx4/M4;

A_star(20, 14) = Fy4/M4;
A_star(20, 20) = -Fy4/M4;

A_star(21, 15) = Fz4/M4;
A_star(21, 21) = -Fz4/M4;

A_star(22, 16) = Tx4/Ixx4;
A_star(22, 22) = -Tx4/Ixx4;

A_star(23, 17) = Ty4/Iyy4;
A_star(23, 23) = -Ty4/Iyy4;

A_star(24, 18) = Tz4/Izz4;
A_star(24, 24) = -Tz4/Izz4;


%add the missalignment

% Initialize B_star matrix
B_star = zeros(24, 30);

% Ground Input
B_star(1, 1) = Fx1/M1;
B_star(2, 2) = Fy1/M1;
B_star(3, 3) = Fz1/M1;
B_star(4, 4) = Tx1/Ixx1;
B_star(5, 5) = Ty1/Iyy1;
B_star(6, 6) = Tz1/Izz1;

% Force input on mass 1
B_star(1, 7) = 1/M1;
B_star(2, 8) = 1/M1;
B_star(3, 9) = 1/M1;
B_star(4, 10) = 1/Ixx1;
B_star(5, 11) = 1/Iyy1;
B_star(6, 12) = 1/Izz1;

% Force Input on mass 2
B_star(7, 13) = 1/M2;
B_star(8, 14) = 1/M2;
B_star(9, 15) = 1/M2;
B_star(10, 16) = 1/Ixx2;
B_star(11, 17) = 1/Iyy2;
B_star(12, 18) = 1/Izz2;

% Force Input on mass 3
B_star(13, 19) = 1/M3;
B_star(14, 20) = 1/M3;
B_star(15, 21) = 1/M3;
B_star(16, 22) = 1/Ixx3;
B_star(17, 23) = 1/Iyy3;
B_star(18, 24) = 1/Izz3;

% Force Input on mass 4
B_star(19, 25) = 1/M4;
B_star(20, 26) = 1/M4;
B_star(21, 27) = 1/M4;
B_star(22, 28) = 1/Ixx4;
B_star(23, 29) = 1/Iyy4;
B_star(24, 30) = 1/Izz4;

%load('SS_BHQS/B_star_dic_bs');
%B_star_perf = B_star;
%B_star(1:6,1:6) = B_star(1:6,1:6)+A_star_1(1:6,1:6);



% Assuming necessary variables and functions are defined
% Initialize matrices
A = zeros(48, 48);  % A is a 48x48 matrix
B = zeros(48, 30);  % B is a 48x30 matrix
C = zeros(24, 48);  % C is a 24x48 matrix
D = zeros(24, 30);  % D is a 24x30 matrix

% Construct matrices A, B, C, and D
A(1:24, 25:48) = eye(24);% Upper right block of A is the identity matrix

A(25:48, 25:48) = -1e-6*eye(24); %Quality factors
A(22,22) = -1;
A(46,46) = -1;

%A(21,21) = -1;
%A(45,45) = -1;


A(25:48, 1:24) = A_star;  % Lower left block of A is A_star_BHQS

B(25:48, 1:30) = B_star;  % Lower part of B is B_star_BHQS

C(1:24, 1:24) = eye(24);  % C is the identity matrix

% D matrix remains zeros, as per the provided Python code

%A_perf = zeros(48, 48);  % A is a 48x48 matrix
%B_perf = zeros(48, 30);  % B is a 48x30 matrix
%C_perf = zeros(24, 48);  % C is a 24x48 matrix
%D_perf = zeros(24, 30);  % D is a 24x30 matrix
%A_perf(1:24, 25:48) = eye(24);% Upper right block of A is the identity matrix

%A_perf(25:48, 25:48) = -1e-6*eye(24); %Quality factors


%A_perf(25:48, 1:24) = A_star_perf;  % Lower left block of A is A_star_BHQS

%B_perf(25:48, 1:30) = B_star_perf;  % Lower part of B is B_star_BHQS

%C_perf(1:24, 1:24) = eye(24);  % C is the identity matrix



end
