%n_shot = [4.2e-11,4.2e-11]; %SHOT NOISE
%freq_shot = [0.001,1000];
freq_shot= [0.01   0.1    0.5    1         10     100      1000    10000]; %BOSEMs
n_shot  = [5e-7   1e-8   3e-10  1.5e-10   6e-11  6e-11   6e-11    6e-11];

n_ASC = [1e-13,1e-13];
freq_ASC = [0.001,1000];