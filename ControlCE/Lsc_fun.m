function [Clsc,M] = Lsc_fun(Fglob)
load('Tst2TstCont.mat');
C = cont;%Controller([100,10,15],'leadcut').K;
B = Fglob(43,55)*C;
C = C/abs(freqresp(B,2*pi*100));
%LPF
%BPF
%HPF
%run("createcomplementaryfilters.m");
%run('generic_blend.m');
%run('three_complementary_filters.m')
M = zeros(48,48);
M(43,19) = -1;
M(43,43)=1;

K3 = 1;

[f,~,~] = damp(minreal(Fglob(23,29),[],false));

fm = f(end);


A3 = minreal(ss(Fglob(19,25)),[],false);
A2 = minreal(ss(Fglob(19,19)),[],false);
A1 = minreal(ss(Fglob(19,13)),[],false);


%K2 = Kasc(fm,2);
%K1 = Kasc(fm,4);
K1 = K3*A3/A1;
K2 = K3*A3/A2;
Clsc = ss(zeros(60,48));
%Cascp(25,19) = K3*C*HPF;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%Cascp(19,19) = K2*C*BPF;
%Cascp(13,19) = K1*C*LPF;

Clsc(55,43) = C;
%Clsc(49,43) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%Clsc(49,43) = K2*C*BP;
%Clsc(43,43) = K1*C*LP;
