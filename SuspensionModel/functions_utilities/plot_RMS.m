function plot_RMS(x)
%run('ISI_inputs.m')
%run('Noise_inputs.m')
%Execute Noise_inputs.m
run('freq.m')
%run('load_inputs.m')
%bhqs = BHQS();
%Mtot=300;
%[A,B,C,D]=get_SSBHQS_M(bhqs,x(3),x(4),Mtot-x(3)-x(4));
%G = ss(A,B,C,D);
%run('get_TF.m');
%Ktop =x(1);
%K = x(1);
%K=0;
%Tau = x(2);
%Ktop = K*tf([10*Tau,1],[Tau, 1]);

%figure()
%bode(HL02Ltop)

%HLtop2Ltst2= HLtop2Ltst*HL02Ltop/(1+Ktop*HF2Ltop);

%res_top = multiply(HP02Ptop/(1+Ktop*HF2Ptop),w,d_ISIP);
%res_noise = multiply(Ktop*HF2Ptop/(1+Ktop*HF2Ptop),w,N_shot);
%Gclose = HP02Ptop/(1+Ktop*HF2Ptop);
%n_darm = multiply(HPtop2Ptst,w,res_top);
%n_darm2 = multiply(HPtop2Ptst,w,res_noise);
%n = multiply(HP02Ptop,w,d_ISIP);

%res_top = multiply(HL02Ltop/(1+Ktop*HF2Ltop),w,d_ISIL);
%res_noise = multiply(Ktop*HF2Ltop/(1+Ktop*HF2Ltop),w,N_shot);
%Gclose = HL02Ltop/(1+Ktop*HF2Ltop);
%n_darm = multiply(HLtop2Ltst,w,res_top);
%n_darm2 = multiply(HLtop2Ltst,w,res_noise);
%n = multiply(HL02Ltop,w,d_ISIL)

%lnk = linspace(0, 30, 1000);
%r = zeros(1, 2000);

%for i = 1:1000
%    r(1000+i) = control_opt(exp(lnk(i)), HL2L, HF2L, d_ISIL,  w, f, N_shot,HLtop2Ltst);
%    r(i) = control_opt(-exp(lnk(i)), HL2L, HF2L, d_ISIL,  w, f, N_shot,HLtop2Ltst);
%end

%figure()

%plot([-lnk,lnk],r)




%plot(f,abs(h))

%plot(f,abs(h_cls))
%plot(f,d_ISIL)
%hold on
%plot(f,res_noise)
%hold on
%plot(f,n_darm,'DisplayName',strcat(label,' input ISI L'),LineWidth=1,Color=color,LineStyle='-.')
%plot(f,n_darm2,'DisplayName',strcat(label,' input Shot Noise'),LineWidth=1,LineStyle='--',Color=color)
%plot(f,n_darm2+n_darm,'DisplayName',strcat(label,' Total Budget'),LineWidth=2,Color=color)
%plot(f,n)
%legend
plot(f,x,'LineWidth',4,'LineStyle','--')
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
set(gca, 'FontSize', 25,'FontWeight','bold');
grid on
%res_top = multiply(HL2L/(1+Ktop*HF2L),w,d_ISIL);
%res_noise = multiply(Ktop*HF2L/(1+Ktop*HF2L),w,N_shot);
%darmL = multiply(HLtop2Ltst,w,res_top+res_noise);
%darm_RMS = -RMS_log(darmL,f,0.1,30) ;

end

