if strcmp('Cloc_L',Controller)
    input = 'eFx1';
    output = 'ex1';
elseif strcmp('Cloc_T',Controller)
    input = 'eFy1';
    output = 'ey1';
elseif strcmp('Cloc_V',Controller)
    input = 'eFz1';
    output = 'ez1';
elseif strcmp('Cloc_R',Controller)
    input = 'eMR1';
    output = 'eR1';
elseif strcmp('Cloc_P',Controller)
    input = 'eMP1';
    output = 'eP1';
elseif strcmp('Cloc_Y',Controller)
    input = 'eMY1';
    output = 'eY1';
elseif strcmp('CascP_soft',Controller)
    input = 'sMP4';
    output = 'sP4';
elseif strcmp('CascP_hard',Controller)
    input = 'hMP4';
    output = 'hP4';
elseif strcmp('CascY_soft',Controller)
    input = 'sMY4';
    output = 'sY4';
elseif strcmp('CascY_hard',Controller)
    input = 'hMY4';
    output = 'hY4';
elseif strcmp('Clsc',Controller)  
    input = 'eFx4';
    output = 'ex4';
end

disp(['<strong> input is </strong>',input,', <strong> output is </strong>', output])