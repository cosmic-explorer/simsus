%clearWorkspaceandClosefigures
clear;closeall;clc;

%IntializeLaplacevariable
s=zpk('s');

%Initialize FrequencyVector
freqs=logspace(-1,3,1000);

%%WeightingFunctionDesign
%Parameters
 n=3;w0=2*pi*10;G0=1e-3;G1=1e1;Gc=2;

%Formulas
 W=(((1/w0)*sqrt((1-(G0/Gc)^(2/n))/(1-(Gc/G1)^(2/n)))*s+(G0/Gc)^(1/n))/((1/G1)^(1/n)*(1/w0)*sqrt((1-(G0/Gc)^(2/n))/(1-(Gc/G1)^(2/n)))*s+(1/Gc)^(1/n)))^n;

%%Magnitudeoftheweightingfunctionwithparameters
figure;
holdon;
plot(freqs,abs(squeeze(freqresp(W,freqs, 'Hz'))),'k-');

plot([1e-3 1e0],[G0G0], 'k--', 'LineWidth',1)
text(1e0,G0, '$\quadG_0$')

plot([1e1 1e3],[G1G1], 'k--', 'LineWidth',1)
text(1e1,G1,'$G_{\infty}\quad$','HorizontalAlignment', 'right')

plot([w0/2/pi w0/2/pi],[12*Gc], 'k--', 'LineWidth',1)
text(w0/2/pi,1,'$\omega_c$','VerticalAlignment', 'top', 'HorizontalAlignment','center')

plot([w0/2/pi/2 2*w0/2/pi],[GcGc], 'k--', 'LineWidth',1)
text(w0/2/pi/2,Gc, '$G_c\quad$','HorizontalAlignment', 'right')

text(w0/5/pi/2,abs(evalfr(W,j*w0/5)), 'Slope:$n\quad$', 'HorizontalAlignment', 'right')

text(w0/2/pi,abs(evalfr(W,j*w0)), '$\bullet$', 'HorizontalAlignment', 'center')
set(gca,'XScale', 'log');set(gca, 'YScale', 'log');
xlabel('Frequency[Hz]');ylabel('Magnitude');
holdoff;
xlim([freqs(1),freqs(end)]);
ylim([5e-4,20]);
yticks([1e-4,1e-3,1e-2,1e-1,1,1e1]);

%%DesignoftheWeightingFunctions
W1=generateWF('n',3, 'w0',2*pi*10, 'G0',1000, 'Ginf',1/10, 'Gc',0.45);
W2=generateWF('n',2, 'w0',2*pi*10, 'G0',1/10, 'Ginf',1000, 'Gc',0.45);

%%Plotofthe Weightingfunctionmagnitude
figure;
tiledlayout(1, 1,'TileSpacing', 'None', 'Padding', 'None');
ax1=nexttile();
holdon;
set(gca,'ColorOrderIndex',1)
plot(freqs,1./abs(squeeze(freqresp(W1,freqs, 'Hz'))), '--', 'DisplayName', '$|W_1|^{-1}$');
set(gca,'ColorOrderIndex',2)
plot(freqs,1./abs(squeeze(freqresp(W2,freqs, 'Hz'))), '--', 'DisplayName', '$|W_2|^{-1}$');

et(gca, 'XScale', 'log');set(gca, 'YScale', 'log');
xlabel('Frequency[Hz]','FontSize',10);ylabel('Magnitude', 'FontSize',10);
holdoff;
xlim([freqs(1),freqs(end)]);
xticks([0.1,1,10,100,1000]);
ylim([8e-4,20]);
yticks([1e-3, 1e-2,1e-1,1,1e1]);
yticklabels({'', '$10^{-2}$', '', '$10^0$', ''});
ax1.FontSize=9;
leg=legend('location', 'south', 'FontSize',8);
leg.ItemTokenSize(1)=18;

%%GeneralizedPlant
P=[W1-W1;
0 W2;
1 0];

%%H-Infinity Synthesis
[H2,~,gamma,~]=hinfsyn(P,1,1,'TOLGAM',0.001,'METHOD', 'ric', 'DISPLAY', 'on');

%%DefineH1tobethecomplementaryofH2
H1=1-H2;

%%Bodeplotofthecomplementaryfilters
figure;
tiledlayout(3,1, 'TileSpacing', 'None', 'Padding', 'None');

%Magnitude
ax1=nexttile([2,1]);
holdon;
set(gca,'ColorOrderIndex',1)
plot(freqs,1./abs(squeeze(freqresp(W1,freqs, 'Hz'))),'--', 'DisplayName', '$|W_1|^{-1}$');
set(gca,'ColorOrderIndex',2)
plot(freqs,1./abs(squeeze(freqresp(W2,freqs, 'Hz'))),'--', 'DisplayName', '$|W_2|^{-1}$');

set(gca,'ColorOrderIndex',1)
plot(freqs,abs(squeeze(freqresp(H1,freqs, 'Hz'))), '-','DisplayName', '$H_1$');
set(gca,'ColorOrderIndex',2)
plot(freqs,abs(squeeze(freqresp(H2,freqs, 'Hz'))), '-','DisplayName', '$H_2$');
holdoff;
set(gca,'XScale','log');set(gca, 'YScale','log');
set(gca,'XTickLabel',[]);ylabel('Magnitude');
ylim([8e-4,20]);
 yticks([1e-3,1e-2,1e-1,1,1e1]);
 yticklabels({'', '$10^{-2}$', '', '$10^0$', ''})
 leg=legend('location', 'south','FontSize',8,'NumColumns',2);
 leg.ItemTokenSize(1)=18;

 %Phase
 ax2=nexttile;
 holdon;
 set(gca,'ColorOrderIndex',1)
 plot(freqs,180/pi*phase(squeeze(freqresp(H1,freqs, 'Hz'))),'-');
 set(gca,'ColorOrderIndex',2)
 plot(freqs,180/pi*phase(squeeze(freqresp(H2,freqs, 'Hz'))),'-');
 holdoff;
 set(gca,'XScale', 'log');
 xlabel('Frequency[Hz]');ylabel('Phase[deg]');
 yticks([-180:90:180]);
 ylim([-180,200])
 yticklabels({'-180','', '0', '','180'})

 linkaxes([ax1,ax2],'x');
 xlim([freqs(1),freqs(end)])