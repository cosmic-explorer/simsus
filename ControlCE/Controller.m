classdef Controller
    properties
        parameters
        type
        K
        
        
    end
    
    methods
        function obj = Controller(parameters,type)
            % Assign default values or ensure properties are initialized
            if nargin > 0
                obj.type = type;
                obj.parameters = parameters;
                obj.K = obj.calculateParameter(); % Calculate K using internal method
            else
                obj.type = '';
                obj.parameters = struct();
                obj.K = 0; % Initialize K with default value
            end
        end

        function K = calculateParameter(obj)
            % Call external function CreateController
            K = CreateController(obj.parameters, obj.type);
            %K = tf([obj.parameters(1)]);
        end
    end
end