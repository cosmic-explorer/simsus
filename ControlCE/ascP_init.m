%C =ss(cont);% Controller([100,10,15],'leadcut').K;
%load('controller.mat');
%load('cs.mat');
%load('ch.mat');
%C = con;
%B1 = Hglobsh(23,29)*C;
%B2 = Hglobsh(47,59)*C;
%C1 = cs;%/abs(freqresp(B1,2*pi*50));
%C2 = ch;%/abs(freqresp(B2,2*pi*50));
%ch = zpk(-2*pi*[pair(nu1,phi1) pair(nu2,phi2) ],-2*pi*[pair(mu1,psi1) pair(mu2,psi2)],gh);
ch=zpk(-[pair(2,80) pair(15,50) pair(15,50) pair(300,15) ],-[ 5 pair(25,55) pair(25,55) pair(25,55) pair(25,75) pair(25,75) pair(25,75)],1);
%ch = zpk(-[],-[pair(15,15) pair(15,15) pair(15,15)],1);
%cs = zpk(-2*pi*[pair(nus1,phis1) pair(nus2,phis2) ],-2*pi*[pair(mus1,psis1) pair(mus2,psis2)],gs);
cs = 1e4*zpk(-[pair(0.5,45) pair(1,75)],-[0.2 0.2 0.8 0.8  pair(15,75)],1);
%C1=C;
%C2=C;
%LPF
%BPF
%HPF

K3 = 1;

[f,~,~] = damp(minreal(Gglob(23,29),[],false));

fm = f(end);


CascP = ss(zeros(60,48));
KascPmini = inv(bhqs.Spitch)*[[cs,0];[0 ch]]*bhqs.Spitch; 
%CascP(29,23) = KascPmini(1,1);
%CascP(29,47) = KascPmini(1,2);
%CascP(59,23) = KascPmini(2,1);
%CascP(59,47) = KascPmini(2,2);

Cii = ss(KascPmini(1,1));
Cie = ss(KascPmini(1,2));
Cei = ss(KascPmini(2,1));
Cee = ss(KascPmini(2,2));

A3ii = Hglob2(23,29);
A2ii = Hglob2(23,23);
A1ii = Hglob2(23,17);

K1ii = zpk(A3ii/A1ii);%1/minreal(1/(K3*A3ii/A1ii),1e-12);
K2ii = zpk(A3ii/A2ii);%1/minreal(1/(K3*A3ii/A2ii),1e-12);

%A3ie = minreal(tf(Hglob2(47,29)));
%A2ie = minreal(tf(Hglob2(47,23)));
%A1ie = minreal(tf(Hglob2(47,17)));

%K1ie = K3*A3ie/A1ie;
%K2ie = K3*A3ie/A2ie;

%A3ee = minreal(tf(Hglob2(47,59)));
%A2ee = minreal(tf(Hglob2(47,53)));
%A1ee = minreal(tf(Hglob2(47,47)));

%K1ee = K3*A3ee/A1ee;
%K2ee = K3*A3ee/A2ee;


%A3ei = minreal(tf(Hglob2(23,59)));
%A2ei = minreal(tf(Hglob2(23,53)));
%A1ei = minreal(tf(Hglob2(23,47)));

%K1ei = K3*A3ei/A1ei;
%K2ei = K3*A3ei/A2ei;


%K2 = Kasc(fm,2);
%K1 = Kasc(fm,4);

%Cascp = tf(zeros(60,48));
CascP(29,23) = Cii;%*K3*HP;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%CascP(23,23) = Cii*K2ii*BP;
%CascP(17,23) = Cii*K1ii*LP;

CascP(29,47) = Cie;%*K3*HP;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%CascP(23,47) = Cie*K2ie*BP;
%CascP(17,47) = Cie*K1ie*LP;

CascP(59,47) = Cee;%*HP;
%Cascp(53,47) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%CascP(53,47) = K2ee*Cee*BP;
%CascP(47,47) = K1ee*Cee*LP;

CascP(59,23) = Cei;%*HP;
%Cascp(53,47) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%CascP(53,23) = K2ei*Cei*BP;
%CascP(47,23) = K1ei*Cei*LP;