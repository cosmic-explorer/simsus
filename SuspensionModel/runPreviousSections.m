
scriptName = mfilename('main.mlx'); % Get full path of current script
    scriptName = strrep(scriptName, '.mlx', ''); % Remove .mlx extension
    
    % Read the script file
    fid = fopen([scriptName, '.m'], 'r');
    lines = textscan(fid, '%s', 'Delimiter', '\n');
    fclose(fid);
    
    % Prepare to execute lines up to the current section
    currentSection = find(contains(lines{1}, '%%'), 1, 'last'); % Find the last section marker
    runLines = lines{1}(1:currentSection-1); % Lines to execute
    
    % Combine the lines and evaluate
    eval(strjoin(runLines, newline));