function K = CreateController(inputs, controller_type)
%LOCALCONTROLER Summary of this function goes here
%   Detailed explanation goes here
if strcmp('lead',controller_type)
    K = inputs(1)*tf([inputs(2)*inputs(3),1],[inputs(3),1]);
elseif strcmp('leadcut',controller_type)
    
    K = inputs(1)*tf([inputs(2)/inputs(3)/5,1],[1/inputs(3),1])^2*zpk([0],-[pair(inputs(3),70) pair(inputs(3),70)],1)*zpk([pair(inputs(4)/2,75) pair(inputs(4)*2,75)],[pair(inputs(4),75) pair(inputs(4),75)],1);
    %K = inputs(1)*zpk([0 0],-[pair(inputs(3)*2,50) pair(inputs(3)*2,50)],1);
end

