function CascY = get_CascY(chY,csY,model)

CascY = ss(zeros(60,48));
        
KascPminiY = inv(model.Syaw)*ss([[csY,0];[0 chY]])*model.Syaw; 

Cii = zpk(KascPminiY(1,1));
Cie = zpk(KascPminiY(1,2));
Cei = zpk(KascPminiY(2,1));
Cee = zpk(KascPminiY(2,2));

CascY(30,24) = Cii;
CascY(30,48) = Cie;
CascY(60,48) = Cee;
CascY(60,24) = Cei;

end

