function result = multiply(TF,f, d_ISI)
     %mag0 =reshape(freqresp(prescale(TF,{0.01,10000}),f),1,length(f));% freqresp(TF); % Assuming freqresp computes magnitude in dB
   % mag0 = 10.^(mag0/20); % Convert dB magnitude to linear magnitude
    mag0 =reshape(freqresp(TF,f),1,length(f));
    % Calculate the result by element-wise multiplication
    result = abs(mag0) .* d_ISI;
end

