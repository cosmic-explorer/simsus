% Define the transfer function
%numerator = [1]; % Example numerator coefficients
%denominator = [1, 3, 3, 1]; % Example denominator coefficients
warning('off', 'all');
sys = Hglobfsh(47,59);
% Generate the Bode plot
[mag, phase, wout] = bode(sys);
mag = squeeze(mag); % Magnitude in absolute terms
phase = squeeze(phase); % Phase in degrees
wout = squeeze(wout); % Frequency in rad/s
% Convert magnitude to dB
magdB = 20 * log10(mag);
% Find the peak frequencies and magnitudes
[pks, locs] = findpeaks(magdB, 'MinPeakHeight',-20); % Example threshold
% Filter out peaks below 0.005 Hz
min_frequency = 0.005 * 2 * pi; % Convert Hz to rad/s
valid_peaks = wout(locs) > min_frequency;
locs = locs(valid_peaks);
pks = pks(valid_peaks);
% Initialize quality factor array
Q_factors = zeros(length(pks), 1);
% Calculate quality factors
for i = 1:length(pks)
    % Find the -3dB points around each peak
    half_power_point = pks(i) - 3;
    idx_left = find(magdB(1:locs(i)) <= half_power_point, 1, 'last');
    idx_right = find(magdB(locs(i):end) <= half_power_point, 1, 'first') + locs(i) - 1;
    if ~isempty(idx_left) && ~isempty(idx_right)
        bandwidth = wout(idx_right) - wout(idx_left);
        resonant_frequency = wout(locs(i));
        Q_factors(i) = resonant_frequency / bandwidth;
    else
        Q_factors(i) = NaN; % Handle cases where -3dB points are not found
    end
end
% Display the results
%disp('Resonant Frequencies (rad/s):');
%disp(wout(locs));
%disp('Quality Factors:');
%disp(Q_factors);