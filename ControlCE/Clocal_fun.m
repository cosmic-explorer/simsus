function Cloc = Clocal_fun(G,Gglob)
%function Cloc = Clocal(Gglob)
%CLOCAL Summary of this function goes here
%   Detailed explanation goes here

%Length

GL= minreal(G(1,7),[],false);
CtopL = localdampController(GL);

GT= minreal(G(2,8),[],false);
CtopT = localdampController(GT);

GV= minreal(G(3,9),[],false);
CtopV = localdampController(GV);

GR= minreal(G(4,10),[],false);
CtopR = localdampController(GR);

GP= minreal(G(5,11),[],false);
CtopP = localdampController(GP);

GY= minreal(G(6,12),[],false);
CtopY = localdampController(GY);

Cloc = ss(zeros(60,48));


%I

Cloc(7,1) = CtopL;
%Cloc(13,7) = CuimL;

Cloc(8,2) = CtopT;
%Cloc(14,8) = CuimT;

Cloc(9,3) = CtopV;
%Cloc(15,9) = CuimV;

Cloc(10,4) = CtopR;
%Cloc(16,10) = CuimR;

Cloc(11,5) = CtopP;
%Cloc(17,11) = CuimP;

Cloc(12,6) = CtopY;
%Cloc(18,12) = CuimY;


%E
Cloc(37,25) = CtopL;
%Cloc(37,31) = CuimL;

Cloc(38,26) = CtopT;
%Cloc(38,32) = CuimT;

Cloc(39,27) = CtopV;
%Cloc(39,33) = CuimV;

Cloc(40,28) = CtopR;
%Cloc(40,34) = CuimR;

Cloc(41,29) = CtopP;
%Cloc(41,35) = CuimP;

Cloc(42,30) = CtopY;
%Cloc(42,36) = CuimY;
%end



Gint = inv(eye(48)+Gglob*Cloc)*Gglob;

%GL= minreal(Gint(7,13));
%CuimL = localdampController(GL);

%GT= minreal(Gint(8,14));
%CuimT = localdampController(GT);

%GV= minreal(Gint(9,15));
%CuimV = localdampController(GV);

%GR= minreal(Gint(10,16));
%CuimR = localdampController(GR);

%GP= minreal(Gint(11,17));
%CuimP = localdampController(GP);

%GY= minreal(Gint(12,18));
%CuimY = localdampController(GY);

%Cloc(31,25) = CtopL;
%Cloc(31,37) = CuimL;

%Cloc(32,26) = CtopT;
%Cloc(32,38) = CuimT;

%Cloc(33,27) = CtopV;
%Cloc(33,39) = CuimV;

%Cloc(34,28) = CtopR;
%Cloc(34,40) = CuimR;

%Cloc(35,29) = CtopP;
%Cloc(35,41) = CuimP;

%Cloc(36,30) = CtopY;
%Cloc(36,42) = CuimY;

%CuimL = Controller([Kuim,10,Tauuim],'lead').K;

%CtopT = Controller([Ktop,10,Tautop],'lead').K;
%CuimT = Controller([Kuim,10,Tauuim],'lead').K;

%CtopV = Controller([Ktop,10,Tautop],'lead').K;
%CuimV = Controller([Kuim,10,Tauuim],'lead').K;

%CtopR = Controller([Ktop,10,Tautop],'lead').K;
%CuimR = Controller([Kuim,10,Tauuim],'lead').K;

%CtopP = Controller([Ktop,10,Tautop],'lead').K;
%CuimP = Controller([Kuim,10,Tauuim],'lead').K;

%CtopY = Controller([Ktop,10,Tautop],'lead').K;
%CuimY = Controller([Kuim,10,Tauuim],'lead').K;


%GLobal Hard Controler

end

