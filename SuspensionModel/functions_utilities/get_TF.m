
HL02Ltop = tf(G(1,1));
HF2Ltop = tf(G(1,7));
HLtop2Ltst = tf(G(19,1))/tf(G(1,1));

HT02Ttop = tf(G(2,2));
HF2Ttop = tf(G(2,8));
HTtop2Ttst = tf(G(20,2))/tf(G(2,2));

HV02Vtop = tf(G(3,3));
HF2Vtop = tf(G(3,9));
HVtop2Vtst = tf(G(21,3))/tf(G(3,3));

HR02Rtop = tf(G(4,4));
HF2Rtop = tf(G(4,10));
HRtop2Rtst = minreal(tf(G(22,4))/tf(G(4,4)));

HP02Ptop = tf(G(5,5));
HF2Ptop = tf(G(5,11));
HPtop2Ptst = minreal(tf(G(23,5))/tf(G(5,5)));
HPtop2Puim = minreal(tf(G(11,5))/tf(G(5,5)));
HF2Puim = tf(G(11,17));
HPuim2Ptst = minreal(tf(G(23,5))/tf(G(11,5)));
HF2Ptst = tf(G(23,29));
HP02Ptst = tf(G(23,5));

HY02Ytop = tf(G(6,6));
HF2Ytop = tf(G(6,13));
HYtop2Ytst = tf(G(24,6))/tf(G(6,6));

