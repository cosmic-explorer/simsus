   %%  make cbrs/t240 blend xcross over at 0.7 hz
  fcbrs_t240 = 0.1; % about the cross over frequency
  eps = 1/1.7; % slide factor to adjust the gain peaking
  f0 = fcbrs_t240;
  
    %make symmetric filters, arbitry factor of 2
   HP = zpk(-2*pi*[0 0   ],-2*pi*[  f0/2 f0*2 ]*eps,1);
   LP = zpk(-2*pi*[],-2*pi*[[f0/2 f0*2 ]/eps],1);

   HP = HP*zpk(-2*pi*[0 0 ],-2*pi*[pair(0.015,77)],1); % add some shaping as necessary
   HP = HP/abs(freqresp(HP,1e8));  %normalize to 1 at high frequency
   
   LP = LP *zpk(-2*pi*pair(5,45),-2*pi*[pair(0.5,45) 1.5 20],1); % add some shaping as necessary
   LP = LP/abs(freqresp(LP,1e-8)); %normalize to 1 at low frequency
   
   % make compentary
   nLPF = minreal(LP/(LP+HP),1e-4);
   nHPF = minreal(HP/(LP+HP),1e-4);
    PPP = pole(nHPF)/(2*pi);  % check for positive plane poles
   
   figure(8)
   bode(LP,HP,nLPF,nHPF,nLPF+nHPF,2*pi*logspace(-2.5, 1,3333))
   grid on
    if any(real(PPP) > 1e-6)
         title('*** POSTIVE PLANE POLES ***','fontsize',40,'color',[1 0 0],'fontname','Madfont Thorns')
   else
     title('Complementary BRS-T240 Blends','fontsize',40,'color',[0.4 0.1 0.7],'fontname','Mercurius MT')
    end
     h = findobj(gcf,'type','line');
    set(h,'linewidth',4)
legend('Input Low Pass','Input High Pass','Effective Low Pass','Effective High Pass','Sum')