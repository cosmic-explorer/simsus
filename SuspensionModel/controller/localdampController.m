function CtopL = localdampController(G0,Gperfect,DegFree)

Gperfect = G0;

[f,~,~] = damp(Gperfect);

[~, ia, ~] = unique(f, 'first');

% Extraire les valeurs uniques en utilisant les indices
f = f(sort(ia));

if ismember(DegFree,['R','Y','V']) %'V',
fm = f(end-1);
f1 = f(1);
Cop = G0*Controller([1,10,fm*1.3,f1],'leadcut').K;
g_cross = abs(freqresp(Cop,1.005*fm));
K = 1/g_cross;

CtopL = Controller([K,10,fm*1.3,f1],'leadcut').K;

elseif ismember(DegFree,['X']) %'P', %%A+
fm = f(end-12);
f1 = f(1);
Cop = G0*Controller([1,10,fm*1.3,f1],'leadcut').K;
g_cross = abs(freqresp(Cop,1.01*fm));
K = 1/g_cross;

CtopL = Controller([K,10,fm*1.3,f1],'leadcut').K;


c = zpk(-2*pi*[pair(6/2/pi,80)],-2*pi*[pair(30/2/pi,80) pair(43/2/pi,70)],1);
o = G0*c;
K = abs(freqresp(o,fm*1.01));
c = c/K;
CtopL = c;
%elseif ismember(DegFree,['L','V']) 
%fm = 21.4;
%f1 = f(1);
%Cop = G0*Controller([1,10,fm*1.3,f1],'leadcut').K;
%g_cross = abs(freqresp(Cop,1.005*fm));
%K = 1/g_cross;

%CtopL = Controller([K,10,fm*1.3,f1],'leadcut').K;
elseif ismember(DegFree,['P']) %'P', %%%A#
fm = f(end);
f1 = f(1);
Cop = G0*Controller([1,10,fm*1.3,f1],'leadcut').K*zpk(-2*pi*[pair(f1,20)],-2*pi*[pair(f1,75)],1);
g_cross = abs(freqresp(Cop,1.01*fm));
K = 1/g_cross;

CtopL = Controller([K,10,fm*1.8,f1],'leadcut').K*zpk(-2*pi*[pair(f1,20)],-2*pi*[pair(f1,75)],1);

%%A+ L
elseif ismember(DegFree,['']) 
fm = f(end-1);

f1 = f(1);
Cop = G0*Controller([1,10,fm*2,f1],'leadcut').K*zpk(-2*pi*[pair(f1,20)],-2*pi*[pair(f1,75)],1);
g_cross = abs(freqresp(Cop,1.01*fm));
K = 1/g_cross;

CtopL = Controller([1,10,fm*2,f1],'leadcut').K*zpk(-2*pi*[pair(f1,20)],-2*pi*[pair(f1,75)],1)*K;


%%A# L
else
fm = f(end);

f1 = f(1);
Cop = G0*Controller([1,10,fm*1.3,f1],'leadcut').K*zpk(-2*pi*[pair(f1,20)],-2*pi*[pair(f1,75)],1);
g_cross = abs(freqresp(Cop,1.01*fm));
K = 1/g_cross;

CtopL = Controller([1,10,fm*1.3,f1],'leadcut').K*zpk(-2*pi*[pair(f1,20)],-2*pi*[pair(f1,75)],1)*K;

end

CtopL=0;
end

