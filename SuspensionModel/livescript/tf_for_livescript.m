
Hglob1 = Hglob;
Hglob1.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};  
Hglob1.OutputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};

if ismember(input,{'sMP1','sMP2','sMP3','sMP4','hMP1','hMP2','hMP3','hMP4','sMY1','sMY2','sMY3','sMY4','hMY1','hMY2','hMY3','hMY4'}) 

Hglob1 = Hglob1*Sp1inv;
Hglob1.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','sMP1','sMY1','eFx2','eFy2','eFz2','eMR2','sMP2','sMY2','eFx3','eFy3','eFz3','eMR3','sMP3','sMY3','eFx4','eFy4','eFz4','eMR4','sMP4','sMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','hMP1','hMY1','iFx2','iFy2','iFz2','iMR2','hMP2','hMY2','iFx3','iFy3','iFz3','iMR3','hMP3','hMY3','iFx4','iFy4','iFz4','iMR4','hMP4','hMY4'};


end



if ismember(output,{'sP1','sP2','sP3','sP4','hP1','hP2','hP3','hP4','sY1','sY2','sY3','sY4','hY1','hY2','hY3','hY4'}) 

Hglob1 = Sp2*Hglob1;

Hglob1.OutputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};


end





if ismember(output,{'deP1','deP2','deP3','deP4','diP1','diP2','diP3','diP4','deY1','deY2','deY3','deY4','diY1','diY2','diY3','diY4'}) 

Hglob1 = Dd*Hglob1;

Hglob1.OutputName = {'sx1','sy1','sz1','sR1','deP1','sY1','sx2','sy2','sz2','sR2','deP2','sY2','sx3','sy3','sz3','sR3','deP3','sY3','sx4','sy4','sz4','sR4','deP4','deY4','hx1','hy1','hz1','hR1','diP1','hY1','hx2','hy2','hz2','hR2','diP2','hY2','hx3','hy3','hz3','hR3','diP3','diY3','hx4','hy4','hz4','hR4','diP4','diY4'};


end






tf_livescript = Hglob1(output,input);



%figure('Position', [10 10 2*900 2*600]);



[magC, phaseC, wC] = bode(tf_livescript, w);
magdBC = 20*log10(squeeze(magC));

if exist('last_tf')
else
    last_tf = 'firstdef';
end


subplot(2, 1, 1);
if tf_hold == 1
    hold on
else
    hold off
end
%if ismember(strcat(input,'  to  \',output),{'None'})
%elseif eval(strcat(input,'  to  \',output))==0
%if ismember(strcat(input,'  to  \',output),last_tf)
%else    
semilogx(wC/(2*pi), magdBC, 'LineWidth', 2,'DisplayName', strcat(input,'  to  \',output));



%end


xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');
grid on;
set(gca, 'XScale', 'log')
%set(gca, 'FontSize', 30,'FontWeight','bold');
legend();

subplot(2, 1, 2);
if tf_hold == 1
    hold on
else
    hold off
end

%if ismember(strcat(input,'  to  \',output),last_tf)
%else    
semilogx(wC/(2*pi), squeeze(phaseC), 'LineWidth', 2,'DisplayName', strcat(input,'  to  \',output));


last_tf = strcat(input,'  to  \',output);
%end


xlabel('Frequency (Hz)');
ylabel('Phase (deg)');
yticks([-270, -180, -90, 0, 90, 180, 270])
yticklabels({'-270', '-180', '-90', '0', '90', '180', '270'})
%set(gca, 'FontSize', 30,'FontWeight','bold');
grid on;
set(gca, 'XScale', 'log')
legend();

