tic
run('ISI_inputs.m')
run('Noise_inputs.m')
%Execute Noise_inputs.m
run('freq.m')
run('load_inputs.m')
%run("createcomplementaryfilters.m");
run('three_complementary_filters.m');
save('HP.mat',"HP")
save('BP.mat',"BP")
save('LP.mat',"LP")

M10 = 100;
M20 = 100;
M30 = 150;
M40 = 100;
bhqs = BHQS();
save('bhqs.mat','bhqs');

load('SS_BHQS\A_star_dic_ap.mat')
load('SS_BHQS\B_star_dic_ap.mat')



[A,B,C,D]=get_SSBHQS_M(bhqs,M10,M20,300-M10-M20);

A_res_full = zeros(48, 48);  % A is a 48x48 matrix
B_res_full = zeros(48, 30);  % B is a 48x30 matrix

A_res = A_star_dic_ap{1,1};
B_res = B_star_dic_ap{1,1};

fields_A = fieldnames(A_res);
fields_B = fieldnames(B_res);

% Loop through each field and display its content
%parpool();  % Opens a parallel pool with default settings
N=1;
d_e_tot_vec = [];
d_i_tot_vec = [];
S_P_cross_vec = [];
xsol_vec = [];
%parpool();
q = parallel.pool.DataQueue;
afterEach(q, @disp);
tic
for i = 1:N
    fprintf('i')
    send(q, ['Message from worker ' num2str(i)]);
    fieldName_A = fields_A{i};
    A_res_i = A_res.(fieldName_A);
    fieldName_B = fields_B{i};
    B_res_i = B_res.(fieldName_B);
    A_res_full = zeros(48, 48);
    A_res_full(25:48, 1:24) = A_res_i;
    B_res_full = zeros(48, 30);% Lower left block of A is A_star_BHQS
    B_res_full(1:6,1:6) = B_res_i(1:6,1:6);
    %B_star(1:6,1:6)
    G = minreal(ss(A+A_res_full,B+B_res_full,C,D),[],false);
    Gglob = [[G,zeros(24,30)];[zeros(24,30),G]];
    
    input =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47    49    53    55    59];
    output =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47];
    
    Gglobred = Gglob(output,input);
    save('Gglob.mat',"Gglob","G",'Gglobred');


    %run('Clocal.m')
Cloc = Clocal_fun(G,Gglob);
    
run('some_var.m')
[Kopt,Fglob,Sp1red,Sp2red,Sp2invred,Sp1invred,Sp1,Sp1inv,Sp2,Sp2inv,I48] = some_var_fun(bhqs,Gglob,Cloc);
%run('Lsc.m');
save('Cloc.mat','Cloc','Clocred')
save('some_var.mat','Fglob','Sp2','Sp2inv','Sp1','Sp1inv','I48','Kopt','Sp2red','Sp2invred','Sp1red','Sp1invred','Koptred','ClscM','ClscMred')

[Clsc,M] = Lsc_fun(Fglob);


    
    Koptred = Kopt(input,output);
    Clocred = Cloc(input,output);
    ClscM = Clsc*M;
    ClscMred = ClscM(input,output);
    Mred = M(output,output);
    
    Hglob2 = feedback(Gglob,Kopt+Cloc+Clsc*M);%inv(I48+Gglob*(Kopt+Cloc+Clsc*M))*Gglob;
    Hglob2red = feedback(Gglobred,Koptred+Clocred+ClscMred);
    
 
    save('Lsc_var.mat','Hglob2','M','Clsc','Hglob2red','Mred');
    Mtot = 400-100;
    
    nu1 = 0.1;
    phi1 = 45;
    nu2 = 1;
    phi2 = 45;
    gh = 3e6*1/50;
    n1 = 1;
    n2 = 1;
    m1 = 1;
    m2 = 1;
    mu1 = 15;
    psi1 = 45;
    mu2 = 1;
    psi2 = 45;
    
    nus1 = 1;
    phis1 = 45;
    nus2 = 1;
    phis2 = 45;
    gs = 1e1*1;
    
    mus1 = 15;
    psis1 = 45;
    mus2 = 1;
    psis2 = 45;
    warning('off', 'all');
    
    %cont_varh = [nu1,nu2,phi1,phi2,mu1,mu2,psi1,psi2,gh];
    cont_varh=[nu1,phi1,n1,nu2,phi2,n2,mu1,psi1,m1,mu2,psi2,m2,gh];
    
    lb = [log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0];  % Lower bounds for variables
    ub = [log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf,log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf];   % Upper bounds for variables
    
    
    %cont_vars = [nus1,nus2,phis1,phis2,mus1,mus2,psis1,psis2,gs];
    %cont_vars=[nus1,phis1,mus1,psis1,gs];
    sus_var = [M10,M20];
    warning('off', 'Control:analysis:AccuracyLoss');
    %initial_darm 
    %darm_initial = optim_initial();
    
    x0 = cont_varh;%,cont_vars];
    %options = optimoptions("fmincon",'Display','iter-detailed',...
    %    "Algorithm","interior-point",...
    %    "EnableFeasibilityMode",false,...
    %    "SubproblemAlgorithm","cg", 'Display','iter-detailed');
    %options = optimoptions('lsqnonlin','Disp m lay','iter-detailed');%,'Algorithm','sqp');
    %options.ConstraintTolerance = 1e-8;
    %options.OptimalityTolerance = 1e-8;
    %options.StepTolerance = 1e-8;
    %options.FiniteDifferenceStepSize = 0.03                                                                                                                                                                                                                                                           ;
    %options.FiniteDifferenceType="central";
    %options.FunctionTolerance = 1e-8;
    %options.MaxFunctionEvaluations = 2000;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    parpool();
    options = optimoptions('particleswarm', 'SwarmSize', 50, 'UseParallel', true,'UseVectorized',true,'MaxIterations', 100, ...  % Optional: Maximum number of iterations
        'OutputFcn', @ps_outputfcn);
    options.FunctionTolerance = 1e-3;
    %options.MaxIterations = 5;
    %options.OptimalityTolerance = 1e-3;
    %options.StepTolerance = 1e-3;
    %x0 = [-3, 3];   % Initial guess
    
    [xsol, fvalsol, exitflag, output, points] = particleswarm(@optim_vec, 26, lb, ub, options);
    toc
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %[x,f_val] = fmincon(@optim,x0,[0,0,0,0,0,0,0,0,0,1,1],Mtot,[],[],[0,0,-inf,-inf,0,0,-inf,-inf,0,0,0],[+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,Mtot,Mtot],[],options);%@localdamp,options);
    %%%%%[x,f_val] = fmincon(@optim,x0,[0,0,0,0,0,0,0,0,0,0],Mtot,[],[],[0,-inf,0,-inf,0,0,-inf,0,-inf,0],[+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf],[],options);%@localdamp,options);
    
    %[x,f_val] = lsqnonlin(@optim,x0,[-inf,-inf,-inf,-inf,-inf,-inf,0,0],[+inf,+inf,+inf,+inf,+inf,+inf,Mtot,Mtot],[0,0,0,0,0,0,1,1],Mtot,[],[],[],options);%@localdamp,options);
    
    %[x,fval] = fminunc(@optim,x0);
    figure()
    hold on
    %plot_noise(x);
    %plot_noise(x0);
    set(gca, 'YScale', 'log')
    set(gca, 'XScale', 'log')
    
    delete(gcp);  % Close the parallel pool
    %A = readmatrix('SS_BHQS\A_bhqs');
    %B = readmatrix('SS_BHQS\B_bhqs');
    %C = readmatrix('SS_BHQS\C_bhqs');
    %D = readmatrix('SS_BHQS\D_bhqs');


    [d_e_tot,d_i_tot,S_P_cross] = optim_getval(xsol);
    d_e_tot_vec=[d_e_tot_vec;d_e_tot];
    d_i_tot_vec=[d_i_tot_vec;d_i_tot];
    S_P_cross_vec=[S_P_cross_vec;S_P_cross];
    xsol_vec = [xsol_vec;xsol];

end
toc
    function stop = ps_outputfcn(optimValues,y,states)
        % Extract relevant information from optimValues
        disp(optim(optimValues.bestx))
        iteration = optimValues.iteration;
        bestfval = optimValues.bestfval;
        bestx = optimValues.bestx;
        
        % Display current iteration, best objective value, and best position
        fprintf('Iteration: %d, Best Objective: %.4f, Best Solution: %s\n', ...
            iteration, bestfval, mat2str(bestx));
        
        % Check for stopping condition (optional)
        stop = false;  % Set to true to stop optimization early
    end

