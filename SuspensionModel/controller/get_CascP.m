function CascP = get_CascP(ch,cs,model)

CascP = ss(zeros(60,48));
        
KascPmini = inv(model.Spitch)*ss([[cs,0];[0 ch]])*model.Spitch; 

Cii = zpk(KascPmini(1,1));
Cie = zpk(KascPmini(1,2));
Cei = zpk(KascPmini(2,1));
Cee = zpk(KascPmini(2,2));

CascP(29,23) = Cii;
CascP(29,47) = Cie;
CascP(59,47) = Cee;
CascP(59,23) = Cei;

end

