function rms_value = RMS_log(d, f, f_low, f_up)
    s = 0;
    for i = 1:length(d)-1
        if f(i) > f_low && f(i) < f_up
            s = s + ((log(d(i))+log(d(i+1)))/2)^2 * (log(f(i+1)) - log(f(i)));
        end
    end
    rms_value = sqrt(s);
end