function [A, B, C, D] = get_SSBHQS_M(BHQS_instance, M1,M2,M3)
    % Access properties directly from BHQS_instance
    %M1 = BHQS_instance.M1;
    %M2 = BHQS_instance.M2;
    %M3 = BHQS_instance.M3;
    %M4 = BHQS_instance.M4;
    
    L1 = BHQS_instance.L1;
    L2 = BHQS_instance.L2;
    L3 = BHQS_instance.L3;
    L4 = BHQS_instance.L4;
    
    K1 = BHQS_instance.K1;
    K2 = BHQS_instance.K2;
    K3 = BHQS_instance.K3;
    K4 = BHQS_instance.K4;
    
    Ixx1 = BHQS_instance.Ixx1;
    Ixx2 = BHQS_instance.Ixx2;
    Ixx3 = BHQS_instance.Ixx3;
    Ixx4 = BHQS_instance.Ixx4;
    
    Iyy1 = BHQS_instance.Iyy1;
    Iyy2 = BHQS_instance.Iyy2;
    Iyy3 = BHQS_instance.Iyy3;
    Iyy4 = BHQS_instance.Iyy4;
    
    Izz1 = BHQS_instance.Izz1;
    Izz2 = BHQS_instance.Izz2;
    Izz3 = BHQS_instance.Izz3;
    Izz4 = BHQS_instance.Izz4;

    w1 = BHQS_instance.w1;
    w3 = BHQS_instance.w3;
    w5 = BHQS_instance.w5;
    w7 = BHQS_instance.w7;

    n1 = BHQS_instance.n1;
    n3 = BHQS_instance.n3;
    n5 = BHQS_instance.n5;
    n7 = BHQS_instance.n7;
    M4 = BHQS_instance.M4;

    % Initialize matrices
  [A, B, C, D] = get_SS(M1, M2, M3, M4, Ixx1, Iyy1, Izz1, Ixx2, Iyy2, Izz2, Ixx3, Iyy3, Izz3, Ixx4, Iyy4, Izz4, L1, L2,L3,L4, K1,K2,K3,K4,n1,n3,n5,n7,w1,w3,w5,w7);


    % Return matrices A, B, C, and D
    % Note: D matrix remains zeros as per the provided Python code
end