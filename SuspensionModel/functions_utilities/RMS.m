function rms_value = RMS(signals,f, f_low, f_up)
    s = 0;

    for i = 1:length(signals(1,:))-1
        if f(i) >= f_low && f(i) <= f_up
        for j = 1:length(signals(:,1))
        % Get the current signal
        d = signals(j,:);
        
        
        s = s + d(i)^2 * (f(i+1) - f(i));
        end

        end
    end
    rms_value = sqrt(s);
end