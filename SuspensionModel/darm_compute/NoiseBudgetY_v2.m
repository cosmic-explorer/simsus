%function S_P = NoiseBudgetP(Hglob)
%run('Clocal.m');
%tic

p0e = zeros(60,1);
p0e(6) = 1;

p0i = zeros(60,1);
p0i(36) = 1;

%%particular TF

%Effect on i
%tic
%L = Hglob2(43,55)*ClscM(55,43);
%Lf = L/(1+L);%minreal(L/(1+L),1,false);
Lf =1;% dcgain(Lf);
Hi = Hglob*p0i*Lf;
He = Hglob*p0e*Lf;



Hii = minreal(zpk(Hi(48)),1e-5,false);
Hei = minreal(zpk(He(48)),1e-5,false);
%toc
n1e = zeros(48,1);
n1e(6) = 1;


n1i = zeros(48,1);
n1i(30) = 1;
%tic


Hn = Hglob*Cloc*Lf;

nASCs = zeros(48,1);
nASCs(24) = 1;
nASCh = zeros(48,1);
nASCh(48) = 1;
%tic
HnASC = Hglob*CascY*Sp2inv*Lf;

HnASCs = HnASC*nASCs;
HnASCse = minreal(zpk(HnASCs(24)),1e-5,false);
HnASCsi = minreal(zpk(HnASCs(48)),1e-5,false);

HnASCh = HnASC*nASCh;
%toc

%tic
HnASChe = minreal(zpk(HnASCh(24)),1e-5,false);
HnASChi = minreal(zpk(HnASCh(48)),1e-5,false);
%toc

Hn1i = Hn*n1i;

Hn1e = Hn*n1e;

 
%tic

Hn1ii = minreal(zpk(Hn1i(48)),1e-5,false);%,[],false);
Hn1ei = minreal(zpk(Hn1e(48)),1e-5,false);%,[],false);
%toc

n2i = zeros(48,1);
n2i(12) = 1;


n2e = zeros(48,1);
n2e(42) = 1;

Hn2i = Hn*n2i;
Hn2e = Hn*n2e;
%tic
Hn2ii = minreal(zpk(Hn2i(48)),1e-5,false);
Hn2ei = minreal(zpk(Hn2e(48)),1e-5,false);
%toc
%effect on e

%tic
Hie = minreal(zpk(Hi(24)),1e-5,false);
Hee = minreal(zpk(He(24)),1e-5,false);

Hn1ie = minreal(zpk(Hn1i(24)),1e-5,false);%,[],false);
Hn1ee = minreal(zpk(Hn1e(24)),1e-5,false);%,[],false);

Hn2ie = minreal(zpk(Hn2i(24)),1e-5,false);
Hn2ee = minreal(zpk(Hn2e(24)),1e-5,false);

%tic
%S_Pisii=multiply(Hii,w,d_ISIP);

%S_Pisei=multiply(Hei,w,d_ISIP);

%S_Pnii=multiply(Hn1ii,w,N_shot);

%S_Pnei=multiply(Hn1ei,w,N_shot);


%S_Pascsi=multiply(HnASCsi,w,N_ASC);
%S_Paschi=multiply(HnASChi,w,N_ASC);

%S_Pascse=multiply(HnASCse,w,N_ASC);
%S_Pasche=multiply(HnASChe,w,N_ASC);
%toc
%tic
%toc






%%% TEST %%%%%


p0Le = zeros(60,1);
p0Le(1) = 1;
p0Li = zeros(60,1);
p0Li(31) = 1;
%p0Li = zeros(60,1);
%p0Li(1) = 1;
%p0Le = zeros(60,1);
%p0Le(31) = 1;
HLi = Hglob*p0Li;%*Lf;
HLe = Hglob*p0Le;%*Lf;
HLii = minreal(zpk(HLi(48)),1e-5,false);
HLei = minreal(zpk(HLe(48)),1e-5,false);
nL1e = zeros(48,1);
nL1e(1) = 1;
nL1i = zeros(48,1);
nL1i(31) = 1;
Hn = Hglob*Cloc*Lf;
HnL1i = Hn*nL1i;
HnL1e = Hn*nL1e;
HnL1ii = minreal(zpk(HnL1i(48)),1e-5,false);%,[],false);
HnL1ei = minreal(zpk(HnL1e(48)),1e-5,false);%,[],false);
HLie = minreal(zpk(HLi(24)),1e-5,false);
HLee = minreal(zpk(HLe(24)),1e-5,false);
HnL1ie = minreal(zpk(HnL1i(24)),1e-5,false);%,[],false);
HnL1ee = minreal(zpk(HnL1e(24)),1e-5,false);%,[],false);

p0Te = zeros(60,1);
p0Te(2) = 1;
p0Ti = zeros(60,1);
p0Ti(32) = 1;
HTi = Hglob*p0Ti*Lf;
HTe = Hglob*p0Te*Lf;
HTii = minreal(zpk(HTi(48)),1e-5,false);
HTei = minreal(zpk(HTe(48)),1e-5,false);
nT1e = zeros(48,1);
nT1e(2) = 1;
nT1i = zeros(48,1);
nT1i(32) = 1;
Hn = Hglob*Cloc*Lf;
HnT1i = Hn*nT1i;
HnT1e = Hn*nT1e;
HnT1ii = minreal(zpk(HnT1i(48)),1e-5,false);%,[],false);
HnT1ei = minreal(zpk(HnT1e(48)),1e-5,false);%,[],false);
HTie = minreal(zpk(HTi(24)),1e-5,false);
HTee = minreal(zpk(HTe(24)),1e-5,false);
HnT1ie = minreal(zpk(HnT1i(24)),1e-5,false);%,[],false);
HnT1ee = minreal(zpk(HnT1e(24)),1e-5,false);%,[],false);

p0Ve = zeros(60,1);
p0Ve(3) = 1;
p0Vi = zeros(60,1);
p0Vi(33) = 1;
HVi = Hglob*p0Vi*Lf;
HVe = Hglob*p0Ve*Lf;
HVii = minreal(zpk(HVi(48)),1e-5,false);
HVei = minreal(zpk(HVe(48)),1e-5,false);
nV1e = zeros(48,1);
nV1e(3) = 1;
nV1i = zeros(48,1);
nV1i(33) = 1;
Hn = Hglob*Cloc*Lf;
HnV1i = Hn*nV1i;
HnV1e = Hn*nV1e;
HnV1ii = minreal(zpk(HnV1i(48)),1e-5,false);%,[],false);
HnV1ei = minreal(zpk(HnV1e(48)),1e-5,false);%,[],false);
HVie = minreal(zpk(HVi(24)),1e-5,false);
HVee = minreal(zpk(HVe(24)),1e-5,false);
HnV1ie = minreal(zpk(HnV1i(24)),1e-5,false);%,[],false);
HnV1ee = minreal(zpk(HnV1e(24)),1e-5,false);%,[],false);

p0Re = zeros(60,1);
p0Re(4) = 1;
p0Ri = zeros(60,1);
p0Ri(34) = 1;
HRi = Hglob*p0Ri*Lf;
HRe = Hglob*p0Re*Lf;
HRii = minreal(zpk(HRi(48)),1e-5,false);
HRei = minreal(zpk(HRe(48)),1e-5,false);
nR1e = zeros(48,1);
nR1e(4) = 1;
nR1i = zeros(48,1);
nR1i(34) = 1;
Hn = Hglob*Cloc*Lf;
HnR1i = Hn*nR1i;
HnR1e = Hn*nR1e;
HnR1ii = minreal(zpk(HnR1i(48)),1e-5,false);%,[],false);
HnR1ei = minreal(zpk(HnR1e(48)),1e-5,false);%,[],false);
HRie = minreal(zpk(HRi(24)),1e-5,false);
HRee = minreal(zpk(HRe(24)),1e-5,false);
HnR1ie = minreal(zpk(HnR1i(24)),1e-5,false);%,[],false);
HnR1ee = minreal(zpk(HnR1e(24)),1e-5,false);%,[],false);

p0Ye = zeros(60,1);
p0Ye(5) = 1;
p0Yi = zeros(60,1);
p0Yi(35) = 1;
HYi = Hglob*p0Yi*Lf;
HYe = Hglob*p0Ye*Lf;
HYii = minreal(zpk(HYi(48)),1e-12,false);
HYei = minreal(zpk(HYe(48)),1e-12,false);
nY1e = zeros(48,1);
nY1e(5) = 1;
nY1i = zeros(48,1);
nY1i(35) = 1;
Hn = Hglob*Cloc*Lf;
HnY1i = Hn*nY1i;
HnY1e = Hn*nY1e;
HnY1ii = minreal(zpk(HnY1i(48)),1e-12,false);%,[],false);
HnY1ei = minreal(zpk(HnY1e(48)),1e-12,false);%,[],false);
HYie = minreal(zpk(HYi(24)),1e-12,false);
HYee = minreal(zpk(HYe(24)),1e-12,false);
HnY1ie = minreal(zpk(HnY1i(24)),1e-12,false);%,[],false);
HnY1ee = minreal(zpk(HnY1e(24)),1e-12,false);%,[],false);

%toc


%Kglob = feedback(Gglob,Cloc+CascP+Kopt+Clsc*M);
Hlsc = Hglob*ClscM;






%Hn2ie = minreal(Hn2i(16),[],false);
%Hn2ee = minreal(Hn2e(16),[],false);
%toc
%tic
%S_Pisii=multiply(Hii,w,d_ISIP);

%S_Pisei=multiply(Hei,w,d_ISIP);

%S_Pnii=multiply(Hn1ii,w,N_shot);

%S_Pnei=multiply(Hn1ei,w,N_shot);


%S_Pascsi=multiply(HnASCsi,w,N_ASC);
%S_Paschi=multiply(HnASChi,w,N_ASC);

%S_Pascse=multiply(HnASCse,w,N_ASC);
%S_Pasche=multiply(HnASChe,w,N_ASC);
%toc
%tic
%%%%%%%%%%%%%%
%tic

S_theta_iY = sqrt(multiply(Hii,w,d_ISIY).^2+multiply(Hei,w,d_ISIY).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn1ei,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hn2ei,w,N_shot).^2);
S_theta_iY_ASC = sqrt(multiply(HnASCsi,w,N_ASC).^2+multiply(HnASChi,w,N_ASC).^2);
S_theta_iY_crossL = sqrt(multiply(HLii,w,d_ISIL).^2+multiply(HLei,w,d_ISIL).^2+multiply(HnL1ii,w,N_shot).^2+multiply(HnL1ei,w,N_shot).^2);
S_theta_iY_crossT = sqrt(multiply(HTii,w,d_ISIT).^2+multiply(HTei,w,d_ISIT).^2+multiply(HnT1ii,w,N_shot).^2+multiply(HnT1ei,w,N_shot).^2);
S_theta_iY_crossV = sqrt(multiply(HVii,w,d_ISIV).^2+multiply(HVei,w,d_ISIV).^2+multiply(HnV1ii,w,N_shot).^2+multiply(HnV1ei,w,N_shot).^2);
S_theta_iY_crossR = sqrt(multiply(HRii,w,d_ISIR).^2+multiply(HRei,w,d_ISIR).^2+multiply(HnR1ii,w,N_shot).^2+multiply(HnR1ei,w,N_shot).^2);
S_theta_iY_crossP = sqrt(multiply(HYii,w,d_ISIP).^2+multiply(HYei,w,d_ISIP).^2+multiply(HnY1ii,w,N_shot).^2+multiply(HnY1ei,w,N_shot).^2);





S_theta_eY = sqrt(multiply(Hie,w,d_ISIP).^2+multiply(Hee,w,d_ISIP).^2+multiply(Hn1ie,w,N_shot).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ie,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2);
S_theta_eY_ASC = sqrt(multiply(HnASCse,w,N_ASC).^2+multiply(HnASChe,w,N_ASC).^2);
S_theta_eY_crossL = sqrt(multiply(HLie,w,d_ISIL).^2+multiply(HLee,w,d_ISIL).^2+multiply(HnL1ie,w,N_shot).^2+multiply(HnL1ee,w,N_shot).^2);
S_theta_eY_crossT = sqrt(multiply(HTie,w,d_ISIT).^2+multiply(HTee,w,d_ISIT).^2+multiply(HnT1ie,w,N_shot).^2+multiply(HnT1ee,w,N_shot).^2);
S_theta_eY_crossV = sqrt(multiply(HVie,w,d_ISIV).^2+multiply(HVee,w,d_ISIV).^2+multiply(HnV1ie,w,N_shot).^2+multiply(HnV1ee,w,N_shot).^2);
S_theta_eY_crossR = sqrt(multiply(HRie,w,d_ISIR).^2+multiply(HRee,w,d_ISIR).^2+multiply(HnR1ie,w,N_shot).^2+multiply(HnR1ee,w,N_shot).^2);
S_theta_eY_crossP = sqrt(multiply(HYie,w,d_ISIP).^2+multiply(HYee,w,d_ISIP).^2+multiply(HnY1ie,w,N_shot).^2+multiply(HnY1ee,w,N_shot).^2);
%toc
%tic


theta_iY_RMS = RMS(S_theta_iY,f,0.001,300);
theta_eY_RMS = RMS(S_theta_eY,f,0.001,300);

theta_iY_ASC_RMS = RMS(S_theta_iY_ASC,f,0.001,300);
theta_eY_ASC_RMS = RMS(S_theta_eY_ASC,f,0.001,300);

theta_iY_cross_RMSL = RMS(S_theta_iY_crossL,f,0.001,300);
theta_eY_cross_RMSL = RMS(S_theta_eY_crossL,f,0.001,300);

theta_iY_cross_RMST = RMS(S_theta_iY_crossT,f,0.001,300);
theta_eY_cross_RMST = RMS(S_theta_eY_crossT,f,0.001,300);

theta_iY_cross_RMSV = RMS(S_theta_iY_crossV,f,0.001,300);
theta_eY_cross_RMSV = RMS(S_theta_eY_crossV,f,0.001,300);

theta_iY_cross_RMSR = RMS(S_theta_iY_crossR,f,0.001,300);
theta_eY_cross_RMSR = RMS(S_theta_eY_crossR,f,0.001,300);

theta_iY_cross_RMSP = RMS(S_theta_iY_crossP,f,0.001,300);
theta_eY_cross_RMSP = RMS(S_theta_eY_crossP,f,0.001,300);


S_theta_eY_crossTOT = sqrt(S_theta_eY_crossL.^2+S_theta_eY_crossV.^2+S_theta_eY_crossT.^2+S_theta_eY_crossR.^2+S_theta_eY_crossP.^2+S_theta_eY_ASC.^2);
S_theta_iY_crossTOT = sqrt(S_theta_iY_crossL.^2+S_theta_iY_crossV.^2+S_theta_iY_crossT.^2+S_theta_iY_crossR.^2+S_theta_iY_crossP.^2+S_theta_iY_ASC.^2);


S_theta_YLSC = multiply(Hlsc(48-24,44-24),w,S_L0);

S_theta_eY_LSC = 0;%multiply(Hlsc(47-24,43-24),w,S_L0);
S_theta_iY_LSC =0;% multiply(Hlsc(47,43-24),w,S_L0);


theta_eY_lsc_RMS = RMS(S_theta_eY_LSC,f,0.001,300);
theta_iY_lsc_RMS = RMS(S_theta_iY_LSC,f,0.001,300);


%theta_Ylsc_RMS = RMS(S_theta_YLSC,f,0.001,300);

S_theta_eY_tot = sqrt(S_theta_eY.^2+S_theta_eY_crossTOT.^2+S_theta_eY_LSC.^2+S_theta_iY_LSC.^2);
S_theta_iY_tot = sqrt(S_theta_iY.^2+S_theta_iY_crossTOT.^2);
theta_eY_tot_RMS = RMS(S_theta_eY_tot,f,0.001,300);
theta_iY_tot_RMS = RMS(S_theta_iY_tot,f,0.001,300);




%%%%%%%%


dHglob = Dd*Hglob*Lf;
dHglobCloc = dHglob*Cloc;
dHglobCascY = dHglob*CascY*Sp2inv;
dHglobClsc = dHglob*ClscM;



S_d_iY = sqrt(multiply(minreal(zpk(dHglob(48,6)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(dHglob(48,36)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(dHglobCloc(48,6)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(48,30)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCascY(48,24)),[],1e-5),w,N_ASC).^2+multiply(minreal(zpk(dHglobCascY(48,48)),[],1e-5),w,N_ASC).^2);
S_d_iY_crossL = sqrt(multiply(minreal(zpk(dHglob(48,1)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(dHglob(48,31)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(dHglobCloc(48,1)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(48,25)),[],1e-5),w,N_shot).^2);
S_d_iY_crossT = sqrt(multiply(minreal(zpk(dHglob(48,2)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(dHglob(48,32)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(dHglobCloc(48,2)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(48,26)),[],1e-5),w,N_shot).^2);
S_d_iY_crossV = sqrt(multiply(minreal(zpk(dHglob(48,3)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(dHglob(48,33)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(dHglobCloc(48,3)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(48,27)),[],1e-5),w,N_shot).^2);
S_d_iY_crossR = sqrt(multiply(minreal(zpk(dHglob(48,4)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(dHglob(48,34)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(dHglobCloc(48,4)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(48,28)),[],1e-5),w,N_shot).^2);
S_d_iY_crossP = sqrt(multiply(minreal(zpk(dHglob(48,5)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(dHglob(48,35)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(dHglobCloc(48,5)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(48,29)),[],1e-5),w,N_shot).^2);




S_d_eY = sqrt(multiply(minreal(zpk(dHglob(24,6)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(dHglob(24,36)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(dHglobCloc(24,6)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(24,30)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCascY(24,24)),[],1e-5),w,N_ASC).^2+multiply(minreal(zpk(dHglobCascY(24,48)),[],1e-5),w,N_ASC).^2);
S_d_eY_crossL = sqrt(multiply(minreal(zpk(dHglob(24,1)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(dHglob(24,31)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(dHglobCloc(24,1)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(24,25)),[],1e-5),w,N_shot).^2);
S_d_eY_crossT = sqrt(multiply(minreal(zpk(dHglob(24,2)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(dHglob(24,32)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(dHglobCloc(24,2)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(24,26)),[],1e-5),w,N_shot).^2);
S_d_eY_crossV = sqrt(multiply(minreal(zpk(dHglob(24,3)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(dHglob(24,33)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(dHglobCloc(24,3)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(24,27)),[],1e-5),w,N_shot).^2);
S_d_eY_crossR = sqrt(multiply(minreal(zpk(dHglob(24,4)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(dHglob(24,34)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(dHglobCloc(24,4)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(24,28)),[],1e-5),w,N_shot).^2);
S_d_eY_crossP = sqrt(multiply(minreal(zpk(dHglob(24,5)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(dHglob(24,35)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(dHglobCloc(24,5)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(dHglobCloc(24,29)),[],1e-5),w,N_shot).^2);




d_iY_RMS = RMS(S_d_i,f,0.001,300);
d_eY_RMS = RMS(S_d_e,f,0.001,300);

d_iY_cross_RMSL = RMS(S_d_iY_crossL,f,0.001,300);
d_eY_cross_RMSL = RMS(S_d_eY_crossL,f,0.001,300);

d_iY_cross_RMST = RMS(S_d_iY_crossT,f,0.001,300);
d_eY_cross_RMST = RMS(S_d_eY_crossT,f,0.001,300);

d_iY_cross_RMSV = RMS(S_d_iY_crossV,f,0.001,300);
d_eY_cross_RMSV = RMS(S_d_eY_crossV,f,0.001,300);

d_iY_cross_RMSR = RMS(S_d_iY_crossR,f,0.001,300);
d_eY_cross_RMSR = RMS(S_d_eY_crossR,f,0.001,300);

d_iY_cross_RMSP = RMS(S_d_iY_crossP,f,0.001,300);
d_eY_cross_RMSP = RMS(S_d_eY_crossP,f,0.001,300);


S_d_eY_crossTOT = sqrt(S_d_eY_crossL.^2+S_d_eY_crossV.^2+S_d_eY_crossT.^2+S_d_eY_crossR.^2+S_d_eY_crossP.^2);
S_d_iY_crossTOT = sqrt(S_d_iY_crossL.^2+S_d_iY_crossV.^2+S_d_iY_crossT.^2+S_d_iY_crossR.^2+S_d_iY_crossP.^2);


S_d_eY_LSC = multiply(dHglobClsc(23,19),w,S_L0e);
S_d_iY_LSC = multiply(dHglobClsc(47,19),w,S_L0i);

d_iY_lsc_RMS = RMS(S_d_iY_LSC,f,0.001,300);
d_eY_lsc_RMS = RMS(S_d_eY_LSC,f,0.001,300);

S_d_eY_tot = sqrt(S_d_eY.^2+S_d_eY_crossTOT.^2+S_d_eY_LSC.^2);
S_d_iY_tot = sqrt(S_d_iY.^2+S_d_iY_crossTOT.^2+S_d_iY_LSC.^2);
d_eY_tot_RMS = RMS(S_d_eY_tot,f,0.001,300);
d_iY_tot_RMS = RMS(S_d_iY_tot,f,0.001,300);





S_Y_tot = d_eY_tot_RMS*S_theta_eY_tot+theta_eY_tot_RMS*S_d_eY_tot+d_iY_tot_RMS*S_theta_iY_tot+theta_iY_tot_RMS*S_d_iY_tot;
%S_P_cross = sqrt(2*(d_i_tot^2*S_theta_i_crossTOT.^2+d_e_tot^2*S_theta_e_crossTOT.^2));




%toc
%tic

%d_i_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_RMS^2+theta_i_RMS^2);
%_e_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_RMS^2+bhqs.gi^2*theta_e_RMS^2);

%d_i_cross_RMSL = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_cross_RMSL^2+theta_i_cross_RMSL^2);
%d_e_cross_RMSL = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_cross_RMSL^2+bhqs.gi^2*theta_e_cross_RMSL^2);

%d_i_cross_RMST = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_cross_RMST^2+theta_i_cross_RMST^2);
%d_e_cross_RMST = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_cross_RMST^2+bhqs.gi^2*theta_e_cross_RMST^2);

%d_i_cross_RMSV = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_cross_RMSV^2+theta_i_cross_RMSV^2);
%d_e_cross_RMSV = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_cross_RMSV^2+bhqs.gi^2*theta_e_cross_RMSV^2);

%d_i_cross_RMSR = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_cross_RMSR^2+theta_i_cross_RMSR^2);
%d_e_cross_RMSR = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_cross_RMSR^2+bhqs.gi^2*theta_e_cross_RMSR^2);

%d_i_cross_RMSY = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_cross_RMSY^2+theta_i_cross_RMSY^2);
%d_e_cross_RMSY = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_cross_RMSY^2+bhqs.gi^2*theta_e_cross_RMSY^2);


%d_i_cross_tot = sqrt(d_i_cross_RMST^2+ d_i_cross_RMSL^2+ d_i_cross_RMSV^2+ d_i_cross_RMSR^2+ d_i_cross_RMSY^2);
%d_e_cross_tot = sqrt(d_e_cross_RMST^2+ d_e_cross_RMSL^2+ d_e_cross_RMSV^2 + d_e_cross_RMSR^2 + d_e_cross_RMSY^2);




%d_LSC_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_lsc_RMS^2);



%d_i_tot = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_tot_RMS^2+theta_i_tot_RMS^2);
%d_e_tot = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_tot_RMS^2+bhqs.gi^2*theta_e_tot_RMS^2);



%S_P_cross = sqrt(2*(d_i_tot^2*S_theta_i_crossTOT.^2+d_e_tot^2*S_theta_e_crossTOT.^2));
%S_P_cross_cross = sqrt(2*(d_itot^2*S_theta_i_crossTOT.^2+d_etot^2*S_theta_e_crossTOT.^2));
%S_P_LSC = sqrt(2*(d_e_tot^2*S_theta_LSC.^2));
%S_P_ASC = sqrt(2*(d_e_tot^2*multiply(HnASChe,w,N_ASC).^2+d_i_tot^2*multiply(HnASChi,w,N_ASC).^2));
%S_P = sqrt(2*(d_i_tot^2*S_theta_i.^2+d_e_tot^2*S_theta_e.^2));
%S_P_tot=sqrt(2*(d_i_tot^2*S_theta_i_tot.^2+d_e_tot^2*S_theta_e_tot.^2)) ;%sqrt(S_P.^2+S_P_cross.^2+S_P_LSC);%sqrt(2*(d_i_RMS^2*S_theta_i.^2+d_e_RMS^2*S_theta_e.^2+d_i_tot^2*S_theta_i.^2+d_e_tot^2*S_theta_e.^2+d_i_tot^2*S_theta_i_crossTOT.^2+d_e_tot^2*S_theta_e_crossTOT.^2+S_P_LSC.^2));

%toc
%toc

%a=a;

%end

