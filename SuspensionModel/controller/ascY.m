%C =ss(cont);% Controller([100,10,15],'leadcut').K;
load('controller.mat');
%load('cs.mat');
%load('ch.mat');
%C = con;
%B1 = Hglobsh(23,29)*C;
%B2 = Hglobsh(47,59)*C;
%cs = con/abs(freqresp(B1,2*pi*50));
%ch = con/abs(freqresp(B2,2*pi*50));
%ch = zpk(-2*pi*[pair(nu1,phi1) pair(nu2,phi2) ],-2*pi*[pair(mu1,psi1) pair(mu2,psi2)],gh);
%ch = zpk(-2*pi*[pair(nu1,phi1) pair(nu1,phi1) 200],-2*pi*[0.001 pair(mu1,psi1) pair(mu1,psi1)],gh);
%ch = Zpow(nu1,phi1,n1)*Zpow(nu2,phi2,n2)*Ppow(mu1,psi1,m1)*Ppow(mu2,psi2,m2)*gh;
%Hglobshperf = feedback();
[fh,~,~] = damp(minreal(zpk(Hglobsh(48,60)),1e-2));

fm =fh(end);

[fs,~,~] = damp(minreal(zpk(Hglobsh(24,30)),1e-2));

[~, ia, ~] = unique(fs, 'first');

% Extraire les valeurs uniques en utilisant les indices
fs = fs(sort(ia));

fi = fs(1);
f2 = fs(2);
%ch = Zpow(0,0,1)*Ppow(fm*1.22/2/pi,80,1)*Ppow(1.533*fm/2/pi,80,2)*exp(gh);

%ch = zpk([],[0],1)*Zpow(fm/2/pi/1.3,83,1)*Ppow(1.2*fm/2/pi,75,1)*Ppow(2.5*fm/2/pi,85,1)*exp(gh);

%chlow = zpk(-2*pi*[pair(0.1,80)],-2*pi*[0 0.1],1);
chlow = zpk(-2*pi*[pair(0.3*hgh,30) pair(0.3*hgh,30) pair(0.3*hgh,30) ],-2*pi*[pair(0,80) pair(0.01*lgh,80) pair(0.01*lgh,80)],1);
%chlow = zpk(-2*pi*[pair(0.04,70) pair(0.08,70) pair(fm/2/2/pi,70)],-2*pi*[pair(0.01,80) pair(0.01,80) pair(0.01,80)],1);
%middlepeakstab = zpk(-2*pi*[ pair(f2/5/2/pi,40) pair(f2/5/2/pi,40) pair(f2*2*as/2/pi,40) pair(f2*2*as/2/pi,40)],-2*pi*[pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40)],1);
middlepeakstabh = zpk(-2*pi*[ pair(f2/5/2/pi,40) pair(f2*2*as/2/pi,40)],-2*pi*[pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40)],1);

%ch = chlow*Zpow(fm/a/2/pi,phi1,1)*Ppow(b*fm/2/pi,phi2,2)*Ppow(b*c*fm/2/pi,phi3,1)*exp(gh);
%cutoffhard = myellip_z2(4*fm/2/pi*b, 4, 2, 300, 10);
%cutoffhard = Ppow(5*fm/2/pi*b,53,1)*Ppow(8*fm/2/pi*b,83,1);
%cutoffhard = Ppow(4*fm/2/pi*b,58,1)*Ppow(14*fm/2/pi*b,73,1);
cutoffhard = myellip_z2(4*fm/2/pi*b*2.5, 6, 2, 400, 10);
compensationh = zpk(-2*pi*[pair(9*csoft*fi/2/pi,phics)],-2*pi*[30*9*csoft*fi/2/pi],1);
%ch = chlow*Zpow(0.1*a,phi1,1)*Ppow(b*fm/2/pi,phi2,2)*Ppow(b*c*fm/2/pi,phi3,1)*exp(gh);
%ch = chlow*Zpow(0.1*a,phi1,1)*cutoffhard*exp(gh);


%ch = chlow*middlepeakstabh*cutoffhard*compensationh;


%ch = cutoffhard*Zpow(fm/2/pi/2,63,1)*zpk(-2*pi*[],-2*pi*[0.00001],1)*zpk(-2*pi*[pair(0.01,30)],-2*pi*[0.00001 0.00001],1);
%ol = ch*Hglobsh(47,59);
%K = abs(freqresp(ol,fm*gh));
%ch =0;% ch/K;

chlow = Ppow(0.01,70,1);
cross1 = Ppow(0.4,40,1)*Zpow(0.2,40,1);
cross2 = Zpow(1,83,1)*Zpow(2,83,1);
%croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
crollh = myellip_z2(2.6*3.5,8,3,120,30);
ch = chlow*cross1*cross2*crollh;
olh = ch*Hglobsh(48,60);
Kh = abs(freqresp(olh,0.3*2*pi));
ch = ch/Kh;
%figure();bode(Hglobsh(47,59)*ch)
%zpk([],[0],1)*Zpow(fm/2/pi/1.3,83,1)*Ppow(2*fm/2/pi,7,1)*exp(gh);
%ch=zpk(-[pair(2,80) pair(15,50) pair(15,50) pair(300,15) ],-[ 5 pair(25,55) pair(25,55) pair(25,55) pair(25,75) pair(25,75) pair(25,75)],1);
%ch = zpk(-[],-[pair(15,15) pair(15,15) pair(15,15)],1);
%cs = zpk(-2*pi*[pair(nus1,phis1) pair(nus2,phis2)],-2*pi*[pair(mus1,psis1) pair(mus2,psis2)],gs);



% chlow = Ppow(0.01,70,1);
% cross1 = Ppow(0.4,40,1)*Zpow(0.2,40,1);
% cross2 = Zpow(1,83,1)*Zpow(2,83,1);
% %croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
% croll = myellip_z2(2.6*2.5,6,3,120,30);
% ch = chlow*cross1*cross2*croll;
% ol = ch*Hglobsh(47,59);
% K = abs(freqresp(ol,0.3*2*pi));
% ch = ch/K;



%%order 4 A#
 chlow = Ppow(0.01,70,1);
 cross1 = Ppow(0.4,40,1)*Zpow(0.2,40,1);
 cross2 = Zpow(1,83,1)*Zpow(2,83,1);
 %croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
 croll = myellip_z2(2.6*1.7,4,3,120,30);
 ch = chlow*cross1*cross2*croll;
 ol = ch*Hglobsh(48,60);
 K = abs(freqresp(ol,0.3*2*pi));
 ch = ch/K;


 %%order 4 A+
 %chlow = Ppow(0.01,70,1);
 %cross1 = Ppow(0.4,40,1)*Zpow(0.2,40,1);
 %cross2 = Zpow(1,83,1)*Zpow(2,83,1);
 %%croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
 %croll = myellip_z2(2.6*1.7,4,3,120,30);
 %cross3 = Zpow(6.35/2/pi,70,1)*Ppow(6.35/2/pi,30,1);
 %ch = chlow*cross1*cross2*croll*cross3;
 %ol = ch*Hglobsh(47,59);
 %K = abs(freqresp(ol,0.3*2*pi));
 %ch = ch/K;



%cslow = zpk(-2*pi*[pair(0.04*hg,70) pair(0.08*hg,70) pair(0.3*hg,70) ],-2*pi*[pair(0,80) pair(0.01*lg,80) pair(0.01*lg,80)],1);
%cutoffsoft = myellip_z2(20*fi*bs, 8, 2, 300, 10);




%cutoffsoft = myellip_z2(10*fi*bs,3,1,100,10);
%cslow = zpk(-2*pi*[pair(0.3*hgs,70) pair(0.3*hgs,70) pair(0.3*hgs,70) ],-2*pi*[pair(0,80) pair(0.01*lgs,80) pair(0.01*lgs,80)],1);
%middlepeakstabs = zpk(-2*pi*[pair(f2/5/2/pi,40) pair(f2/5/2/pi,80) pair(f2*2*as/2/pi,80) pair(f2*2*as/2/pi,40)],-2*pi*[pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40) pair(f2/1.5/2/pi,40)],1);
%compensations = zpk(-2*pi*[pair(9*csoft*fi/2/pi,phics)],-2*pi*[9*csoft*fi/2/pi],1);
%cs = cslow*middlepeakstabs*compensations*cutoffsoft*exp(gs);



%cs = cslow*Zpow(fi/as/2/pi,phis1,1)*cutoffsoft*exp(gs);
%ol = cs*Hglobsh(23,29);
%K = abs(freqresp(ol,0.1*2*pi));
%cs = cs/K*3;

cutoffsoft = myellip_z2(fi*bs*1.5,4,1,100,10);
cs = cutoffsoft*Zpow(fm/2/pi/20,63,1)*zpk(-2*pi*[],-2*pi*[0.00001],1)*zpk(-2*pi*[pair(0.01,30)],-2*pi*[0.00001 0.00001],1)*zpk(-2*pi*[fm/2/pi/20],-2*pi*[0],1);
ols = cs*Hglobsh(24,30);
Ks = abs(freqresp(ols,fi*8));
cs = 0;%cs/Ks;

chlow = Ppow(0.01,70,1);
cross1 = Ppow(0.4,40,1)*Zpow(0.2,40,1);
cross2 = Zpow(0.2,83,1)*Zpow(0.2,83,1)*zpk([],-2*pi*[0.2],1);
%croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
crolls = myellip_z2(2.6*4.5,8,3,120,30);
cs = chlow*cross1*cross2*crolls;
ols = cs*Hglobsh(24,30);
Ks = abs(freqresp(ols,0.3*2*pi));
cs = cs/Ks;


%%ORDER 4 A#


chlow = Ppow(0.01,70,1);
cross1 = Ppow(0.4,40,1)*Zpow(0.2,40,1);
cross2 = Zpow(0.2,83,1)*Zpow(0.2,83,1)*zpk([],-2*pi*[0.2],1);
%croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
crolls = myellip_z2(2.6*1.7,4,3,120,30);
cross3 = Zpow(12.8/2/pi,70,1)*Ppow(12.8/2/pi,30,1);
cs = chlow*cross1*cross2*crolls*cross3;
ols = cs*Hglobsh(24,30);
Ks = abs(freqresp(ols,0.3*2*pi));
cs = cs/Ks;



%%ORDER 4 A+


%chlow = Ppow(0.01,70,1);
%cross1 = Ppow(0.4,80,1)*Zpow(0.2,30,1);
%cross2 = Zpow(0.2,83,1)*Zpow(0.2,83,1)*zpk([],-2*pi*[0.2],1);
%%croll = Ppow(4.7,50,1)*Ppow(8.5,70,1)*Ppow(10,40,1)*Ppow(15,60,1)*Ppow(20,70,1);
%crolls = myellip_z2(2.6*2.1,4,3,120,30);
%cross3 = Zpow(12.8/2/pi,70,1)*Ppow(12.8/2/pi,30,1);
%cs = chlow*cross1*cross2*crolls*cross3;
%ols = cs*Hglobsh(23,29);
%Ks = abs(freqresp(ols,0.3*2*pi));
%cs = cs/Ks;

%cs = Zpow(fi/3/2/pi,80,1)*Ppow(fi*1.3/2/pi,80,2)*Ppow(fi*5/2/pi,80,2);
%ols = cs*Hglobsh(23,29);
%Ks = abs(freqresp(ols,fi*1.05));
%cs = cs/Ks;
%ch= Zpow(fm/3/2/pi,80,1)*Ppow(fm*1.2/2/pi,76,2)*Ppow(fm*4.5/2/pi,60,2)*zpk(-2*pi*[fm/6/2/pi],-2*pi*[0],1);
%ol = ch*Hglobsh(47,59);
%K = abs(freqresp(ol,fm*1.03));
%ch = ch/K;



%cs = zpk([],[0],1)*Zpow(fi/2/pi/1.3,83,1)*Ppow(2*fi/2/pi,7,1)*exp(gs);%Zpow(nus1,phis1,ns1)*Zpow(nus2,phis2,ns2)*Ppow(mus1,psis1,ms1)*Ppow(mus2,psis2,ms2)*gs;%zpk(-2*pi*[pair(nus1,phis1) pair(nus1,phis1) 200],-2*pi*[0.001 pair(mus1,psis1) pair(mus1,psis1)],gs);



ch=0;
cs=0;



%cs = 1e7*zpk(-[pair(0.5,45) pair(1,75)],-[0.2 0.2 0.8 0.8  pair(15,75)],1);
%C1=C;
%C2=C;
%LPF
%BPF
%HPF

K3 = 1;

[f,~,~] = damp(minreal(Gglob(24,30),[],false));

fm = f(end);


CascY = ss(zeros(60,48));
KascYmini = inv(bhqs.Syaw)*ss([[cs,0];[0 ch]])*bhqs.Syaw; 
%KascPmini = bhqs.Spitch*[[cs,0];[0 ch]]*inv(bhqs.Spitch); 
%CascP(29,23) = KascPmini(1,1);
%CascP(29,47) = KascPmini(1,2);
%CascP(59,23) = KascPmini(2,1);
%CascP(59,47) = KascPmini(2,2);

Cii = zpk(KascYmini(1,1));
Cie = zpk(KascYmini(1,2));
Cei = zpk(KascYmini(2,1));
Cee = zpk(KascYmini(2,2));

A3ii = Hglob2(24,30);
A2ii = Hglob2(24,24);
A1ii = Hglob2(24,18);

K1ii = zpk(A3ii/A1ii);%1/minreal(1/(K3*A3ii/A1ii),1e-12);
K2ii = zpk(A3ii/A2ii);%1/minreal(1/(K3*A3ii/A2ii),1e-12);

%A3ie = minreal(tf(Hglob2(47,29)));
%A2ie = minreal(tf(Hglob2(47,23)));
%A1ie = minreal(tf(Hglob2(47,17)));

%K1ie = K3*A3ie/A1ie;
%K2ie = K3*A3ie/A2ie;

%A3ee = minreal(tf(Hglob2(47,59)));
%A2ee = minreal(tf(Hglob2(47,53)));
%A1ee = minreal(tf(Hglob2(47,47)));

%K1ee = K3*A3ee/A1ee;
%K2ee = K3*A3ee/A2ee;


%A3ei = minreal(tf(Hglob2(23,59)));
%A2ei = minreal(tf(Hglob2(23,53)));
%A1ei = minreal(tf(Hglob2(23,47)));

%K1ei = K3*A3ei/A1ei;
%K2ei = K3*A3ei/A2ei;


%K2 = Kasc(fm,2);
%K1 = Kasc(fm,4);

%Cascp = tf(zeros(60,48));
CascY(30,24) = Cii;%*K3*HP;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%CascP(23,23) = Cii*K2ii*BP;
%CascP(17,23) = Cii*K1ii*LP;

CascY(30,48) = Cie;%*K3*HP;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%CascP(23,47) = Cie*K2ie*BP;
%CascP(17,47) = Cie*K1ie*LP;

CascY(60,48) = Cee;%*HP;
%Cascp(53,47) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%CascP(53,47) = K2ee*Cee*BP;
%CascP(47,47) = K1ee*Cee*LP;

CascY(60,24) = Cei;%*HP;
%Cascp(53,47) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%CascP(53,23) = K2ei*Cei*BP;
%CascP(47,23) = K1ei*Cei*LP;