function rms_int = RMS_int(signals,f, f_low, f_up)
   
    rms_int = zeros(1,length(signals(1,:)));
    rms_loc = 0;
    for i = 1:length(signals(1,:))-1
        if f(i) >= f_low && f(i) <= f_up
        for j = 1:length(signals(:,1))
        % Get the current signal
        d = signals(j,:);
        rms_loc = sqrt(rms_loc^2+d(end-i)^2*(f(end-i+1)-f(end-i)));
        
        rms_int(end-i) = rms_loc;
        end

        end
    end
end