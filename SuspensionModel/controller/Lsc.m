load('Tst2TstCont.mat');
Cq = cont;%Controller([100,10,15],'leadcut').K;
%Bq = Fglob(43,55)*Cq;
%Cq = Cq/abs(freqresp(Bq,2*pi*100));
%LPF
%BPF
%HPF
%run("createcomplementaryfilters.m");
%run('generic_blend.m');
%run('three_complementary_filters.m')
M = eye(48);%zeros(48,48);
M(19,43) = -1;
M(19,19)=1;

K3 = 1;

[f,~,~] = damp(minreal(Fglob(23,29),[],false));

fm = f(end);


A3 = minreal(zpk(Fglob(19,25)),[],false);
A2 = minreal(zpk(Fglob(19,19)),[],false);
A1 = minreal(zpk(Fglob(19,13)),[],false);


%K2 = Kasc(fm,2);
%K1 = Kasc(fm,4);
K1 = K3*A3/A1;
K2 = K3*A3/A2;
Clsc = zpk(zeros(60,48));
%Cascp(25,19) = K3*C*HPF;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%Cascp(19,19) = K2*C*BPF;
%Cascp(13,19) = K1*C*LPF;

Clsc(25,19) = Cq;
%Clsc(25,23) = 0.002*Cq;
%Clsc(47,23) = 0.004*Cq;
%Clsc(49,43) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%Clsc(49,43) = K2*C*BP;
%Clsc(43,43) = K1*C*LP;
