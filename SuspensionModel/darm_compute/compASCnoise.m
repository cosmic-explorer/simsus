Hie2sh = Sp2*Hglob;

Hie2shloc = Sp2*Hglob*Cloc;
Hie2shasc = Sp2*Hglob*CascP*Sp2inv;

S_theta_h = sqrt(multiply(Hie2sh(47,5),w,d_ISIP).^2+multiply(Hie2sh(47,5+30),w,d_ISIP).^2+multiply(Hie2shloc(47,5),w,N_shot).^2+multiply(Hie2shloc(47,29),w,N_shot).^2);%multiply(HnASCsi,w,N_ASC).^2+multiply(HnASChi,w,N_ASC).^2);
S_theta_h_ASC = multiply(Hie2shasc(47,47),w,N_ASC);%.^2+multiply(HnASChi,w,N_ASC).^2);
S_theta_h_crossL = sqrt(multiply(Hie2sh(47,1),w,d_ISIL).^2+multiply(Hie2sh(47,31),w,d_ISIL).^2+multiply(Hie2shloc(47,1),w,N_shot).^2+multiply(Hie2shloc(47,25),w,N_shot).^2);
S_theta_h_crossT = sqrt(multiply(Hie2sh(47,1+1),w,d_ISIT).^2+multiply(Hie2sh(47,31+1),w,d_ISIT).^2+multiply(Hie2shloc(47,1+1),w,N_shot).^2+multiply(Hie2shloc(47,25+1),w,N_shot).^2);
S_theta_h_crossV = sqrt(multiply(Hie2sh(47,1+2),w,d_ISIV).^2+multiply(Hie2sh(47,31+2),w,d_ISIV).^2+multiply(Hie2shloc(47,1+2),w,N_shot).^2+multiply(Hie2shloc(47,25+2),w,N_shot).^2);
S_theta_h_crossR = sqrt(multiply(Hie2sh(47,1+3),w,d_ISIR).^2+multiply(Hie2sh(47,31+3),w,d_ISIR).^2+multiply(Hie2shloc(47,1+3),w,N_shot).^2+multiply(Hie2shloc(47,25+3),w,N_shot).^2);
S_theta_h_crossY = sqrt(multiply(Hie2sh(47,1+5),w,d_ISIY).^2+multiply(Hie2sh(47,31+5),w,d_ISIY).^2+multiply(Hie2shloc(47,1+5),w,N_shot).^2+multiply(Hie2shloc(47,25+5),w,N_shot).^2);

S_theta_h_tot = sqrt(S_theta_h.^2+S_theta_h_ASC.^2+S_theta_h_crossL.^2+S_theta_h_crossT.^2+S_theta_h_crossV.^2+S_theta_h_crossR.^2+S_theta_h_crossY.^2);

Hlschs = Sp2*Hglob*Clsc*M;
S_theta_h_LSC = multiply(Hlschs(47,43),w,S_L0);


S_theta_s = sqrt(multiply(Hie2sh(23,5),w,d_ISIP).^2+multiply(Hie2sh(23,5+30),w,d_ISIP).^2+multiply(Hie2shloc(23,5),w,N_shot).^2+multiply(Hie2shloc(23,29),w,N_shot).^2);%multiply(HnASCsi,w,N_ASC).^2+multiply(HnASChi,w,N_ASC).^2);
S_theta_s_ASC = multiply(Hie2shasc(23,23),w,N_ASC);%.^2+multiply(HnASChi,w,N_ASC).^2);
S_theta_s_crossL = sqrt(multiply(Hie2sh(23,1),w,d_ISIL).^2+multiply(Hie2sh(23,31),w,d_ISIL).^2+multiply(Hie2shloc(23,1),w,N_shot).^2+multiply(Hie2shloc(23,25),w,N_shot).^2);
S_theta_s_crossT = sqrt(multiply(Hie2sh(23,1+1),w,d_ISIT).^2+multiply(Hie2sh(23,31+1),w,d_ISIT).^2+multiply(Hie2shloc(23,1+1),w,N_shot).^2+multiply(Hie2shloc(23,25+1),w,N_shot).^2);
S_theta_s_crossV = sqrt(multiply(Hie2sh(23,1+2),w,d_ISIV).^2+multiply(Hie2sh(23,31+2),w,d_ISIV).^2+multiply(Hie2shloc(23,1+2),w,N_shot).^2+multiply(Hie2shloc(23,25+2),w,N_shot).^2);
S_theta_s_crossR = sqrt(multiply(Hie2sh(23,1+3),w,d_ISIR).^2+multiply(Hie2sh(23,31+3),w,d_ISIR).^2+multiply(Hie2shloc(23,1+3),w,N_shot).^2+multiply(Hie2shloc(23,25+3),w,N_shot).^2);
S_theta_s_crossY = sqrt(multiply(Hie2sh(23,1+5),w,d_ISIY).^2+multiply(Hie2sh(23,31+5),w,d_ISIY).^2+multiply(Hie2shloc(23,1+5),w,N_shot).^2+multiply(Hie2shloc(23,25+5),w,N_shot).^2);

S_theta_s_tot = sqrt(S_theta_s.^2+S_theta_s_ASC.^2+S_theta_s_crossL.^2+S_theta_s_crossT.^2+S_theta_s_crossV.^2+S_theta_s_crossR.^2+S_theta_s_crossY.^2);
S_theta_s_LSC = multiply(Hlschs(29,43),w,S_L0);



S_theta_tot_hs = sqrt(S_theta_s_tot.^2+S_theta_h_tot.^2);











%%










hsdHglob = Sp2*Dd*Hglob*Lf;
hsdHglobCloc = hsdHglob*Cloc;
hsdHglobCascP = hsdHglob*CascP*Sp2inv;
hsdHglobClsc = hsdHglob*Clsc;

S_d_s = sqrt(multiply(minreal(zpk(hsdHglob(23,5)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(hsdHglob(23,35)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(hsdHglobCloc(23,5)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(23,29)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCascP(23,23)),[],1e-5),w,N_ASC).^2+multiply(minreal(zpk(hsdHglobCascP(23,47)),[],1e-5),w,N_ASC).^2);
S_d_s_crossL = sqrt(multiply(minreal(zpk(hsdHglob(23,1)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(hsdHglob(23,31)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(hsdHglobCloc(23,1)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(23,25)),[],1e-5),w,N_shot).^2);
S_d_s_crossT = sqrt(multiply(minreal(zpk(hsdHglob(23,2)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(hsdHglob(23,32)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(hsdHglobCloc(23,2)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(23,26)),[],1e-5),w,N_shot).^2);
S_d_s_crossV = sqrt(multiply(minreal(zpk(hsdHglob(23,3)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(hsdHglob(23,33)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(hsdHglobCloc(23,3)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(23,27)),[],1e-5),w,N_shot).^2);
S_d_s_crossR = sqrt(multiply(minreal(zpk(hsdHglob(23,4)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(hsdHglob(23,34)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(hsdHglobCloc(23,4)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(23,28)),[],1e-5),w,N_shot).^2);
S_d_s_crossY = sqrt(multiply(minreal(zpk(hsdHglob(23,6)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(hsdHglob(23,36)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(hsdHglobCloc(23,6)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(23,30)),[],1e-5),w,N_shot).^2);






S_d_h = sqrt(multiply(minreal(zpk(hsdHglob(47,5)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(hsdHglob(47,35)),[],1e-5),w,d_ISIP).^2+multiply(minreal(zpk(hsdHglobCloc(47,5)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(47,29)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCascP(47,47)),[],1e-5),w,N_ASC).^2+multiply(minreal(zpk(hsdHglobCascP(47,47)),[],1e-5),w,N_ASC).^2);
S_d_h_crossL = sqrt(multiply(minreal(zpk(hsdHglob(47,1)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(hsdHglob(47,31)),[],1e-5),w,d_ISIL).^2+multiply(minreal(zpk(hsdHglobCloc(47,1)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(47,25)),[],1e-5),w,N_shot).^2);
S_d_h_crossT = sqrt(multiply(minreal(zpk(hsdHglob(47,2)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(hsdHglob(47,32)),[],1e-5),w,d_ISIT).^2+multiply(minreal(zpk(hsdHglobCloc(47,2)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(47,26)),[],1e-5),w,N_shot).^2);
S_d_h_crossV = sqrt(multiply(minreal(zpk(hsdHglob(47,3)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(hsdHglob(47,33)),[],1e-5),w,d_ISIV).^2+multiply(minreal(zpk(hsdHglobCloc(47,3)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(47,27)),[],1e-5),w,N_shot).^2);
S_d_h_crossR = sqrt(multiply(minreal(zpk(hsdHglob(47,4)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(hsdHglob(47,34)),[],1e-5),w,d_ISIR).^2+multiply(minreal(zpk(hsdHglobCloc(47,4)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(47,28)),[],1e-5),w,N_shot).^2);
S_d_h_crossY = sqrt(multiply(minreal(zpk(hsdHglob(47,6)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(hsdHglob(47,36)),[],1e-5),w,d_ISIY).^2+multiply(minreal(zpk(hsdHglobCloc(47,6)),[],1e-5),w,N_shot).^2+multiply(minreal(zpk(hsdHglobCloc(47,30)),[],1e-5),w,N_shot).^2);




d_s_RMS = RMS(S_d_i,f,0.001,300);
d_h_RMS = RMS(S_d_e,f,0.001,300);

d_s_cross_RMSL = RMS(S_d_s_crossL,f,0.001,300);
d_h_cross_RMSL = RMS(S_d_h_crossL,f,0.001,300);

d_s_cross_RMST = RMS(S_d_s_crossT,f,0.001,300);
d_h_cross_RMST = RMS(S_d_h_crossT,f,0.001,300);

d_s_cross_RMSV = RMS(S_d_s_crossV,f,0.001,300);
d_h_cross_RMSV = RMS(S_d_h_crossV,f,0.001,300);

d_s_cross_RMSR = RMS(S_d_s_crossR,f,0.001,300);
d_h_cross_RMSR = RMS(S_d_h_crossR,f,0.001,300);

d_s_cross_RMSY = RMS(S_d_s_crossY,f,0.001,300);
d_h_cross_RMSY = RMS(S_d_h_crossY,f,0.001,300);


S_d_h_crossTOT = sqrt(S_d_h_crossL.^2+S_d_h_crossV.^2+S_d_h_crossT.^2+S_d_h_crossR.^2+S_d_h_crossY.^2);
S_d_s_crossTOT = sqrt(S_d_s_crossL.^2+S_d_s_crossV.^2+S_d_s_crossT.^2+S_d_s_crossR.^2+S_d_s_crossY.^2);


S_d_h_LSC = multiply(hsdHglobClsc(47,43),w,S_L0e);
S_d_s_LSC = multiply(hsdHglobClsc(23,43),w,S_L0i);

d_s_lsc_RMS = RMS(S_d_s_LSC,f,0.001,300);
d_h_lsc_RMS = RMS(S_d_h_LSC,f,0.001,300);

S_d_h_tot = sqrt(S_d_h.^2+S_d_h_crossTOT.^2+S_d_h_LSC.^2);
S_d_s_tot = sqrt(S_d_s.^2+S_d_s_crossTOT.^2+S_d_s_LSC.^2);
d_h_tot_RMS = RMS(S_d_h_tot,f,0.001,300);
d_s_tot_RMS = RMS(S_d_s_tot,f,0.001,300);



