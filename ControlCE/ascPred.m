%C =ss(cont);% Controller([100,10,15],'leadcut').K;
load('controller.mat');
%load('cs.mat');
%load('ch.mat');
%C = con;
%B1 = Hglobsh(23,29)*C;
%B2 = Hglobsh(47,59)*C;
%cs = con/abs(freqresp(B1,2*pi*50));
%ch = con/abs(freqresp(B2,2*pi*50));
%ch = zpk(-2*pi*[pair(nu1,phi1) pair(nu2,phi2) ],-2*pi*[pair(mu1,psi1) pair(mu2,psi2)],gh);
%ch = zpk(-2*pi*[pair(nu1,phi1) pair(nu1,phi1) 200],-2*pi*[0.001 pair(mu1,psi1) pair(mu1,psi1)],gh);
ch = Zpow(nu1,phi1,n1)*Zpow(nu2,phi2,n2)*Ppow(mu1,psi1,m1)*Ppow(mu2,psi2,m2)*gh;
%ch=zpk(-[pair(2,80) pair(15,50) pair(15,50) pair(300,15) ],-[ 5 pair(25,55) pair(25,55) pair(25,55) pair(25,75) pair(25,75) pair(25,75)],1);
%ch = zpk(-[],-[pair(15,15) pair(15,15) pair(15,15)],1);
%cs = zpk(-2*pi*[pair(nus1,phis1) pair(nus2,phis2)],-2*pi*[pair(mus1,psis1) pair(mus2,psis2)],gs);
cs = Zpow(nus1,phis1,ns1)*Zpow(nus2,phis2,ns2)*Ppow(mus1,psis1,ms1)*Ppow(mus2,psis2,ms2)*gs;%zpk(-2*pi*[pair(nus1,phis1) pair(nus1,phis1) 200],-2*pi*[0.001 pair(mus1,psis1) pair(mus1,psis1)],gs);
%cs = 1e7*zpk(-[pair(0.5,45) pair(1,75)],-[0.2 0.2 0.8 0.8  pair(15,75)],1);
%C1=C;
%C2=C;
%LPF
%BPF
%HPF

K3 = 1;

[f,~,~] = damp(minreal(Gglobred(8,10),[],false));

fm = f(end);


CascPred = zpk(zeros(20,16));
KascPmini = inv(bhqs.Spitch)*[[cs,0];[0 ch]]*bhqs.Spitch; 
%CascP(29,23) = KascPmini(1,1);
%CascP(29,47) = KascPmini(1,2);
%CascP(59,23) = KascPmini(2,1);
%CascP(59,47) = KascPmini(2,2);

Cii = zpk(KascPmini(1,1));
Cie = zpk(KascPmini(1,2));
Cei = zpk(KascPmini(2,1));
Cee = zpk(KascPmini(2,2));

A3ii = Hglob2red(8,10);
A2ii = Hglob2red(8,8);
A1ii = Hglob2red(8,6);

K1ii = zpk(A3ii/A1ii);%1/minreal(1/(K3*A3ii/A1ii),1e-12);
K2ii = zpk(A3ii/A2ii);%1/minreal(1/(K3*A3ii/A2ii),1e-12);

%A3ie = minreal(tf(Hglob2(47,29)));
%A2ie = minreal(tf(Hglob2(47,23)));
%A1ie = minreal(tf(Hglob2(47,17)));

%K1ie = K3*A3ie/A1ie;
%K2ie = K3*A3ie/A2ie;

%A3ee = minreal(tf(Hglob2(47,59)));
%A2ee = minreal(tf(Hglob2(47,53)));
%A1ee = minreal(tf(Hglob2(47,47)));

%K1ee = K3*A3ee/A1ee;
%K2ee = K3*A3ee/A2ee;


%A3ei = minreal(tf(Hglob2(23,59)));
%A2ei = minreal(tf(Hglob2(23,53)));
%A1ei = minreal(tf(Hglob2(23,47)));

%K1ei = K3*A3ei/A1ei;
%K2ei = K3*A3ei/A2ei;


%K2 = Kasc(fm,2);
%K1 = Kasc(fm,4);

%Cascp = tf(zeros(60,48));
CascPred(10,8) = Cii;%*K3*HP;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%CascP(23,23) = Cii*K2ii*BP;
%CascP(17,23) = Cii*K1ii*LP;

CascPred(10,16) = Cie;%*K3*HP;
%Cascp(23,23) = C*tf(G(23,29))/tf(G(23,23));%*BPF;
%CascP(23,47) = Cie*K2ie*BP;
%CascP(17,47) = Cie*K1ie*LP;

CascPred(20,16) = Cee;%*HP;
%Cascp(53,47) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%CascP(53,47) = K2ee*Cee*BP;
%CascP(47,47) = K1ee*Cee*LP;

CascPred(20,8) = Cei;%*HP;
%Cascp(53,47) = tf(G(23,23))/tf(G(23,29))*C;%*BPF;
%CascP(53,23) = K2ei*Cei*BP;
%CascP(47,23) = K1ei*Cei*LP;