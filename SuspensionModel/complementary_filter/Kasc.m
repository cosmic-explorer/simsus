function CustomFilter = Kasc(fb,n)
% Define the specifications
%fb = 50; % Transition frequency (Hz)
%Fs = 1000; % Sampling frequency
N = 2; % Order of the filter

% Define the normalized transition frequency
%Wn = fb / (Fs/2);

% Design a low-pass filter with a cutoff at fb
[b_lp, a_lp] = butter(N, fb, 'low', 's');

% Design a high-pass filter with a cutoff at fb
[b_hp, a_hp] = butter(N, fb, 'high', 's');

% Create transfer function objects
LPF = tf(b_lp, a_lp);
HPF = tf(b_hp, a_hp);

% Combine filters to create the desired response
% The low-pass filter will be flat up to fb
% The high-pass filter will be multiplied by f^2 to create the desired slope
s = tf('s');
CustomFilter = LPF + (s^n * HPF);

% Display frequency response
%figure;
%bode(CustomFilter);
%title('Custom Transfer Function with Gain 1 up to fb and f^2 slope after fb');

end

