function [Kopt,Fglob,Sp1red,Sp2red,Sp2invred,Sp1invred,Sp1,Sp1inv,Sp2,Sp2inv,I48,Dd] = some_var_fun(bhqs,Gglob,Cloc)
I48 = eye(48);
Kopt = ss(zeros(60,48)); %%
Kopt_mini = inv(bhqs.Spitch)*[[bhqs.ks,0];[0,bhqs.kh]]*bhqs.Spitch; 
Kopt(29,23) = Kopt_mini(1,1);
Kopt(29,47) = Kopt_mini(1,2);
Kopt(59,23) = Kopt_mini(2,1);
Kopt(59,47) = Kopt_mini(2,2);

%%YAW
Kopt_miniY = inv(bhqs.Syaw)*[[bhqs.ks,0];[0,bhqs.kh]]*bhqs.Syaw; 
Kopt(30,24) = Kopt_miniY(1,1);%/bhqs.Izz4;
Kopt(30,48) = Kopt_miniY(1,2);%/bhqs.Izz4;
Kopt(60,24) = Kopt_miniY(2,1);%/bhqs.Izz4;
Kopt(60,48) = Kopt_miniY(2,2);%/bhqs.Izz4;


%%PITCH+YAW

Fglob = feedback(Gglob,Kopt+Cloc);%inv(I48+Gglob*(Kopt+Cloc))*Gglob;
Spitch = bhqs.Spitch;
Syaw = bhqs.Syaw;
Sp1 = zeros(60,60);
Sp1(29,29) = Spitch(1,1);
Sp1(29,59) = Spitch(1,2);
Sp1(59,29) = Spitch(2,1);
Sp1(59,59) = Spitch(2,2);

Sp1(30,30) = Syaw(1,1);
Sp1(30,60) = Syaw(1,2);
Sp1(60,30) = Syaw(2,1);
Sp1(60,60) = Syaw(2,2);



% Sp1(23,23) = Spitch(1,1);
% Sp1(23,53) = Spitch(1,2);
% Sp1(53,23) = Spitch(2,1);
% Sp1(53,53) = Spitch(2,2);
% 
% Sp1(24,24) = Syaw(1,1);
% Sp1(24,54) = Syaw(1,2);
% Sp1(54,24) = Syaw(2,1);
% Sp1(54,54) = Syaw(2,2);
% 
% 
% 
% Sp1(29-2*6,29-2*6) = Spitch(1,1);
% Sp1(29-2*6,59-2*6) = Spitch(1,2);
% Sp1(59-2*6,29-2*6) = Spitch(2,1);
% Sp1(59-2*6,59-2*6) = Spitch(2,2);
% 
% Sp1(30-2*6,30-2*6) = Syaw(1,1);
% Sp1(30-2*6,60-2*6) = Syaw(1,2);
% Sp1(60-2*6,30-2*6) = Syaw(2,1);
% Sp1(60-2*6,60-2*6) = Syaw(2,2);
% 
% 
% 
% Sp1(29-3*6,29-3*6) = Spitch(1,1);
% Sp1(29-3*6,59-3*6) = Spitch(1,2);
% Sp1(59-3*6,29-3*6) = Spitch(2,1);
% Sp1(59-3*6,59-3*6) = Spitch(2,2);
% 
% Sp1(30-3*6,30-3*6) = Syaw(1,1);
% Sp1(30-3*6,60-3*6) = Syaw(1,2);
% Sp1(60-3*6,30-3*6) = Syaw(2,1);
% Sp1(60-3*6,60-3*6) = Syaw(2,2);














Sp2 = zeros(48,48);
Sp2(23,23) = Spitch(1,1);
Sp2(23,47) = Spitch(1,2);
Sp2(47,23) = Spitch(2,1);
Sp2(47,47) = Spitch(2,2);

Sp2(24,24) = Syaw(1,1);
Sp2(24,48) = Syaw(1,2);
Sp2(48,24) = Syaw(2,1);
Sp2(48,48) = Syaw(2,2);



% Sp2(17,17) = Spitch(1,1);
% Sp2(17,41) = Spitch(1,2);
% Sp2(41,17) = Spitch(2,1);
% Sp2(41,41) = Spitch(2,2);
% 
% Sp2(18,18) = Syaw(1,1);
% Sp2(18,42) = Syaw(1,2);
% Sp2(48,18) = Syaw(2,1);
% Sp2(42,42) = Syaw(2,2);
% 
% 
% 
% Sp2(23-2*6,23-2*6) = Spitch(1,1);
% Sp2(23-2*6,47-2*6) = Spitch(1,2);
% Sp2(47-2*6,23-2*6) = Spitch(2,1);
% Sp2(47-2*6,47-2*6) = Spitch(2,2);
% 
% Sp2(24-2*6,24-2*6) = Syaw(1,1);
% Sp2(24-2*6,48-2*6) = Syaw(1,2);
% Sp2(48-2*6,24-2*6) = Syaw(2,1);
% Sp2(48-2*6,48-2*6) = Syaw(2,2);
% 
% 
% 
% Sp2(23-3*6,23-3*6) = Spitch(1,1);
% Sp2(23-3*6,47-3*6) = Spitch(1,2);
% Sp2(47-3*6,23-3*6) = Spitch(2,1);
% Sp2(47-3*6,47-3*6) = Spitch(2,2);
% 
% Sp2(24-3*6,24-3*6) = Syaw(1,1);
% Sp2(24-3*6,48-3*6) = Syaw(1,2);
% Sp2(48-3*6,24-3*6) = Syaw(2,1);
% Sp2(48-3*6,48-3*6) = Syaw(2,2);








Sp2inv = zeros(48,48);
Spitchinv = inv(Spitch);
Syawinv = inv(Syaw);
Sp2inv(23,23) = Spitchinv(1,1);
Sp2inv(23,47) = Spitchinv(1,2);
Sp2inv(47,23) = Spitchinv(2,1);
Sp2inv(47,47) = Spitchinv(2,2);

Sp2inv(24,24) = Syawinv(1,1);
Sp2inv(24,48) = Syawinv(1,2);
Sp2inv(48,24) = Syawinv(2,1);
Sp2inv(48,48) = Syawinv(2,2);




% Sp2inv(17,17) = Spitchinv(1,1);
% Sp2inv(17,41) = Spitchinv(1,2);
% Sp2inv(41,17) = Spitchinv(2,1);
% Sp2inv(41,41) = Spitchinv(2,2);
% 
% Sp2inv(18,18) = Syawinv(1,1);
% Sp2inv(17,42) = Syawinv(1,2);
% Sp2inv(48,18) = Syawinv(2,1);
% Sp2inv(42,42) = Syawinv(2,2);
% 
% 
% Sp2inv(23-2*6,23-2*6) = Spitchinv(1,1);
% Sp2inv(23-2*6,47-2*6) = Spitchinv(1,2);
% Sp2inv(47-2*6,23-2*6) = Spitchinv(2,1);
% Sp2inv(47-2*6,47-2*6) = Spitchinv(2,2);
% 
% Sp2inv(24-2*6,24-2*6) = Syawinv(1,1);
% Sp2inv(24-2*6,48-2*6) = Syawinv(1,2);
% Sp2inv(48-2*6,24-2*6) = Syawinv(2,1);
% Sp2inv(48-2*6,48-2*6) = Syawinv(2,2);
% 
% 
% 
% Sp2inv(23-3*6,23-3*6) = Spitchinv(1,1);
% Sp2inv(23-3*6,47-3*6) = Spitchinv(1,2);
% Sp2inv(47-3*6,23-3*6) = Spitchinv(2,1);
% Sp2inv(47-3*6,47-3*6) = Spitchinv(2,2);
% 
% Sp2inv(24-3*6,24-3*6) = Syawinv(1,1);
% Sp2inv(24-3*6,48-3*6) = Syawinv(1,2);
% Sp2inv(48-3*6,24-3*6) = Syawinv(2,1);
% Sp2inv(48-3*6,48-3*6) = Syawinv(2,2);
% 





Sp1inv = zeros(60,60);
Sp1inv(29,29) = Spitchinv(1,1);
Sp1inv(29,59) = Spitchinv(1,2);
Sp1inv(59,29) = Spitchinv(2,1);
Sp1inv(59,59) = Spitchinv(2,2);

Sp1inv(30,30) = Syawinv(1,1);
Sp1inv(30,60) = Syawinv(1,2);
Sp1inv(60,30) = Syawinv(2,1);
Sp1inv(60,60) = Syawinv(2,2);





% Sp1inv(23,23) = Spitchinv(1,1);
% Sp1inv(23,53) = Spitchinv(1,2);
% Sp1inv(53,23) = Spitchinv(2,1);
% Sp1inv(53,53) = Spitchinv(2,2);
% 
% Sp1inv(24,24) =Syawinv(1,1);
% Sp1inv(24,54) = Syawinv(1,2);
% Sp1inv(54,24) = Syawinv(2,1);
% Sp1inv(54,54) = Syawinv(2,2);
% 
% 
% Sp1inv(29-2*6,29-2*6) = Spitchinv(1,1);
% Sp1inv(29-2*6,59-2*6) = Spitchinv(1,2);
% Sp1inv(59-2*6,29-2*6) = Spitchinv(2,1);
% Sp1inv(59-2*6,59-2*6) = Spitchinv(2,2);
% 
% Sp1inv(30-2*6,30-2*6) = Syawinv(1,1);
% Sp1inv(30-2*6,60-2*6) = Syawinv(1,2);
% Sp1inv(60-2*6,30-2*6) = Syawinv(2,1);
% Sp1inv(60-2*6,60-2*6) = Syawinv(2,2);
% 
% 
% 
% Sp1inv(29-3*6,29-3*6) = Spitchinv(1,1);
% Sp1inv(29-3*6,59-3*6) = Spitchinv(1,2);
% Sp1inv(59-3*6,29-3*6) = Spitchinv(2,1);
% Sp1inv(59-3*6,59-3*6) = Spitchinv(2,2);
% 
% Sp1inv(30-3*6,30-3*6) = Syawinv(1,1);
% Sp1inv(30-3*6,60-3*6) = Syawinv(1,2);
% Sp1inv(60-3*6,30-3*6) = Syawinv(2,1);
% Sp1inv(60-3*6,60-3*6) = Syawinv(2,2);
% 




Sp1invred = 0;%Sp1inv(input,input);
Sp1red = 0;%Sp1(input,input);

Sp2invred = 0;%Sp2inv(output,output);
Sp2red = 0;%Sp2(output,output);



Dd = zeros(48,48);
Dd(23,23) = bhqs.gi;
Dd(23,47) = 1;
Dd(47,23) = 1;
Dd(47,47) = bhqs.ge;


Dd(24,24) = bhqs.gi;
Dd(24,48) = 1;
Dd(48,24) = 1;
Dd(48,48) = bhqs.ge;




Dd = bhqs.Lcav/(1-bhqs.ge*bhqs.gi)*Dd;
