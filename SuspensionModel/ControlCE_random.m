tic
addpath A_plus\;
addpath BHQS\;
addpath complementary_filter\;
addpath controller\;
addpath darm_compute\;
addpath functions_utilities\;
addpath inputs\;
addpath SS_BHQS\;
addpath variables\;
run('ISI_inputs.m')
run('Noise_inputs.m')
%Execute Noise_inputs.m
run('freq.m')
run('load_inputs.m')
%run("createcomplementaryfilters.m");
run('three_complementary_filters.m');
save('variables\HP.mat',"HP")
save('variables\BP.mat',"BP")
save('variables\LP.mat',"LP")


%%%%%%%%%%




M10 = 117;
M20 = 83;
M30 = 100;
M40 = 100;
bhqs = BHQS(1.5e6,0,0,0,0);
%bhqs = BHQS(0,0,0,0,0);
save('variables\bhqs.mat','bhqs');

load('SS_BHQS\A_star_dic_ap01.mat')
load('SS_BHQS\B_star_dic_ap01.mat')
load('G_Aplus.mat','G_Aplus')


[A,B,C,D]=get_SSBHQS_M(bhqs,M10,M20,300-M10-M20);
Gperfect = minreal(ss(A,B,C,D),[],false);
A_res_full = zeros(48, 48);  % A is a 48x48 matrix
B_res_full = zeros(48, 30);  % B is a 48x30 matrix

A_res = A_star_dic_ap{1,1};
B_res = B_star_dic_ap{1,1};

fields_A = fieldnames(A_res);
fields_B = fieldnames(B_res);

% Loop through each field and display its content
%parpool();  % Opens a parallel pool with default settings
N=10;
d_e_tot_vec = [];
d_i_tot_vec = [];
S_P_cross_vec = [];
xsol_vec = [];
%parpool();
Gperfect = minreal(ss(A,B,C,D),[],false);
q = parallel.pool.DataQueue;
afterEach(q, @disp);
tic


structname = 'ss_bhqs_bs1_noasc';
save('variables\structname.mat','structname');
ss_bhqs.(structname) = struct();
save('variables\ss_bhqs.mat','ss_bhqs');
ss_bhqs.(structname) = struct();



for i = 1:N
    fprintf('i')
    disp(i)
    ss_name = strcat(structname,'_',num2str(i));
    save('variables\ss_name.mat','ss_name');
    

    
 

    
    
    
    send(q, ['Message from worker ' num2str(i)]);
    fieldName_A = fields_A{i};
    A_res_i = A_res.(fieldName_A);
    fieldName_B = fields_B{i};
    B_res_i = B_res.(fieldName_B);
    A_res_full = zeros(48, 48);
    A_res_full(25:48, 1:24) = A_res_i;
    B_res_full = zeros(48, 30);% Lower left block of A is A_star_BHQS
    B_res_full(25:30,1:6) = B_res_i(1:6,1:6).';
    %B_star(1:6,1:6)
    G = minreal(ss(A,B,C,D));%+A_res_full,B+B_res_full,C,D),[],false);
    
    %G1 = ss(A+A_res_full,B+B_res_full,C,D);
    %G = G_Aplus;
    G.InputName={'x0','y0','z0','R0','P0','Y0','Fx1','Fy1','Fz1','MR1','MP1','MY1','Fx2','Fy2','Fz2','MR2','MP2','MY2','Fx3','Fy3','Fz3','MR3','MP3','MY3','Fx4','Fy4','Fz4','MR4','MP4','MY4'};
    G.OutputName = {'x1','y1','z1','R1','P1','Y1','x2','y2','z2','R2','P2','Y2','x3','y3','z3','R3','P3','Y3','x4','y4','z4','R4','P4','Y4'};

    Gglob = [[G,zeros(24,30)];[zeros(24,30),G]];
    Gglob.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
    Gglob.OutputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};

    
    input =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47    49    53    55    59];
    output =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47];
    
    Gglobred = Gglob(output,input);
   
    save('variables\Gglob.mat',"Gglob","G",'Gglobred');


%run('Clocal.m')

Gperf = [[Gperfect,zeros(24,30)];[zeros(24,30),Gperfect]];
run('some_var.m')
Kglob = feedback(Gperf,Kopt);
Cloc = Clocal_fun(G,Gglob,Kglob);
    

[Kopt,Fglob,Sp1red,Sp2red,Sp2invred,Sp1invred,Sp1,Sp1inv,Sp2,Sp2inv,I48,Dd] = some_var_fun(bhqs,Gglob,Cloc);
run('Lsc.m');
[Clsc,M] = Lsc_fun(Fglob);
ClscM = Clsc*M;

save('variables\Cloc.mat','Cloc');%,'Clocred')
save('variables\some_var.mat','Fglob','Sp2','Sp2inv','Sp1','Sp1inv','I48','Kopt','Sp2red','Sp2invred','Sp1red','Sp1invred','ClscM','Dd');%,'ClscMred');%'Koptred',



    
    Koptred = Kopt(input,output);
    Clocred = Cloc(input,output);
    ClscM = Clsc*M;
    ClscMred = ClscM(input,output);
    Mred = M(output,output);
    
    Hglob2 = feedback(Gglob,Kopt+Cloc+Clsc*M);%inv(I48+Gglob*(Kopt+Cloc+Clsc*M))*Gglob;
    Hglob2red = feedback(Gglobred,Koptred+Clocred+ClscMred);
    
 
    save('variables\Lsc_var.mat','Hglob2','M','Clsc','Hglob2red','Mred');
    Mtot = 400-100;
    
    nu1 = 0.1;
    phi1 = 45;
    nu2 = 1;
    phi2 = 45;
    gh = 3e6*1/50;
    n1 = 1;
    n2 = 1;
    m1 = 1;
    m2 = 1;
    mu1 = 15;
    psi1 = 45;
    mu2 = 1;
    psi2 = 45;
    
    nus1 = 1;
    phis1 = 45;
    nus2 = 1;
    phis2 = 45;
    gs = 1e1*1;
    
    mus1 = 15;
    psis1 = 45;
    mus2 = 1;
    psis2 = 45;
    warning('off', 'all');
    
    %cont_varh = [nu1,nu2,phi1,phi2,mu1,mu2,psi1,psi2,gh];
    cont_varh=[nu1,phi1,n1,nu2,phi2,n2,mu1,psi1,m1,mu2,psi2,m2,gh];
   % lb = [1,7,3,7,1,7,-30,0.1,1,1,1,7,7,7,7,0.2,0.2];%,-30];%,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0];  % Lower bounds for variables
   % ub = [5,83,8,83,5,83,+55,10,30,30,30,83,83,83,83,20,20];%,+25];%,log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf];   % Upper bounds for variables
    lb = [0.3,.2,.2,-5,.1,.3,.3,7,.1,.1,-5];
    ub = [5,5,5,10,2,3,3,83,3,3,10];
    %lb = [log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0];  % Lower bounds for variables
    %ub = [log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf,log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf];   % Upper bounds for variables
    
    
    %cont_vars = [nus1,nus2,phis1,phis2,mus1,mus2,psis1,psis2,gs];
    %cont_vars=[nus1,phis1,mus1,psis1,gs];
    sus_var = [M10,M20];
    warning('off', 'Control:analysis:AccuracyLoss');
    %initial_darm 
    %darm_initial = optim_initial();
    
    x0 = cont_varh;%,cont_vars];
    %options = optimoptions("fmincon",'Display','iter-detailed',...
    %    "Algorithm","interior-point",...
    %    "EnableFeasibilityMode",false,...
    %    "SubproblemAlgorithm","cg", 'Display','iter-detailed');
    %options = optimoptions('lsqnonlin','Disp m lay','iter-detailed');%,'Algorithm','sqp');
    %options.ConstraintTolerance = 1e-8;
    %options.OptimalityTolerance = 1e-8;
    %options.StepTolerance = 1e-8;
    %options.FiniteDifferenceStepSize = 0.03                                                                                                                                                                                                                                                           ;
    %options.FiniteDifferenceType="central";
    %options.FunctionTolerance = 1e-8;
    %options.MaxFunctionEvaluations = 2000;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    optim([1 70.3264829247942 3.00108611227825 22.6700889728774 1.00005631546766 22.5200296570119 20.3371166737604 4.16995695017463 1.50868588346207 12.013978605982 1.69180802114991 28.5880923163304 33.2822090063226 81.7897024945818 14.6810636339675])



%   parpool();
%    options = optimoptions('particleswarm', 'SwarmSize', 500, 'UseParallel', true,'UseVectorized',true,'MaxIterations', 100, ...  % Optional: Maximum number of iterations
%        'OutputFcn', @ps_outputfcn);
%    options.FunctionTolerance = 1e-3;
    %options.MaxIterations = 6;
    %options.OptimalityTolerance = 1e-3;
    %options.StepTolerance = 1e-3;
    %x0 = [-3, 3];   % Initial guess
    
%    [xsol, fvalsol, exitflag, output, points] = particleswarm(@optim_vec, 11, lb, ub, options);
    %toc
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%[x,f_val] = fmincon(@optim,x0,[0,0,0,0,0,0,0,0,0,1,1],Mtot,[],[],[0,0,-inf,-inf,0,0,-inf,-inf,0,0,0],[+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,Mtot,Mtot],[],options);%@localdamp,options);
    %%%%%%[x,f_val] = fmincon(@optim,x0,[0,0,0,0,0,0,0,0,0,0],Mtot,[],[],[0,-inf,0,-inf,0,0,-inf,0,-inf,0],[+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf],[],options);%@localdamp,options);
    
    %%[x,f_val] = lsqnonlin(@optim,x0,[-inf,-inf,-inf,-inf,-inf,-inf,0,0],[+inf,+inf,+inf,+inf,+inf,+inf,Mtot,Mtot],[0,0,0,0,0,0,1,1],Mtot,[],[],[],options);%@localdamp,options);
    
    
    
    %%[x,fval] = fminunc(@optim,x0);
%    figure()
%    hold on
    %%plot_noise(x);
    %%plot_noise(x0);
%    set(gca, 'YScale', 'log')
%    set(gca, 'XScale', 'log')
    
%   delete(gcp);  % Close the parallel pool
    %%A = readmatrix('SS_BHQS\A_bhqs');
    %%B = readmatrix('SS_BHQS\B_bhqs');
    %%C = readmatrix('SS_BHQS\C_bhqs');
    %%D = readmatrix('SS_BHQS\D_bhqs');


    [d_e_tot,d_i_tot] = optim_getval([1 70.3264829247942 3.00108611227825 22.6700889728774 1.00005631546766 22.5200296570119 20.3371166737604 4.16995695017463 1.50868588346207 12.013978605982 1.69180802114991 28.5880923163304 33.2822090063226 81.7897024945818 14.6810636339675]);
    d_e_tot_vec=[d_e_tot_vec;d_e_tot];
    d_i_tot_vec=[d_i_tot_vec;d_i_tot];
 %   S_P_cross_vec=[S_P_cross_vec;S_P_cross];
 %   xsol_vec = [xsol_vec;xsol];

end
toc
    function stop = ps_outputfcn(optimValues,y,states)
        % Extract relevant information from optimValues
        disp(optim(optimValues.bestx))
        iteration = optimValues.iteration;
        bestfval = optimValues.bestfval;
        bestx = optimValues.bestx;
        
        % Display current iteration, best objective value, and best position
        fprintf('Iteration: %d, Best Objective: %.4f, Best Solution: %s\n', ...
            iteration, bestfval, mat2str(bestx));
        
        % Check for stopping condition (optional)
        stop = false;  % Set to true to stop optimization early
    end

