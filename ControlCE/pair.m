function c = pair(g,phase)
%PAIR Summary of this function goes here
%   Detailed explanation goes here
cplx1 = g*(cos(phase/180*pi)+sin(phase/180*pi)*1i);
cplx2 = g*(cos(phase/180*pi)-sin(phase/180*pi)*1i);
c = [cplx1,cplx2];
end

