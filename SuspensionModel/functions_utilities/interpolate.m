function d_isi = interpolate(freq_ISI, data_ISI, freq)
    d_isi = zeros(size(freq));
    for j = 1:numel(freq)
        for i = 1:numel(freq_ISI)-1
            if freq(j) > freq_ISI(i) && freq(j) <= freq_ISI(i+1)
                % Perform interpolation
                d_isi(j) = data_ISI(i) * (data_ISI(i+1)/data_ISI(i))^((log(freq(j)/freq_ISI(i))) / (log(freq_ISI(i+1)/freq_ISI(i))));
            end
        end
    end
end

