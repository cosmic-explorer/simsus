%function S_L0 = NoiseBudgetL0(Hglob)

L0e = zeros(60,1);
L0e(1) = 1;

L0i = zeros(60,1);
L0i(31) = 1;

%%particular TF

%Effect on i
Hi = M*Hglob*L0i;
He = M*Hglob*L0e;
Hii = minreal(Hi(43),[],false);
%Hei = minreal(He(23),[],false);

n1i = zeros(48,1);
n1i(1) = 1;


n1e = zeros(48,1);
n1e(25) = 1;


Hn = M*Hglob*Cloc;

Hn1i = Hn*n1i;
Hn1e = Hn*n1e;

Hn1ii = minreal(Hn1i(43),[],false);
%Hn1ei = minreal(Hn1e(23),[],false);


n2i = zeros(48,1);
n2i(7) = 1;


n2e = zeros(48,1);
n2e(31) = 1;

Hn2i = Hn*n2i;
Hn2e = Hn*n2e;
Hn2ii = minreal(Hn2i(43),[],false);
%Hn2ei = minreal(Hn2e(23),[],false);

%effect on e


%Hie = minreal(Hi(47),[],false);
Hee = minreal(He(43),[],false);

%Hn1ie = minreal(Hn1i(47),[],false);
Hn1ee = minreal(Hn1e(43),[],false);

%Hn2ie = minreal(Hn2i(47),[],false);
Hn2ee = minreal(Hn2e(43),[],false);

S_Lisii =  multiply(Hii,w,d_ISIL);
S_Lisie =  multiply(Hee,w,d_ISIL);
S_Lni = multiply(Hn1ii,w,N_shot);
S_Lne = multiply(Hn1ee,w,N_shot);



S_dL = multiply(Hii,w,d_ISIL).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hee,w,d_ISIL).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2;
%S_L_e2 = multiply(Hee,w,d_ISIL).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2;

S_L0 = sqrt(S_dL);
%end



%Iglob = M*Hglob;
%Iglob*