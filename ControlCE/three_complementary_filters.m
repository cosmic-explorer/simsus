%clear;close all;clc;

%%IntializeLaplacevariable
 s=zpk('s');

 freqs=logspace(-2,3,1000);
f1 = 0.1;
f2=10;
%%DesignoftheWeightingFunctions
  W1=generateWF('n',2,'w0',2*pi*f1, 'G0',1/10,'Ginf',1000,'Gc',0.5);
  W2=0.22*(1+s/2/pi/f1)^2/(sqrt(1e-4)+s/2/pi/f1)^2*(1+s/2/pi/f2)^2/(1+s/2/pi/1000)^2;
  W3=generateWF('n',3,'w0',2*pi*f2, 'G0',1000,'Ginf',1/10,'Gc',0.5);

 %%Inversemagnitudeoftheweightingfunctions
  figure;
  hold on;
  set(gca,'ColorOrderIndex',1)
  plot(freqs,1./abs(squeeze(freqresp(W1,freqs, 'Hz'))), '--', 'DisplayName', '$|W_1|^{-1}$');
  set(gca,'ColorOrderIndex',2)
  plot(freqs,1./abs(squeeze(freqresp(W2,freqs, 'Hz'))),'--', 'DisplayName', '$|W_2|^{-1}$');
  set(gca,'ColorOrderIndex',3)
  plot(freqs,1./abs(squeeze(freqresp(W3,freqs, 'Hz'))),'--', 'DisplayName', '$|W_3|^{-1}$');
  set(gca,'XScale', 'log');set(gca, 'YScale', 'log');
  xlabel('Frequency[Hz]');ylabel('Magnitude');
  hold off;
  xlim([freqs(1),freqs(end)]);ylim([2e-4,1.3e1])
  leg=legend('location', 'northeast', 'FontSize',8);
  leg.ItemTokenSize(1)=18;

 %%Generalized plantforthesynthesisof3complementaryfilters
 P=[W1 -W1 -W1;
 0 W2 0;
 0 0 W3;
 1 0 0];

 %%StandardH-InfinitySynthesis
 [H,~,gamma,~]=hinfsyn(P,1,2,'TOLGAM',0.001, 'METHOD', 'ric', 'DISPLAY', 'on');

 %%Synthesized H2andH3filters

 BP=tf(H(1));
 HP=tf(H(2));

 %%H1isdefinedasthecomplementaryfilterofH2andH3
 LP=1-BP-HP;
 if any(real(pole(LP)/(2*pi)) > 1e-6) 
         disp('*** POSTIVE PLANE POLES ***')%,'fontsize',40,'color',[1 0 0],'fontname','Madfont Thorns')
 elseif any(real(pole(BP)/(2*pi)) > 1e-6) 
         disp('*** POSTIVE PLANE POLES ***')%,'fontsize',40,'color',[1 0 0],'fontname','Madfont Thorns')

 elseif any(real(pole(HP)/(2*pi)) > 1e-6) 
         disp('*** POSTIVE PLANE POLES ***')%,'fontsize',40,'color',[1 0 0],'fontname','Madfont Thorns')

 end
 %%%Bodeplotoftheobtainedcomplementaryfilters
 %figure;
 %tiledlayout(3,1, 'TileSpacing', 'None', 'Padding','None');
%
 %%Magnitude
 %ax1=nexttile([2,1]);
 %hold on;
 %set(gca,'ColorOrderIndex',1)
 %plot(freqs,1./abs(squeeze(freqresp(W1,freqs, 'Hz'))),'--', 'DisplayName', '$|W_1|^{-1}$');
 %set(gca,'ColorOrderIndex',2)
 %plot(freqs,1./abs(squeeze(freqresp(W2,freqs, 'Hz'))),'--', 'DisplayName', '$|W_2|^{-1}$');
 %set(gca,'ColorOrderIndex',3)
 %plot(freqs,1./abs(squeeze(freqresp(W3,freqs, 'Hz'))),'--', 'DisplayName', '$|W_3|^{-1}$');
 %set(gca,'ColorOrderIndex',1)
 %plot(freqs,abs(squeeze(freqresp(H1,freqs, 'Hz'))), '-','DisplayName', '$H_1$');
 %set(gca,'ColorOrderIndex',2)
 %plot(freqs,abs(squeeze(freqresp(H2,freqs, 'Hz'))), '-','DisplayName', '$H_2$');
 %set(gca,'ColorOrderIndex',3)
 %plot(freqs,abs(squeeze(freqresp(H3,freqs, 'Hz'))), '-', 'DisplayName','$H_3$');
 %set(gca,'XScale', 'log');set(gca, 'YScale', 'log');
 %hold off;
 %set(gca,'XScale', 'log');set(gca, 'YScale', 'log');
 %ylabel('Magnitude');
 %set(gca,'XTickLabel',[]);
 %ylim([1e-4,20]);
 %leg=legend('location', 'northeast', 'FontSize',8);
 %leg.ItemTokenSize(1)=18;
%
 %%Phase
 %ax2=nexttile;
 %hold on;
 %set(gca,'ColorOrderIndex',1)
 %plot(freqs,180/pi*phase(squeeze(freqresp(H1,freqs, 'Hz'))));
 %set(gca,'ColorOrderIndex',2)
 %plot(freqs,180/pi*phase(squeeze(freqresp(H2,freqs, 'Hz'))));
 %set(gca,'ColorOrderIndex',3)
 %plot(freqs,180/pi*phase(squeeze(freqresp(H3,freqs, 'Hz'))));
 %hold off;
 %xlabel('Frequency[Hz]');ylabel('Phase[deg]');
 %set(gca, 'XScale', 'log');
 %yticks([-180:90:180]);ylim([-220,220]);
%
 %linkaxes([ax1,ax2],'x');
 %xlim([freqs(1),freqs(end)])