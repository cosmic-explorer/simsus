function darm_RMS = optim(x)
%tic
%close all%;clc;
%tic
load('LP.mat');
load('BP.mat');
load('HP.mat');
load('Gglob.mat');
load('BHQS.mat')
load('Cloc.mat')
load("some_var.mat")
load('Lsc_var.mat')
%load('darm_initial.mat')
warning('off', 'all');

%[darm1,darm2,darm]
%[alpha,beta,M1,M2,M3]= deal(x(1),x(2),x(3),x(4),x(5));
%[nu1,nu2,phi1,phi2,mu1,mu2,psi1,psi2,gh,nus1,nus2,phis1,phis2,mus1,mus2,psis1,psis2,gs]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10),x(11),x(12),x(13),x(14),x(15),x(16),x(17),x(18));
%[nu1,phi1,mu1,psi1,gh,nus1,phis1,mus1,psis1,gs]=
%deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10));\
%[nu1,phi1,n1,nu2,phi2,n2,mu1,psi1,m1,mu2,psi2,m2,gh,nus1,phis1,ns1,nus2,phis2,ns2,mus1,psis1,ms1,mus2,psis2,ms2,gs]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10),x(11),x(12),x(13),x(14),x(15),x(16),x(17),x(18),x(19),x(20),x(21),x(22),x(23),x(24),x(25),x(26));
%[a,phi1,b,phi2,c,phi3,gh]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7));%,x(8));%,x(9),x(10),x(11),x(12),x(13),x(14));
%[a,phi1,b,phi2,c,phi3,lgh,hgh,gh,as,bs,cs,ds,phis1,phis2,phis3,phis4,lgs,hgs,gs]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10),x(11),x(12),x(13),x(14),x(15),x(16),x(17),x(18),x(19),x(20));
%[a,phi1,b,lgh,hgh,gh,as,bs,csoft,phics,lgs,hgs,gs]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10),x(11),x(12),x(13));%,x(14),x(15),x(16),x(17),x(18),x(19),x(20));
[b,lgh,hgh,gh,as,bs,csoft,phics,lgs,hgs,gs]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),x(9),x(10),x(11));%,x(12),x(13));%,x(14),x(15),x(16),x(17),x(18),x(19),x(20));

%[gh,gs] = deal(x(1),x(2));
%n1 = round(n1);
%n2 = round(n2);
%m1 = round(m1)+n1;
%m2 = round(m2)+n2;
%nu1 = exp(nu1);
%nu2 = exp(nu2);
%mu1 = exp(mu1); 
%mu2 = exp(mu2);

%ns1 = round(ns1);
%ns2 = round(ns2);
%ms1 = round(ms1)+ns1;
%ms2 = round(ms2)+ns2;
%nus1 = exp(nus1);
%nus2 = exp(nus2);
%mus1 = exp(mus1);
%mus2 = exp(mus2);

%toc

%disp(Ktop);
%K=0;
%Mtot=300;
%bhqs = BHQS();
%Kopt_eig = bhqs.Kopt;
%S = bhqs.S;

%Ktop=0;

warning('off', 'Control:analysis:AccuracyLoss');
%LOCAL CONTROLER
%run('Clocal.m')
%Cloc = Clocal(Gglob);
%run("createcomplementaryfilters.m");
%ASC CONTROLER



%tic
Hglobsh = Sp2*Hglob2*Sp1inv;
Hglobsh.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
Hglobsh.OutputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};

%Hglobshred = Sp2red*Hglob2red*Sp1invred;
%toc
%tic
%run('ascPred.m');
run('ascP.m');
run('ascY.m');
%toc



%CAVITY MODEL 




%[A,B,C,D]=get_SSBHQS_M(bhqs,100,100,100,100);



% FULL SYSTEM
%tic
%Pglob = feedback(Gglob,Kopt+Cloc+CascP);
%Hglob = feedback(Gglob,Kopt+Cloc+Clsc*M+CascP);%inv(I48+Gglob*(Kopt+Cloc+Clsc*M+CascP))*Gglob;

%Pglobred = feedback(Gglob,Kopt+Cloc+CascP);
%Hglobred = feedback(Gglobred,Koptred+Clocred+ClscMred+CascPred);%inv(I48+Gglob*(Kopt+Cloc+Clsc*M+CascP))*Gglob;

Hglob = feedback(Gglob,Kopt+Cloc+ClscM+CascP+CascY);
%Hglobbis = feedback(Hglob2,CascP);
%Hglob = inv(I48+Gglob*(Kopt+Cloc+ClscM+CascP))*Gglob;

%Pglobred = feedback(Gglobred,Clocred);
%Pglob = feedback(Gglob,Cloc);
%toc
%tic
%Hglobfsh = Sp2*Hglob*Sp1inv;
%Hglobfshred = Sp2red*Hglobred*Sp1invred;
Hglobfsh = Sp2*Hglob*Sp1inv;
Hglobfsh.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
Hglobfsh.OutputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};

%toc
%CONTROL_OPT Summary of this function goes here
%   Detailed explanation goes here
%tic
run('get_TF.m')
run('freq.m')
run('ISI_inputs.m')
run('Noise_inputs.m')
run('load_inputs.m')
%toc
%tic
%Execute Noise_inputs.m


%[GP02Ptop,GF2Ptop] = closeloop(HP02Ptop,HF2Ptop,Ctop);
%[GPtop2Puim,GF2Puim] = closeloop(HPtop2Puim,HF2Puim,Cuim);


%Cavity tf from angle without cavity to with cavity

%HF2Ptst_mat = HF2Ptst*eye(2); 
%Kdamphs = [[KdampS,0];[0,KdampH]];


%Hcav = minreal(zpk(inv((eye(2)+HF2Ptst*inv(S)*(Kopt_eig+Kdamphs)*S))));

%H_ISIi_i = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,1));%+Hcav(1,2));%+Hcav(2,1));
%H_ISIi_e = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,1));%+Hcav(2,2));

%H_ISIe_i = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,2));%+Hcav(1,2));%+Hcav(2,1));
%H_ISIe_e = HP02Ptst/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,2));

%H_n_topi_i = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,1));%+Hcav(1,2));
%H_n_topi_e = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,1));%+Hcav(2,2));

%H_n_tope_i = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(1,2));%+Hcav(1,2));
%H_n_tope_e = Ctop*HPtop2Ptst*HF2Ptop/(1+Ctop*HF2Ptop)/(1+Cuim*HF2Puim)*(Hcav(2,2));%+Hcav(2,2));

%H_n_uimi_i = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(1,1));%+Hcav(1,2));
%H_n_uimi_e = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(2,1));%+Hcav(2,2));

%H_n_uime_i = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(1,2));%+Hcav(1,2));
%H_n_uime_e = Cuim*HPuim2Ptst*HF2Puim/(1+Cuim*HF2Puim)*(Hcav(2,2));%+Hcav(2,2));

%S_theta_i = sqrt(multiply(Hii,w,d_ISIP).^2+multiply(Hei,w,d_ISIP).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn1ei,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hn2ei,w,N_shot).^2);
%S_theta_e = sqrt(multiply(Hie,w,d_ISIP).^2+multiply(Hee,w,d_ISIP).^2+multiply(Hn1ie,w,N_shot).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ie,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2);


%S_d_i =abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*S_theta_i.^2+S_theta_e.^2) ;
%S_d_e =abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(S_theta_i.^2+bhqs.gi^2*S_theta_e.^2) ;
%S_theta_e = sqrt(multiply(H_ISI_e,w,d_ISIP).^2+multiply(H_n_top_s,w,N_shot).^2+multiply(H_n_uim_s,w,N_shot).^2);
%S_theta_e = sqrt(multiply(H_ISIi_e,w,d_ISIP).^2+multiply(H_ISIe_e,w,d_ISIP).^2+multiply(H_n_topi_e,w,N_shot).^2+multiply(H_n_tope_e,w,N_shot).^2+multiply(H_n_uimi_e,w,N_shot).^2+multiply(H_n_uime_e,w,N_shot).^2);
%theta_i_RMS = RMS(S_theta_i);
%theta_e_RMS = RMS(S_theta_e);

%d_i_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_RMS^2+theta_e_RMS^2);
%d_e_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_e_RMS^2+bhqs.gi^2*theta_e_RMS^2);


%theta_i_RMS = RMS([multiply(Hii,w,d_ISIP);multiply(Hei,w,d_ISIP);multiply(Hn1ii,w,N_shot);multiply(Hn1ei,w,N_shot);multiply(Hn2ii,w,N_shot);multiply(Hn2ei,w,N_shot)],f,.1,50);
%theta_e_RMS = RMS([multiply(Hie,w,d_ISIP);multiply(Hee,w,d_ISIP);multiply(Hn1ie,w,N_shot);multiply(Hn1ee,w,N_shot);multiply(Hn2ie,w,N_shot);multiply(Hn2ee,w,N_shot)],f,.1,50);

%darm = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi)*((bhqs.gi*bhqs.R^2+2*bhqs.R+bhqs.ge))*theta_s_RMS*S_theta_s+abs((bhqs.ge*bhqs.R^2-2*bhqs.R+bhqs.gi))*theta_h_RMS*S_theta_h);
%darm = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*((abs(bhqs.ge))*theta_i_RMS*S_theta_i+abs(bhqs.gi)*theta_e_RMS*S_theta_e);
%darm1 = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*((abs(bhqs.ge))*theta_i_RMS*S_theta_i);
%darm2 = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*abs(bhqs.gi)*theta_e_RMS*S_theta_e;
%darm = sqrt(2*(d_i_RMS^2*S_theta_i.^2+d_e_RMS^2*S_theta_e.^2));
%S_P = NoiseBudgetP(Hglob);
%toc
%tic
%toc
warning('off', 'Control:analysis:AccuracyLoss');
%run('NoiseBudgetP.m');
%tic
run('NoiseBudgetL0.m')

run('NoiseBudgetP_v2.m');
run('NoiseBudgetY_v2.m');
%toc
%tic
%run('NoiseBudgetL0red.m')

%toc
%tic
darm = sqrt(S_P_tot.^2+S_L0.^2);
%darm_RMS = -RMS_log(darm,f,10,30);
%tic
%h_stable = pole(Hglobfsh(47,59));%,1e-6,false));
%s_stable = pole(Hglobfsh(23,29));%,1e-6,false));
h_stable = pole(minreal(zpk(Hglobsh(47,59)/(1+Hglobsh(47,59)*ch)),[],1e-2));%,1e-6,false));
s_stable = pole(minreal(zpk(Hglobsh(23,29)/(1+Hglobsh(23,29)*cs)),[],1e-2));%,1e-6,false));

%unstable_h_index = (h_stable > 1e-3) & (h_stable<1e3);
unstable_h_index = (h_stable > 1e-2) & (h_stable<1e2);
unstable_h = h_stable(unstable_h_index);


unstable_s_index = (s_stable > 1e-3) & (s_stable<1e3);
unstable_s = s_stable(unstable_s_index);

stability_penalty = (sum(real(unstable_s))+sum(real(unstable_h)));

run('getQ.m');
%[fr,z,q]=damp(minreal(Hglobfsh(47,59),1e-6,false));

Qm = Q_factors-15;

Qm_index = Qm > 0;
Qm_large = Qm(Qm_index);


stability_marge_ch =  20*log(getPeakGain(1/(1+Hglobsh(47,59)*ch)))/log(10);
stability_marge_cs =  20*log(getPeakGain(1/(1+Hglobsh(23,29)*cs)))/log(10);


stab_marge_pen = 0;
if stability_marge_ch>10
    stab_marge_pen = stab_marge_pen+(stability_marge_ch-10)*1e3;
end


if stability_marge_cs>10
    stab_marge_pen = stab_marge_pen+(stability_marge_cs-10)*1e3;
end

%toc
%tic
%darm_RMS =
%log(RMS(darm,f,10,30))+stability_penalty*1e6+sum(Qm_large)*1e3;\
darm_RMS = RMS_log(darm,f,10,30)+stability_penalty*1e6+sum(Qm_large)*1e3+stab_marge_pen;
%toc
%disp(string('Qm'+Qm_large));
%disp(real(unstable_s));
%disp(real(unstable_h));
%toc
%disp('DARM= '+string(darm_RMS));
%disp('time elapsed '+string(toc));

%disp(Ktop);
%disp(darm_RMS);
%res_top_ISI = multiply(GL02Ltop,w,d_ISIL);
%res_noise_top = multiply(Ktop*GF2Ltop,w,N_shot);

%res_top = res_top_ISI+res_noise_top;

%darmL = multiply(HLtop2Ltst,w,res_top+res_noise);
%darm_RMS = -RMS_log(darmL,f,10,30) ;

%toc
%toc




load("ss_bhqs.mat",'ss_bhqs');
ss_tolname = load('structname.mat','structname');
ss_tolname_id = load('ss_name.mat','ss_name');

ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name) = struct();

ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Gglob = Gglob;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Fglob = Fglob;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Hglob = Hglob;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Hglob2 = Hglob2;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Hglobfsh = Hglobfsh;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Hglobsh = Hglobsh;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Hglobsh = Hglobsh;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Cloc = Cloc;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Clsc = Clsc;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).ClscM = ClscM;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).M = M;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).CascP = CascP;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).ch = ch;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).cs = cs;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Kopt = Kopt;

ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Sp1 = Sp1;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Sp1inv = Sp1inv;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Sp2 = Sp2;
ss_bhqs.(ss_tolname.structname).(ss_tolname_id.ss_name).Sp2inv = Sp2inv;

save("variables\ss_bhqs.mat",'ss_bhqs');

end