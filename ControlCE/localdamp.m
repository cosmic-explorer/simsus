function  [cineq,ceq]= localdamp(x)
%[K,Tau,M1,M2]= deal(x(1),x(2),x(3),x(4));
[Ktop,Tautop,Kuim,Tauuim,KdampH,KdampS,M1,M2]= deal(x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8));
Mtot=300;
%K=0;
bhqs = BHQS();
Ctop = Controller([Ktop,10,Tautop],'leadcut').K;
Cuim = Controller([Kuim,10,Tauuim],'lead').K;

%[A,B,C,D]=get_SSBHQS_M(bhqs,100,100,100,100);
[A,B,C,D]=get_SSBHQS_M(bhqs,M1,M2,Mtot-M1-M2);
G = ss(A,B,C,D);

%CONTROL_OPT Summary of this function goes here
%   Detailed explanation goes here
run('get_TF.m')
run('freq.m')
run('ISI_inputs.m')
run('Noise_inputs.m')
%Execute Noise_inputs.m
run('load_inputs.m')

Gclose = 1/(1+Ctop*HF2Ltop);
%Q_factor = [];
Q_con = [];
f_con = [];
Qm = 30;
fm = 15;
ceq=0;
[fw,zeta,p] = damp(Gclose);

for i=1:length(zeta)
    if zeta(i)~=0
        Q_con = [Q_con, -abs(p(i))/(2*real(p(i)))-Qm];
        f_con = [f_con,fw(i)/2/pi-fm];
    end

end

cineq = [Q_con, f_con];