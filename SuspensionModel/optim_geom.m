function [fvalsol] = optim_geom(geom)
%OPTIM_GEOM Summary of this function goes here

M1 = geom(1);
M2 = geom(2);
M3 = 300-M1-M2;
bhqs = BHQS();
[A,B,C,D]=get_SSBHQS_M(bhqs,M1,M2,M3);

G = minreal(ss(A,B,C,D),[],false);
Gglob = [[G,zeros(24,30)];[zeros(24,30),G]];

input =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47    49    53    55    59];
output =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47];

Gglobred = Gglob(output,input);
save('Gglob.mat',"Gglob","G",'Gglobred');

%run('Clocal.m')
Cloc = Clocal_fun(G,Gglob);
%run('some_var.m')
[Kopt,Fglob,Sp1red,Sp2red,Sp2invred,Sp1invred,Sp1,Sp1inv,Sp2,Sp2inv,I48] = some_var_fun(bhqs,Gglob,Cloc);
%run('Lsc.m');

[Clsc,M] = Lsc_fun(Fglob);

%load('LP.mat');
%load('BP.mat');
%load('HP.mat');
%load('Gglob.mat');
%load('BHQS.mat')
%load('Cloc.mat')
%load("some_var.mat")
%load('Lsc_var.mat')
%load('darm_initial.mat')
%warning('off', 'all');

Koptred = Kopt(input,output);
Clocred = Cloc(input,output);
ClscM = Clsc*M;
ClscMred = ClscM(input,output);
Mred = M(output,output);
save('Cloc.mat','Cloc','Clocred')
save('some_var.mat','Fglob','Sp2','Sp2inv','Sp1','Sp1inv','I48','Kopt','Sp2red','Sp2invred','Sp1red','Sp1invred','Koptred','ClscM','ClscMred')

Hglob2 = feedback(Gglob,Kopt+Cloc+Clsc*M);%inv(I48+Gglob*(Kopt+Cloc+Clsc*M))*Gglob;
Hglob2red = feedback(Gglobred,Koptred+Clocred+ClscMred);

save('Lsc_var.mat','Hglob2','M','Clsc','Hglob2red','Mred');
%Mtot = 400-100;

warning('off', 'all');

%cont_varh = [nu1,nu2,phi1,phi2,mu1,mu2,psi1,psi2,gh];


%cont_varh=[nu1,phi1,n1,nu2,phi2,n2,mu1,psi1,m1,mu2,psi2,m2,gh];

lb = [log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,log10(0.000001),0,0,0];  % Lower bounds for variables
ub = [log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf,log10(30),+90,4,log10(300),+90,4,log10(800),+90,2,log10(800),+90,2,+inf];   % Upper bounds for variables


%cont_vars = [nus1,nus2,phis1,phis2,mus1,mus2,psis1,psis2,gs];
%cont_vars=[nus1,phis1,mus1,psis1,gs];

warning('off', 'Control:analysis:AccuracyLoss');
%initial_darm 
darm_initial = optim_initial();
save('darm_initial.mat','darm_initial')

%x0 = cont_varh;%,cont_vars];

%options = optimoptions("fmincon",'Display','iter-detailed',...
%    "Algorithm","interior-point",...
%    "EnableFeasibilityMode",false,...
%    "SubproblemAlgorithm","cg", 'Display','iter-detailed');
%options = optimoptions('lsqnonlin','Disp m lay','iter-detailed');%,'Algorithm','sqp');
%options.ConstraintTolerance = 1e-8;
%options.OptimalityTolerance = 1e-8;
%options.StepTolerance = 1e-8;
%options.FiniteDifferenceStepSize = 0.03                                                                                                                                                                                                                                                           ;
%options.FiniteDifferenceType="central";
%options.FunctionTolerance = 1e-8;
%options.MaxFunctionEvaluations = 2000;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parpool();  % Opens a parallel pool with default settings
options = optimoptions('particleswarm', 'SwarmSize', 100, 'UseParallel', true,'UseVectorized',true,'MaxIterations', 100, ...  % Optional: Maximum number of iterations
    'OutputFcn', @ps_outputfcn);

%x0 = [-3, 3];   % Initial guess

[xsol, fvalsol, exitflag, output, points] = particleswarm(@optim_vec, 26, lb, ub, options);
save('xsol.mat','xsol');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[x,f_val] = fmincon(@optim,x0,[0,0,0,0,0,0,0,0,0,1,1],Mtot,[],[],[0,0,-inf,-inf,0,0,-inf,-inf,0,0,0],[+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,Mtot,Mtot],[],options);%@localdamp,options);
%%%%%[x,f_val] = fmincon(@optim,x0,[0,0,0,0,0,0,0,0,0,0],Mtot,[],[],[0,-inf,0,-inf,0,0,-inf,0,-inf,0],[+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf,+inf],[],options);%@localdamp,options);

%[x,f_val] = lsqnonlin(@optim,x0,[-inf,-inf,-inf,-inf,-inf,-inf,0,0],[+inf,+inf,+inf,+inf,+inf,+inf,Mtot,Mtot],[0,0,0,0,0,0,1,1],Mtot,[],[],[],options);%@localdamp,options);

%[x,fval] = fminunc(@optim,x0);
%figure()
%hold on
%plot_noise(x);
%plot_noise(x0);
%set(gca, 'YScale', 'log')
%set(gca, 'XScale', 'log')

delete(gcp);  % Close the parallel pool
%A = readmatrix('SS_BHQS\A_bhqs');
%B = readmatrix('SS_BHQS\B_bhqs');
%C = readmatrix('SS_BHQS\C_bhqs');
%D = readmatrix('SS_BHQS\D_bhqs');

function stop = ps_outputfcn(optimValues,y,states)
    % Extract relevant information from optimValues
    %disp(optim(optimValues.bestx))
    iteration = optimValues.iteration;
    bestfval = optimValues.bestfval;
    bestx = optimValues.bestx;
    
    % Display current iteration, best objective value, and best position
    fprintf('Iteration: %d, Best Objective: %.4f, Best Solution: %s\n', ...
        iteration, bestfval, mat2str(bestx));
    
    % Check for stopping condition (optional)
    stop = false;  % Set to true to stop optimization early
end

end

