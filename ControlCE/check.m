close all;clc;
[f,z,p] = damp(Hglob);

for i=1:length(f)
    if real(p(i))>1e-6
        disp(f(i))
    end
end

figure
bode(cont)
legend('Clsc')
figure()
bode(Clsc(43,43))
hold on
bode(Clsc(49,43))
bode(Clsc(55,43))
legend('ClscUIM','ClscPUM','ClscTST')

OP = Hglob2*CascP;

figure()



OP2 = Hglobsh*Sp1*CascP*Sp2inv;
bode(OP(23,23),OP2(23,23),OP(47,47),OP2(47,47));
legend('openloop on TST E','openloop on TST S','openloop on TST I','openloop on TST H')



figure()
bode(Hglobsh(23,29),Hglobsh(47,59))
hold on

bode(Hglobfsh(23,29),Hglobfsh(47,59),2*pi*logspace(-2,3,4000))
legend('S2S','H2H','S2Sclosed','H2Hclosed')


figure()
bode(LP);
hold on
bode(BP);
bode(HP);
legend('LP','BP','HP')
