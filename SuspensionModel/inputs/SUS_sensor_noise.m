function noiseAsd = SUS_sensor_noise(sensorName,freq)
% noiseASD = SUS_sensor_noise(sensorName,freq);
% Noise estimates of the sensors for the aLIGO Suspensions, as well as
% other bonus sensors used throughout the GW community
%
% sensorName: name of sensor
% supportedSensors = {'bosemair';...     % BOSEM, Shadow Sensor Readout, RDS
%                     'aosemair';...     % AOSEM, Shadow Sensor Readout, RDS
%                     'euclidair';...    % EUCLID, Interferometric Sensor, RDS
%                     'lvdt';...         % LVDT, Inductive Sensor, RDS
%                     'ivlvdtacc';...    % iVIRGO Accelerometer used on F0
%                                        % and F7 of SuperAttenuators with LVDT Readout, IS
%                     'aeimono';...      % AEI 10m Monolithic Horizontal
%                                        % Accelerometer with LVDT Readout, IS
%                     'advwattlinkage'}; % aVIRGO Monolithic Horizontal
%                                        % Accelerometer with Interferometric Readout, IS
% [Here, RDS = Relative displacement sensor, and IS = Inertial Sensor]
%             
% freq      : frequnecy vector upon which to interpolate the sensor noise
%
% noiseAsd  : amplitude spectral density of the self noise of the
%             instrument, in [m/rtHz]
%
% Any raw data mentioned below is located in the aLIGO Suspensions SVN
% repository, or SusSVN, located here
% <https://redoubt.ligo-wa.caltech.edu/svn/sus/>
% and can be accessed using ligo.org authenticaion. This location (because
% the repository is meant to be checked out on a local machine, not browsed
% through over the web) is abbreviated in the notes below as ${SusSVN}/    

sensorName = lower(sensorName);

supportedSensors = {'bosemair';...     % BOSEM
                    'aosemair';...     % AOSEM
                    'euclidair';...    % EUCLID
                    'lvdt';...         % LVDT
                    'ivlvdtacc';...    % iVIRGO Accelerometer used on F0 and F7 of SuperAttenuators with LVDT Readout
                    'aeimono';...      % AEI 10m Monolithic Horizontal Accelerometer with LVDT Readout 
                    'advwattlinkage';...% aVIRGO Monolithic Horizontal Accelerometer with Interferometric Readout
                    'smaract'};         %SmarAct interferometric displacement sensors

switch sensorName
    case supportedSensors{1} % BOSEM
        % Comes from "average" (by eye) in the figures section 7 of Stuart
        % Aston's T0900496
        dataFreq = [0.01   0.1    0.5    1         10     100      1000    10000];
        dataASD  = [5e-7   1e-8   3e-10  1.5e-10   6e-11  6e-11   6e-11    6e-11];
        
    case supportedSensors{2} % AOSEM
        % Comes from by-hand selection of frequency points of mean noise of
        % 10 sensors. See raw data in 
        % ${SusSVN}/trunk/Common/Data/AOSEM_SensorNoise.mat
        % Raw data collected by JSK from  what was produced as a part of
        % aLIGO QA testing by M. Barton, and J. Bartlett 
        dataFreq = [0.01 0.1 0.125    1      10     100      1000     10000];
        dataASD  = [4e-8 6e-9 5e-9     8e-10  2e-10  7e-11   3e-11    1e-11];
        
    case supportedSensors{3} % EUCLID
        % Comes from by-hand fit of data. See raw data  in
        % ${SusSVN}/trunk/Common/Data/EUCLID_SenserNoise.mat
        % Raw data emailed to JSK from S. Aston 
        dataFreq = [ 0.001,  0.0034,     0.012,     0.04,      0.14, ...
                     1.287,    11.8,       108,     1000];
        dataASD  = [2.5e-7,  2.1e-8,  1.785e-9,  1.5e-10, 1.275e-11,...
                    3e-12 , 6.7e-13,   1.5e-13,  3.5e-14];
        
        % cornerFreq = 0.14; %Hz
        % cornerAmp  = 1.276e-11; % m/rtHz
        % lowSlope   = f^-2;
        % highSlope  = f^(2/3);
        
    case supportedSensors{4} % LVDT
        % Added by JSK, using by-eye fit to figure 5 in 
        % H. Tariq et al "The linear variable
        % differential transformer (LVDT) position sensor for gravitational
        % wave interferometer low-frequency controls" (2002)
        % <http://dx.doi.org/10.1016/S0168-9002(02)00802-1>
        
        dataFreq = [0.01, 0.1, 1, 10, 100];
        dataASD  = [ 400,  80,20, 20,  20]*1e-9;
        
    case supportedSensors{5} % iVIRGO Accelerometer used on F0 and F7 of SuperAttenuators with LVDT Readout
        
%         % Acceleration Sensitivity
%         ivcaccspec_lt10Hz_mps2prtHz = 7e-10; % [(m/s^2)/rtHz] below 10 Hz; From aVIRGO Superattenuator Chapter
%                                              % emailed to JSK by DHS May 2 2012
%                                              % which references S. Braccini et. al,
%                                              % Rev. Sci.  Instruments 66 (3), 2672
%                                              % (1995) 445
%         
%         % Figure 4 from S. Braccini et. al, Rev. Sci.  Instruments 66 (3), 2672 (1995) 445
%         acc.c = zpk(2*pi*[pair(4.5,88)],[],prod(1./abs(2*pi*[pair(4.5,87)])));
%         acc.f = abs(squeeze(freqresp(acc.c,w)));
%         
%         ivcacc_mps2prtHz = ivcaccspec_lt10Hz_mps2prtHz * acc.f';
%         
%         % Convert to Velocity
%         ivcacc_mpsprtHz = ivcacc_mps2prtHz ./ w;
%         
%         % Convert to Displacement
%         ivcacc_mprtHz = ivcacc_mps2prtHz ./ (w.^2);

        % The by-hand, dumb fit to it
        dataFreq = [0.01    0.1    1        2       3       4       4.5     5       6     10    100];
        dataASD  = [1.7e-7  1.7e-9 1.7e-11  3.5e-12 1.1e-12 2.3e-13 6.1e-14 1.8e-13 4e-13 7e-13 9e-13];
        
    case supportedSensors{6} % AEI 10m Monolithic Horizontal Accelerometer with LVDT Readout 
        % Model from email exchange to JSK between AEI group and A. Bertolini
        % May 16 2012
        % "The readout noise floor is consistent with the calculations and
        % is set around 7e-12 pm/rtHz."
        
%         w0=2*pi*0.3;%FP natural frequency (Hz)
%         m=1;%FP mass (kg)
%         ke=10;%elastic stiffness (N/m)
%         phi=3e-4;%7075T6 alloy loss angle
%         tnoise=(1/m)*sqrt(4*4e-21*phi*ke./w)./(w.^2);%brownian noise (m/rtHz)
%         lvdt=7e-12*w./w;%lvdt noise (m/rtHz)
%         rnoise=lvdt.*((w0^2-w.^2+1i*phi*w0^2)./(w.^2));%readout noise (m/rtHz)
%         total=sqrt(tnoise.^2+rnoise.^2);
%         loglog(freq,abs(total),freq,tnoise,freq,abs(rnoise))
%         legend('Total Noise','Thermal Noise','LVDT Noise')
        
        % The by-hand, dumb fit to it
        dataFreq = [0.01 0.1   0.2   0.3     0.4      0.5     1       10    100];
        dataASD  = [1e-8 6e-11 1e-11 1.4e-12 3.16e-12 4.5e-12 6.3e-12 7e-12 7e-12];
        
    case supportedSensors{7} % aVIRGO Monolithic Horizontal Accelerometer with Interferometric Readout
        % From model in E. Hennes Talk at GWADW 2012, slide 27
        
        dataFreq = [0.01 0.1     1     4.5     10    30    100];
        dataASD  = [5e-8 1.5e-10 5e-13 1e-14   4e-15 3e-15 3e-15];
        
    case supportedSensors{8} % SmarAct from P2200019
        
        dataFreq = [0.01   0.1     1     4.5     10    30    100];
        dataASD  = [1e-10 1e-12 4e-13 4e-13   4e-13 4e-13 4e-13];        
    otherwise
        noiseAsd = [];
        disp(['Sorry, sensor type ''' sensorName ''' is does not exist!'])
        disp('Chose from:')
        disp(supportedSensors);
        return
end

lognoise = interp1(log10(dataFreq),log10(dataASD),log10(freq));
noiseAsd = 10.^lognoise;
