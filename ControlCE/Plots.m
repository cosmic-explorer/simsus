% Define the transfer function
TF = Gglob(23, 5);

% Define the frequency range
f = logspace(-1, 2, 30000);

% Set Bode plot options
P = bodeoptions;
P.Grid = 'on';
P.FreqUnits = 'Hz';
P.XLabel.FontSize = 14;
P.TickLabel.FontSize = 14;
P.YLabel.FontSize = 14;

% Generate Bode plot data
[mag, phase, w] = bode(TF, f);

% Convert magnitude to dB
magdB = 20*log10(squeeze(mag));

% Create a figure for the Bode plot
figure(2)

% Plot the magnitude plot with vertical lines
subplot(2, 1, 1);
semilogx(w/(2*pi), magdB, 'LineWidth', 2, 'DisplayName', 'My model'); % Convert rad/s to Hz for the plot
hold on;
xline(0.506, 'r--', 'LineWidth', 1.5,'DisplayName','Mode1'); % Vertical line at 0.5 Hz
xline(0.93, 'r--', 'LineWidth', 1.5,'DisplayName','Mode2'); % Vertical line at 0.93 Hz
xline(1.47, 'r--', 'LineWidth', 1.5,'DisplayName','Mode3'); % Vertical line at 1.47 Hz
xline(2.04, 'r--', 'LineWidth', 1.5,'DisplayName','Mode4','Label','Edgard model'); % Vertical line at 2 Hz
hold off;
xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');
title('Pitch ISI to Pitch Test - Magnitude', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.1,10])
ylim([-100,50])
legend();

% Plot the phase plot
subplot(2, 1, 2);
semilogx(w/(2*pi), squeeze(phase), 'LineWidth', 2, 'DisplayName', 'My model'); % Convert rad/s to Hz for the plot
xlabel('Frequency (Hz)');
ylabel('Phase (deg)');
title('Pitch ISI to Pitch Test - Phase', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.1,10])
legend();

% Adjust properties of all lines in the figure
set(findall(gcf, 'type', 'line'), 'linewidth', 4);










% Define the transfer function
TF1 = Gglob(5, 11);
[mag, phase, w] = bode(TF1, f);

% Convert magnitude to dB
magdB = 20*log10(squeeze(mag));
TF2 = Fglob(5, 11);

% Define the frequency range
f = logspace(-1, 4, 30000);

% Set Bode plot options
P = bodeoptions;
P.Grid = 'on';
P.FreqUnits = 'Hz';
P.XLabel.FontSize = 14;
P.TickLabel.FontSize = 14;
P.YLabel.FontSize = 14;

% Generate Bode plot data
[magf, phasef, w1] = bode(TF2, f);

% Convert magnitude to dB
magdBf = 20*log10(squeeze(magf));

[magc, phasec, w2] = bode(Cloc(11,5), f);

% Convert magnitude to dB
magdBc = 20*log10(squeeze(magc));

% Create a figure for the Bode plot
figure(3)

% Plot the magnitude plot with vertical lines
subplot(2, 1, 1);
semilogx(w/(2*pi), magdB, 'LineWidth', 2, 'DisplayName', 'Plant'); % Convert rad/s to Hz for the plot
hold on;
semilogx(w1/(2*pi), magdBf, 'LineWidth', 2, 'DisplayName', 'Damped Plant'); % Convert rad/s to Hz for the plot

semilogx(w2/(2*pi), magdBc, 'LineWidth', 2, 'DisplayName', 'Lead Controller'); % Convert rad/s to Hz for the plot

hold off;
xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');
title('Pitch TOP to Pitch TOP Local Damping - Magnitude', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.1,10])
ylim([-150,150])
legend();

% Plot the phase plot
subplot(2, 1, 2);
 % Convert rad/s to Hz for the plot
semilogx(w/(2*pi), squeeze(phase), 'LineWidth', 2, 'DisplayName', 'Plant');
hold on
semilogx(w1/(2*pi), squeeze(phasef), 'LineWidth', 2, 'DisplayName', 'Damped Plant');

semilogx(w2/(2*pi), squeeze(phasec), 'LineWidth', 2, 'DisplayName', 'Lead Controller')
 % Convert rad/s to Hz for the plot
xlabel('Frequency (Hz)');
ylabel('Phase (deg)');
title('Pitch TOP to Pitch TOP Local Camping - Phase', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.1,10])
legend();

% Adjust properties of all lines in the figure
set(findall(gcf, 'type', 'line'), 'linewidth', 4);










Jglob = feedback(Gglob,Kopt);
% Define the transfer function
TF = Jglob(23, 5);

% Define the frequency range
f = logspace(-1, 2, 30000);

% Set Bode plot options
P = bodeoptions;
P.Grid = 'on';
P.FreqUnits = 'Hz';
P.XLabel.FontSize = 14;
P.TickLabel.FontSize = 14;
P.YLabel.FontSize = 14;

% Generate Bode plot data
[mag, phase, w] = bode(TF, f);

% Convert magnitude to dB
magdB = 20*log10(squeeze(mag));

% Create a figure for the Bode plot
figure(4)

% Plot the magnitude plot with vertical lines
subplot(2, 1, 1);
semilogx(w/(2*pi), magdB, 'LineWidth', 2, 'DisplayName', 'My model'); % Convert rad/s to Hz for the plot
hold on;
xline(0.26, 'r--', 'LineWidth', 1.5);
xline(0.82, 'b--', 'LineWidth', 1.5);
xline(0.9, 'r--', 'LineWidth', 1.5);
xline(1.4, 'b--', 'LineWidth', 1.5);
xline(1.45, 'r--', 'LineWidth', 1.5);
xline(2.04, 'b--', 'LineWidth', 1.5);
xline(2.06, 'r--', 'LineWidth', 1.5);
xline(2.6, 'b--', 'LineWidth', 1.5);
hold off;
xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');
title('Pitch ISI to Pitch Test with Cavity- Magnitude', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.1,10])
ylim([-100,50])
legend();

% Plot the phase plot
subplot(2, 1, 2);
semilogx(w/(2*pi), squeeze(phase), 'LineWidth', 2, 'DisplayName', 'My model'); % Convert rad/s to Hz for the plot
hold on

xlabel('Frequency (Hz)');
ylabel('Phase (deg)');
title('Pitch ISI to Pitch Test with Cavity - Phase', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.1,10])
legend();

% Adjust properties of all lines in the figure
set(findall(gcf, 'type', 'line'), 'linewidth', 4);





figure(5)
plot_noise(S_L0)
hold on;plot_noise(S_Lisii)
%plot_noise(S_Lisie)
plot_noise(S_Lni)
%plot_noise(S_Lne)
%plot_noise(d_ISIL);
%plot_noise(N_shot);
legend('S(L)','S(ISI)','S(Sensing Noise)')

xlabel('Hz')
ylabel('m/sqrt(Hz)')
title('Pure Length Budget')
xlim([0.01,100])


figure(6)
plot_noise(S_theta_i)
hold on;
plot_noise(S_Pisii)
%plot_noise(S_Pisei)
plot_noise(S_Pnii)
%plot_noise(S_Pnei)
plot_noise(S_Pascsi)
%plot_noise(S_Paschi)
%plot_noise(d_ISIP);
%plot_noise(N_shot);
%plot_noise(N_ASC);
legend('S(P)','S(ISI)','S(Sensing Noise)','S(ASC Soft)')
xlabel('Hz')
ylabel('rad/sqrt(Hz)')
title('Pitch Budget')
xlim([0.01,100])



figure(7)
plot_noise(S_P)
hold on;
plot_noise(S_L0)
plot_noise(sqrt(S_L0.^2+S_P.^2))

%plot_noise(d_ISIP);
%plot_noise(N_shot);
%plot_noise(N_ASC);
legend('S(Hard/Soft)','S(Length)','S(DARM)')
xlabel('Hz')
ylabel('rad/sqrt(Hz)')
title('Darm Noise Budget')
xlim([0.01,100])


f = logspace(-2, 3, 30000);

figure(8)
[magch, phasech, wch] = bode(ch, f);

% Convert magnitude to dB
magdBch = 20*log10(squeeze(magch));

% Create a figure for the Bode plot

% Plot the magnitude plot with vertical lines
subplot(2, 1, 1);
semilogx(wch/(2*pi), magdBch, 'LineWidth', 2, 'DisplayName', 'My model'); % Convert rad/s to Hz for the plot
hold on;

hold off;
xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');
title('Pitch ISI to Pitch Test - Magnitude', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.01,100])
ylim([-100,50])
legend();

% Plot the phase plot
subplot(2, 1, 2);
semilogx(wch/(2*pi), squeeze(phasech), 'LineWidth', 2, 'DisplayName', 'My model'); % Convert rad/s to Hz for the plot
xlabel('Frequency (Hz)');
ylabel('Phase (deg)');
title('Pitch ISI to Pitch Test - Phase', 'FontSize', 25);
grid on;
set(gca, 'FontSize', 25,'FontWeight','bold');
xlim([0.01,100])
legend();



