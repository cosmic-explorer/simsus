classdef BHQS < handle

    properties
        R;
        Spitch;
        Syaw;
        Kopt;
        ks;
        kh;
        k0;
        P;
        Iyy1 ;
        Iyy2 ;
        Iyy3 ;
        Iyy4 ;

        M1 = 117;
        M2 = 83;
        M3 = 100;
        M4 = 100;
        g = 9.81;
        
        Ixx1 = 8.1;
        Ixx2 = 3.6;
        Ixx3 = 2.6;
        Ixx4 = 2.6;
        
   
        
        Izz1 = 15.5;
        Izz2 = 6.7;
        Izz3 = 1.9;
        Izz4 = 1.9;
        
        L1 = 0.34;
        L2 = 0.34;
        L3 = 0.34;
        L4 = 0.60;
        
        K1;
        K2;
        K3;
        K4;
        
        d0 = 0;
        d1 = 0;
        d2 = 0;
        d3 = 0;
        d4 = 0;
        d5 = 0;
        d6 = 0;
        d7 = 0;
        
        w0 = 16e-2;
        w1 = 16e-2;
        w2 = 14e-2;
        w3 = 14e-2;
        w4 = 12.5e-2;
        w5 = 12.5e-2;
        w6 = 2.5e-2;
        w7 = 2.5e-2;
        
        n0 = 18e-2;
        n1 = 18e-2;
        n2 = 13.5e-2;
        n3 = 13.5e-2;
        n4 = 23e-2;
        n5 = 23e-2;
        n6 = 23e-2;
        n7 = 23e-2;
        
        L01;
        L02;
        L03;
        L04;
        Y4 = 72e9;
        r4 = 220e-6;
        %P = 300e3;
        %P = 1.5e6;
        


        ge =- 0.7795;
        gi = -1.0657;

        %ks =      -20;%  -10.57; %N.m  
        %kh =         456;%232.96; %N.m  
        Lcav = 3995;%4.5;
 

    end
    
    methods
        function obj = BHQS(P,Iyy1,Iyy2,Iyy3,Iyy4)
            if nargin < 1
                obj.P = 1.5e6; % Default value if not provided
                obj.Iyy1 = 8.1;
                obj.Iyy2 = 3.3;
                obj.Iyy3 = 1.9;
                obj.Iyy4 = 1.9;
            else
                if P ==0
                    obj.P = 1.5e6;
                else
                    obj.P=P;
                end


                if Iyy1 ==0
                    obj.Iyy1 = 8.1;
                else
                    obj.Iyy1=Iyy1;
                end
               



                 if Iyy2 ==0
                    obj.Iyy2 = 3.3;
                else
                    obj.Iyy2=Iyy2;
                 end




                  if Iyy3 ==0
                    obj.Iyy3 = 1.9;
                else
                    obj.Iyy3=Iyy3;
                 end






                  if Iyy4 ==0
                    obj.Iyy4 = 1.9;
                else
                    obj.Iyy4=Iyy4;
                 end


            end
            obj.K1 = 7.1565e3/2;%7.2e3 / 2;
            obj.K2 = 6.4131e3/2;%6.5e3 / 2;
            obj.K3 = 3.5674e3/2;%3.6e3 / 2;
            
            obj.K4 = 36750/2;%obj.Y4 * pi * obj.r4^2 / obj.L4 - obj.M4 * obj.g / 4 / obj.L4;
            
            obj.L01 = obj.L1 - (obj.M1 + obj.M2 + obj.M3 + obj.M4) * obj.g / 4 / obj.K1;
            obj.L02 = obj.L2 - (obj.M2 + obj.M3 + obj.M4) * obj.g / 4 / obj.K2;
            obj.L03 = obj.L3 - (obj.M3 + obj.M4) * obj.g / 4 / obj.K3;
            obj.L04 = obj.L4 - obj.M4 * obj.g / 4 / obj.K4;
            obj.R = obj.getr();
            %if obj.P ==0
            %    obj.Spitch = [[1 0];[0 -1]];
            %    obj.Syaw = [[1 0];[0 1]];
            %else
            %    obj.Spitch = [[1 obj.R];[obj.R -1]];
            %    obj.Syaw = [[1 obj.R];[-obj.R 1]];
            %end
            obj.Spitch = [[1 obj.R];[obj.R -1]];
            obj.Syaw = [[1 obj.R];[-obj.R 1]];
            obj.Kopt = [[obj.ks 0]; [0 obj.kh]];
            obj.ks = obj.P*obj.Lcav/3e8/(obj.gi*obj.ge-1)*((obj.ge+obj.gi)+sqrt((obj.ge-obj.gi)^2+4));
            obj.kh = obj.P*obj.Lcav/3e8/(obj.gi*obj.ge-1)*((obj.ge+obj.gi)-sqrt((obj.ge-obj.gi)^2+4));
            obj.k0 = 2*obj.P*obj.Lcav/3e8/(obj.gi*obj.ge-1);
        end
        function R = getr(obj)
            R = r(obj.gi,obj.ge);
        end
    end
end