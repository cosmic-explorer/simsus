run('ISI_inputs.m')
run('Noise_inputs.m')

d_ISIL = interpolate(freq_ISIL,data_ISIL,f);
d_ISIT = interpolate(freq_ISIT,data_ISIT,f);
d_ISIV = interpolate(freq_ISIV,data_ISIV,f);
d_ISIP = interpolate(freq_ISIP,data_ISIP,f);
d_ISIR = interpolate(freq_ISIR,data_ISIR,f);
d_ISIY = interpolate(freq_ISIY,data_ISIY,f);

N_shot = interpolate(freq_shot,n_shot,f);
N_ASC = interpolate(freq_ASC,n_ASC,f);