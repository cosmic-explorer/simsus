

%if controller_logic == 1
%    bode(Cloc_i_o);
%    legend('Controller')
%end


%if plant_logic == 1
%bode(HglobFree_i_o);
%legend('FreePlant')
%end



%if openloop_logic == 1
%bode(HglobOpenLoop_i_o);
%end


%if closedloop_logic == 1
%bode(Hglob_i_o);
%end


%grid on;




[magP, phaseP, wP] = bode(HglobFree_i_o, w);
magdBP = 20*log10(squeeze(magP));



[magC, phaseC, wC] = bode(C_part2, w);
magdBC = 20*log10(squeeze(magC));



[magOL, phaseOL, wOL] = bode(HglobOpenLoop_i_o, w);
magdBOL = 20*log10(squeeze(magOL));


[magCL, phaseCL, wCL] = bode(Hglob_i_o, w);
magdBCL = 20*log10(squeeze(magCL));


f2 = figure;f2.Position(1:4) =[10 10 2*900 2*600];
%figure('Position', [10 10 2*900 2*600]);

%disp(['                   <strong> Controller Design  </strong> '])
title(  ' Controller Design ' )
subplot(2, 1, 1);
hold on

if controller_logic == 1
    semilogx(wC/(2*pi), magdBC, 'LineWidth', 2, 'DisplayName', 'Controller');
end


if plant_logic == 1
    semilogx(wP/(2*pi), magdBP, 'LineWidth', 2, 'DisplayName', 'FreePlant');
end


if openloop_logic == 1
    semilogx(wOL/(2*pi), magdBOL, 'LineWidth', 2, 'DisplayName', 'OpenLoop');
end


if closedloop_logic == 1
    semilogx(wCL/(2*pi), magdBCL, 'LineWidth', 2, 'DisplayName', 'ClosedLoop');
end


xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');
grid on;
set(gca, 'XScale', 'log')
%set(gca, 'FontSize', 30,'FontWeight','bold');
legend();

subplot(2, 1, 2);
hold on;
if controller_logic == 1
    semilogx(wC/(2*pi), squeeze(phaseC), 'LineWidth', 2, 'DisplayName', 'Controller');
end


if plant_logic == 1
    semilogx(wP/(2*pi), squeeze(phaseP), 'LineWidth', 2, 'DisplayName', 'FreePlant');
end


if openloop_logic == 1
    semilogx(wOL/(2*pi), squeeze(phaseOL), 'LineWidth', 2, 'DisplayName', 'OpenLoop');
end


if closedloop_logic == 1
    semilogx(wCL/(2*pi), squeeze(phaseCL), 'LineWidth', 2, 'DisplayName', 'ClosedLoop');
end

xlabel('Frequency (Hz)');
ylabel('Phase (deg)');
yticks([-270, -180, -90, 0, 90, 180, 270])
yticklabels({'-270', '-180', '-90', '0', '90', '180', '270'})
%set(gca, 'FontSize', 30,'FontWeight','bold');
grid on;
set(gca, 'XScale', 'log')
legend();
