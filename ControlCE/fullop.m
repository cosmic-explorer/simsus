
%OPTIM_GEOM Summary of this function goes here
%   Detailed explanation goes here
run('ISI_inputs.m')
run('Noise_inputs.m')
%Execute Noise_inputs.m
run('freq.m')
run('load_inputs.m')
%run("createcomplementaryfilters.m");
run('three_complementary_filters.m');
save('HP.mat',"HP")
save('BP.mat',"BP")
save('LP.mat',"LP")

Mtot = 400;
M1 = 100;
M2 = 100;

x0 = [M1,M2];
options = optimoptions("fmincon",'Display','iter-detailed',...
    "Algorithm","interior-point",...
    "EnableFeasibilityMode",false,...
    "SubproblemAlgorithm","cg", 'Display','iter-detailed');


[x_geom,f_val] = fmincon(@optim_geom,x0,[1,1],300,[],[],[0.1,0.1],[Mtot,Mtot],[],options);%@localdamp,options);



