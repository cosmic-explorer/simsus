function rms = computeRMS(ff,asd)
% From Kevin
% Computes the cumulative RMS
%
%     Inputs:
%         ff: the frequency vector [Hz]
%         asd: the ASD of a quantity [A/rtHz, for some units A]
%
%     Returns:
%         rms: the cumulative RMS [A]
    rms2 = -cumtrapz(flip(ff),flip(asd.^2));
    rms = sqrt(flip(rms2));
%     rms = cat(1,rms(1,:), rms);
end









