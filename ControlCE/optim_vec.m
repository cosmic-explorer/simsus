function fval_vec = optim_vec(x)
%OPTIM_VEC Vectorized objective function for PSO
%   x: Matrix where each column represents a different particle's position
%   fval_vec: Vector of objective values corresponding to each particle

% Initialize vector to store objective values
fval_vec = zeros(length(x(:,1)), 1);

% Evaluate objective function for each particle in parallel
parfor i = 1:length(x(:,1))
    % Evaluate scalar objective function for the i-th particle
    fval_vec(i) = optim(x(i,:));
    %disp(fval_vec(i))
end

end

