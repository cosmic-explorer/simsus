function [Kopt,Fglob,Sp1red,Sp2red,Sp2invred,Sp1invred,Sp1,Sp1inv,Sp2,Sp2inv,I48] = some_var_fun(bhqs,Gglob,Cloc)
I48 = eye(48);
input =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47    49    53    55    59];
output =[1     5     7    11    13    17    19    23    25    29    31    35    37    41    43    47];

Kopt = ss(zeros(60,48));
Kopt_mini = inv(bhqs.Spitch)*[[bhqs.ks,0];[0,bhqs.kh]]*bhqs.Spitch; 
Kopt(29,23) = Kopt_mini(1,1);
Kopt(29,47) = Kopt_mini(1,2);
Kopt(59,23) = Kopt_mini(2,1);
Kopt(59,47) = Kopt_mini(2,2);

Fglob = feedback(Gglob,Kopt+Cloc);%inv(I48+Gglob*(Kopt+Cloc))*Gglob;
Spitch = bhqs.Spitch;
Sp1 = zeros(60,60);
Sp1(29,29) = Spitch(1,1);
Sp1(29,59) = Spitch(1,2);
Sp1(59,29) = Spitch(2,1);
Sp1(59,59) = Spitch(2,2);

Sp2 = zeros(48,48);
Sp2(23,23) = Spitch(1,1);
Sp2(23,47) = Spitch(1,2);
Sp2(47,23) = Spitch(2,1);
Sp2(47,47) = Spitch(2,2);

Sp2inv = zeros(48,48);
Spitchinv = inv(Spitch);
Sp2inv(23,23) = Spitchinv(1,1);
Sp2inv(23,47) = Spitchinv(1,2);
Sp2inv(47,23) = Spitchinv(2,1);
Sp2inv(47,47) = Spitchinv(2,2);

Sp1inv = zeros(60,60);
Sp1inv(29,29) = Spitchinv(1,1);
Sp1inv(29,59) = Spitchinv(1,2);
Sp1inv(59,29) = Spitchinv(2,1);
Sp1inv(59,59) = Spitchinv(2,2);


Sp1invred = Sp1inv(input,input);
Sp1red = Sp1(input,input);

Sp2invred = Sp2inv(output,output);
Sp2red = Sp2(output,output);
