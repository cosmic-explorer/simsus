import copy
from sympy import zeros, symbols, diff, Transpose
from sympy import eye, Matrix, Inverse, BlockMatrix,solve_linear_system
import sympy.physics.mechanics as me
from sympy import expand, cancel, factor, simplify
from sympy.parsing.sympy_parser import parse_expr
from sympy.physics.mechanics import Body, PinJoint, PrismaticJoint, JointsMethod, inertia
from sympy.physics.mechanics import dynamicsymbols
from sympy.physics.mechanics import dynamicsymbols, ReferenceFrame, outer
from sympy.physics.mechanics import ReferenceFrame, Point, RigidBody
from sympy.physics.mechanics import kinetic_energy, potential_energy, Point, Particle
#from scipy.signal import ss2tf, TransferFunction
#import random
import sympy as smp
import numpy as np
import matplotlib.pyplot as plt
#import scipy
smp.init_printing(use_latex='mathjax')





def set_point(point_name, location, reference_point, ReferenceFrame, orient, tol):
  P = Point(point_name)
  if tol != False:
    epsx = np.random.uniform(-tol, tol)
    epsy = np.random.uniform(-tol, tol)
    epsz = np.random.uniform(-tol, tol)
  else:
    epsx,epsy,epsz=0,0,0
    #position_N = Orientation*location

  pos = orient*Matrix(location)
  P.set_pos(reference_point, (pos[0]+epsx)*ReferenceFrame.x+(pos[1]+epsy)*ReferenceFrame.y+(pos[2]+epsz)*ReferenceFrame.z  )
  return P

def set_rectangle_points(name, reference_point, reference_frame, w, n, d, orient, dw, tol):

  location1 = [-w,n,d]
  P1 = set_point(name+'1', location1, reference_point, reference_frame, orient, tol)

  location2 = [w,n,d]
  P2 = set_point(name+'2', location2, reference_point, reference_frame, orient, tol)

  location3 = [-w,-n,d]
  P3 = set_point(name+'3', location3, reference_point, reference_frame, orient, tol)

  location4 = [w,-n,d]
  P4 = set_point(name+'4', location4, reference_point, reference_frame, orient, tol)

  return P1,P2,P3,P4

  #Create vector point2 -> point1

def pos(point1, point2, frame, normalize=False):

  pos = point1.pos_from(point2).express(frame)

  if normalize:
    pos = pos.normalize()
  else:
    pass
  return pos

def series_vec_multivar(vec, n,args):
  vecx = vec.dot(N.x)
  vecy = vec.dot(N.y)
  vecz = vec.dot(N.z)
  eps = symbols('eps')
  for arg in args:
    vecx = vecx.subs(arg,arg*eps)
    vecy = vecy.subs(arg,arg*eps)
    vecz = vecz.subs(arg,arg*eps)
  vecx = vecx.series(eps, 0, n).removeO().subs(eps,1)
  vecy = vecy.series(eps, 0, n).removeO().subs(eps,1)
  vecz = vecz.series(eps, 0, n).removeO().subs(eps,1)
  return vecx*N.x+vecy*N.y+vecz*N.z


def series_multivar(expr,n,args):
  eps = symbols('eps')
  for arg in args:
    expr = expr.subs(arg,arg*eps)
    expr = expr.series(eps, 0, n).removeO().subs(eps,1)
  return expr


def series_vec_monovar(vec, reference_frame, qi,n,args,):
  vecx = vec.dot(reference_frame.x)
  vecy = vec.dot(reference_frame.y)
  vecz = vec.dot(reference_frame.z)
  if vecx!=0:
    vecx = series_monovar(vecx, qi, n, args)
  if vecy!=0:
    vecy = series_monovar(vecy, qi, n, args)
  if vecz!=0:
    vecz = series_monovar(vecz, qi, n, args)

  return vecx*reference_frame.x+vecy*reference_frame.y+vecz*reference_frame.z

# def series_monovar(expr,qi,n,args):
#   eps = symbols('eps')
#   for arg in args:
#     if qi!=arg:

#       expr = expr.subs(arg,0)

#   expr = expr.subs(qi,qi*eps)
#   expr = expr.series(eps, 0, n).removeO().subs(eps,1)
#   return expr

def monovar(expr,qi,n,args):
  for arg in args:
    if qi!=arg:

      expr = expr.subs(arg,0)

  return expr

def series_monovar(expr,qi,n,args):
  eps = symbols('eps')
  for arg in args:
    if qi!=arg:

      expr = expr.subs(arg,0)

  #expr = expr.subs(qi,qi*eps)
 # print(expr)
 # print(qi)
  #print(expr)
  #expr = expr.series(qi, 0, n).removeO()
  return expr

def connect4x4(mass1, mass2,dict, ReferenceFrame):

  mass1_number = mass1.mass_number
  mass2_number = mass2.mass_number
  if mass1_number<mass2_number:
    ind1 = 'bot'
    ind2 = 'up'
  elif mass1_number == mass2_number:
    raise
  else:
    ind1 = 'up'
    ind2 = 'top'

  P11 = getattr(mass1,'B'+ind1+str(mass1_number)+'1')
  P21 = getattr(mass2,'B'+ind2+str(mass2_number)+'1')
  #wire1_name = 'B'+str(mass1_number)+'1'+'B'+str(mass2_number)+'1'
  wire1 = Wire(P11,mass1_number, P21, mass2_number, ReferenceFrame)
  dict[wire1.name] = wire1

  P12 = getattr(mass1,'B'+ind1+str(mass1_number)+'2')
  P22 = getattr(mass2,'B'+ind2+str(mass2_number)+'2')
  #wire2_name = 'B'+str(mass1_number)+'2'+'B'+str(mass2_number)+'2'
  wire2 = Wire(P12,mass1_number, P22, mass2_number, ReferenceFrame)
  dict[wire2.name] = wire2

  P13 = getattr(mass1,'B'+ind1+str(mass1_number)+'3')
  P23 = getattr(mass2,'B'+ind2+str(mass2_number)+'3')
  #wire3_name = 'B'+str(mass1_number)+'3'+'B'+str(mass2_number)+'3'
  wire3 = Wire(P13,mass1_number, P23, mass2_number, ReferenceFrame)
  dict[wire3.name] = wire3

  P14 = getattr(mass1,'B'+ind1+str(mass1_number)+'4')
  P24 = getattr(mass2,'B'+ind2+str(mass2_number)+'4')
  #wire4_name = 'B'+str(mass1_number)+'4'+'B'+str(mass2_number)+'4'
  wire4 = Wire(P14,mass1_number, P24, mass2_number, ReferenceFrame)
  dict[wire4.name] = wire4

#def find_TensionMatrix(masses, wires, reference_frame):
  #masses is a dictionnary in which the masses are listed like masses =  {m1: mass1} with mass1 a Mass object
  #wires is a dictionnary in which the wires are listed like wires =  {AB: wire1} with wire1 a Wire object


def reduce_tension_matrix(TensionMatrix, masses, caracteristic_length):
  red_TM = copy.deepcopy(TensionMatrix)


  for i, row in enumerate(red_TM):

    if i%6<3:

      if np.linalg.norm(row)<0.05:
        red_TM[i,:] = 0
    else:
      if np.linalg.norm(row)<0.05*caracteristic_length:
        red_TM[i,:] = 0
  return red_TM


def getTensions(TensionMatrix, masses, caracteristic_length):

  m_tot = sum(masses[key].mass for key in masses)

  redTensionMatrix = reduce_tension_matrix(TensionMatrix, masses, caracteristic_length)

  F_sus = np.array([])
  F_mass=np.array([0,0,1,0,0,0])
  for i, key_i in enumerate(masses):
    F_sus = np.concatenate((F_sus,F_mass*(masses[key_i]).mass*g))

  invredTensionMatrix = np.linalg.pinv(redTensionMatrix)


  Tensions = np.matmul(invredTensionMatrix, F_sus)

  F_rebuilt = np.matmul(TensionMatrix, Tensions)

  err_equilibrium = np.linalg.norm((F_rebuilt-F_sus)/m_tot/g)

  print('Distance from equilibrium:', err_equilibrium)

  if err_equilibrium<1e-10:
    print("The position studied is at equilibrium")

  elif err_equilibrium<0.05:
    print("The position studied is closed to equilibrium")

  else:
    print("The position studied is far from equilibrium")

  return Tensions, err_equilibrium


def get_Aki_Bi(Forces, Torques, reference_frame, qk, var):
  Forcesx = Forces.dot(reference_frame.x)
  #print('Forcesx', Forcesx)
  Forcesx = series_monovar(Forcesx, qk, 2, var)
  #print('Forcesx', Forcesx)
  #Forcesx = factor(cancel(Forcesx))
  #print('Forcesx', Forcesx)
  Akix = diff(Forcesx,qk).subs({qk:0})
 # print(Akix)


  Forcesy = Forces.dot(reference_frame.y)
  Forcesy = series_monovar(Forcesy, qk, 2, var)
  #Forcesy = factor(cancel(Forcesy))
  Akiy = diff(Forcesy,qk).subs({qk:0})

  #print(Akiy)
  Forcesz = Forces.dot(reference_frame.z)
  Forcesz = series_monovar(Forcesz, qk, 2, var)
  #print('Forcez= ',Forcesz)
  #Forcesz = factor(cancel(Forcesz))
  Akiz = diff(Forcesz,qk).subs({qk:0})

  # print(Akiz)
  Torquesx = Torques.dot(reference_frame.x)
  Torquesx = series_monovar(Torquesx, qk, 2, var)
 # print('Torquesx',Torquesx)
  #Torquesx = series_monovar(Torquesx, qk, 2, var)
  #Torquesx = factor(cancel(Torquesx))
  Akialphax = diff(Torquesx,qk).subs({qk:0})

  #print(Akialphax)
  Torquesy = Torques.dot(reference_frame.y)
  Torquesy = series_monovar(Torquesy, qk, 2, var)
  #Torquesy = series_monovar(Torquesy, qk, 2, var)
  #Torquesy = factor(cancel(Torquesy))
  Akialphay = diff(Torquesy,qk).subs({qk:0})


  Torquesz = Torques.dot(reference_frame.z)
  Torquesz = series_monovar(Torquesz, qk, 2, var)
  #Torquesz = series_monovar(Torquesz, qk, 2, var)
  #Torquesz = factor(cancel(Torquesz))
  Akialphaz = diff(Torquesz,qk).subs({qk:0})


  return Akix, Akiy, Akiz, Akialphax, Akialphay, Akialphaz

def factor_vec(vec, reference_frame):
  vecx = vec.dot(reference_frame.x)
  vecx = simplify(vecx)

  vecy = vec.dot(reference_frame.y)
  vecy = simplify(vecy)

  vecz = vec.dot(reference_frame.z)
  vecz = simplify(vecz)

  return vecx*reference_frame.x+vecy*reference_frame.y+vecz*reference_frame.z

# def get_ss(masses, wires, var, reference_frame):
#   n_masses = len(masses)
#   n_wires = len(wires)

#   A = zeros(n_masses*6,n_masses*6)
#   B = zeros(n_masses*6,6)
#   for k, qk in enumerate(var):
#     for i, key_i in enumerate(masses):
#       mass_i = masses[key_i]
#       Forces = -mass_i.mass*g*reference_frame.z
#       sum_vec_wire_top = 0*reference_frame.z
#       sum_vec_wire_tbot = 0*reference_frame.z
#       _vec_wire_tbot = 0*reference_frame.z
#       Torques = 0*reference_frame.x

#       for j, key_j in enumerate(wires):
#         wire_j = wires[key_j]
#         #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
#         inv_norm_vec_j = monovar(1/wire_j.vector.magnitude(),qk,2,var)
#         print('inv vec', inv_norm_vec_j)
#         # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
#         vec_j = monovar(wire_j.vector,qk,2,var)
#         if wire_j.mother_mass_number == mass_i.mass_number :

#           #sum_vec_wires += vec_j
#           Forcej = wire_j.K*(1-wire_j.L0*inv_norm_vec_j)*vec_j
#           Forces = Forces+ Forcej
#           XiBj= pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame)

#         elif wire_j.daughter_mass_number == mass_i.mass_number :

#           Forcej = -wire_j.K*(1-wire_j.L0*inv_norm_vec_j)*vec_j
#           Forces = Forces+ Forcej
#           XiBj= pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame)
#         print('Forces'+str(j), Forces)
#         XiBj = series_vec_monovar(XiBj, reference_frame, qk,2,var)
#         Torquej = XiBj.cross(Forcej)
#         Torques = Torques+Torquej
#       Forces = Forces.simplify()
#       print('simp', Forces)
#       Forces = factor_vec(series_vec_monovar(Forces,reference_frame, qk,2,var), reference_frame)
#       print('lin forces',Forces)
#       Akix, Akiy, Akiz, Akialphax, Akialphay, Akialphaz = get_Aki(Forces, Torques, reference_frame, qk, var)

#       A[k,6*i], A[k,6*i+1], A[k,6*i+2], A[k,6*i+3], A[k,6*i+4], A[k,6*i+5] = Akix/mass_i.mass, Akiy/mass_i.mass, Akiz/mass_i.mass, Akialphax/mass_i.Ixx, Akialphay/mass_i.Iyy, Akialphaz/mass_i.Izz
#       print('A'+str(k)+str(i), Akix)
#    # B[6*i,0],  B[6*i,0] = B[6*i,0] + Bix
#   return A, B

def get_ss_simp(masses, wires, var, var_input, reference_frame, g):

  flattened_var = ()
  for sub_var in var, var_input:
    flattened_var += sub_var
  var_glob = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  A = zeros(n_masses*6,n_masses*6)
  B = zeros(n_masses*6,6)
  for k, qk in enumerate(var_glob):

    for i, key_i in enumerate(masses):
      mass_i = masses[key_i]
      Forces = -mass_i.mass*g*reference_frame.z
      sum_vec_wire_top = 0*reference_frame.z
      sum_vec_wire_bot = 0*reference_frame.z
      torq_vec_wire_top = 0*reference_frame.z
      torq_vec_wire_bot = 0*reference_frame.z
      Torques = 0*reference_frame.x

      for j, key_j in enumerate(wires):
        wire_j = wires[key_j]

        MPx = monovar(wire_j.vector.dot(mass_i.reference_frame.x),qk,2,var_glob)
        MPy = monovar(wire_j.vector.dot(mass_i.reference_frame.y),qk,2,var_glob)
        MPz = monovar(wire_j.vector.dot(mass_i.reference_frame.z),qk,2,var_glob)
        K = wire_j.K
        k = wire_j.k
        L0 = wire_j.L0
        MNz = monovar(K*(wire_j.vector.magnitude()**2/L0**2-1)*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)),qk,2,var_glob)
        MNvec = monovar(MNz*mass_i.reference_frame.z,qk,2,var_glob)
        NPz = monovar(k*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)),qk,2,var_glob)
        NPvec = monovar(NPz*mass_i.reference_frame.z+MPy*mass_i.reference_frame.y+MPx*mass_i.reference_frame.x,qk,2,var_glob)





        #Blade_j=wire_j.K*(1+(wire_j.vector.dot(mass_i.reference_frame.x))**2/(wire_j.L0))/(wire_j.k+wire_j.K*(1+(wire_j.vector.dot(mass_i.reference_frame.z))**2/(wire_j.L0)))*wire_j.vector.dot(mass_i.reference_frame.z)*mass_i.reference_frame.z



        #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
        inv_norm_vec_j = monovar(1/wire_j.vector.magnitude(),qk,2,var_glob)
        #print('inv vec', inv_norm_vec_j)
        # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
        vec_j = monovar(wire_j.vector,qk,2,var_glob)
        if wire_j.mother_mass_number == mass_i.mass_number :
          #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
          #sum_vec_wire_top+=vec_j
          #print(vec_j)
          #sum_vec_wires += vec_j
          #print('mother',vec_j)
          #Forcej = wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))*vec_j


          Forcej1 = MNvec*k
          Forcej2 = K*((wire_j.vector.magnitude()/L0)**2-1)*(MPx*mass_i.reference_frame.x+MPy*mass_i.reference_frame.y)


          #Forces = Forces+ Forcej
          XiBj= monovar(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),qk,2,var_glob)
          Bj = getattr(mass_i,str(wire_j.mother_point))
          P1 = set_point('1', [0,0,mass_i.mass_hang*g/4/wire_j.K], Bj, mass_i.reference_frame, mass_i.orient, 0)
          XiBj_real = monovar(pos(P1, mass_i.loc,reference_frame),qk,2,var_glob)
          #def __init__(self, mass_number, reference_frame_origin, ReferenceFrame, eq, tol):
          #torq_vec_wire_top+=XiBj.cross(vec_j)

          #print(inv_top)
          Forces += Forcej1+Forcej2#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
        #print(Forces)
          Torques+= XiBj.cross(Forcej2)+XiBj_real.cross(Forcej1)
        elif wire_j.daughter_mass_number == mass_i.mass_number :
          Forcej3 = -K*((wire_j.vector.magnitude()/L0)**2-1)*NPvec

          #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
          #sum_vec_wire_bot+=vec_j
          #print('daughter',vec_j)
         # Forcej = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))*vec_j
          #Forces = Forces+ Forcej
          XiBj= monovar(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),qk,2,var_glob)
          #print(inv_bot)
          #print(XiBj)
          #torq_vec_wire_bot+=XiBj.cross(vec_j)

          Forces += Forcej3#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
        #print(Forces)
          Torques+= XiBj.cross(Forcej3)
        #print(Torques)
      #print(Forces)
      #print(Forces)
      #Forces = series_vec_monovar(Forces,reference_frame, qk,2,var_glob)
      #print(Forces)
      #Torques += inv_top*torq_vec_wire_top+inv_bot*torq_vec_wire_bot
      #Torques = series_vec_monovar(Torques,reference_frame, qk,2,var_glob)
      #print(Torques)
      #print(Torques)

     # print('lin forces',Forces)
      if qk not in var_input:
        Akix, Akiy, Akiz, Akialphax, Akialphay, Akialphaz = get_Aki_Bi(Forces, Torques, reference_frame, qk, var_glob)
        A[k,6*i], A[k,6*i+1], A[k,6*i+2], A[k,6*i+3], A[k,6*i+4], A[k,6*i+5] = Akix/mass_i.mass, Akiy/mass_i.mass, Akiz/mass_i.mass, Akialphax/mass_i.Ixx, Akialphay/mass_i.Iyy, Akialphaz/mass_i.Izz
        print('A'+str(k)+str(i), Akix)

      else:
        Bix, Biy, Biz, Bialphax, Bialphay, Bialphaz = get_Aki_Bi(Forces, Torques, reference_frame, qk, var_glob)
        B[6*i,k-6*n_masses],B[6*i+1,k-6*n_masses],B[6*i+2,k-6*n_masses],B[6*i+3,k-6*n_masses],B[6*i+4,k-6*n_masses],B[6*i+5,k-6*n_masses] = Bix/mass_i.mass, Biy/mass_i.mass, Biz/mass_i.mass, Bialphax/mass_i.Ixx, Bialphay/mass_i.Iyy, Bialphaz/mass_i.Izz

        print('Bix', Bix) # B[6*i,0],  B[6*i,0] = B[6*i,0] + Bix

  return A, B


def get_ss_simp_all_(masses, wires, var, var_input, reference_frame, g,self):

  flattened_var = ()
  for sub_var in var, var_input:
    flattened_var += sub_var
  var_glob = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  A = zeros(n_masses*6,n_masses*6)
  B = zeros(n_masses*6,6)
  for k, qk in enumerate(var_glob):

    for i, key_i in enumerate(masses):
      mass_i = masses[key_i]
      Forces = -mass_i.mass*g*reference_frame.z
      sum_vec_wire_top = 0*reference_frame.z
      sum_vec_wire_bot = 0*reference_frame.z
      torq_vec_wire_top = 0*reference_frame.z
      torq_vec_wire_bot = 0*reference_frame.z
      Torques = 0*reference_frame.x

      for j, key_j in enumerate(wires):
        wire_j = wires[key_j]

        #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
        inv_norm_vec_j = monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),qk,2,var_glob)
        #print('TEST',inv_norm_vec_j)
        #print('inv vec', inv_norm_vec_j)
        # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
        vec_j = monovar(replace(wire_j.vector,self,self.BHQSsus),qk,2,var_glob)
        if wire_j.mother_mass_number == mass_i.mass_number :
          #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
          #sum_vec_wire_top+=vec_j
          #print(vec_j)
          #sum_vec_wires += vec_j
          #print('mother',vec_j)
          Forcej = replace(wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),qk,2,var_glob))))*vec_j,self,self.BHQSsus)
          #Forces = Forces+ Forcej
          XiBj= monovar(replace(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),self,self.BHQSsus),qk,2,var_glob)
          #torq_vec_wire_top+=XiBj.cross(vec_j)
          #print(inv_top)
          Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
        #print(Forces)
          Torques+= XiBj.cross(Forcej)
        elif wire_j.daughter_mass_number == mass_i.mass_number :

          #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
          #sum_vec_wire_bot+=vec_j
          #print('daughter',vec_j)
          Forcej = -replace(wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),qk,2,var_glob))))*vec_j,self,self.BHQSsus)
          #Forces = Forces+ Forcej
          XiBj= monovar(replace(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),self,self.BHQSsus),qk,2,var_glob)
          #print(inv_bot)
          #print(XiBj)
          #torq_vec_wire_bot+=XiBj.cross(vec_j)

          Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
        #print(Forces)
          Torques+= XiBj.cross(Forcej)
        #print(Torques)
      #print(Forces)
      #print(Forces)
      #Forces = series_vec_monovar(Forces,reference_frame, qk,2,var_glob)
      #print(Forces)
      #Torques += inv_top*torq_vec_wire_top+inv_bot*torq_vec_wire_bot
      #Torques = series_vec_monovar(Torques,reference_frame, qk,2,var_glob)
      #print(Torques)
      #print(Torques)

     # print('lin forces',Forces)
      if qk not in var_input:
        Akix, Akiy, Akiz, Akialphax, Akialphay, Akialphaz = get_Aki_Bi(Forces, Torques, reference_frame, qk, var_glob)
        A[k,6*i], A[k,6*i+1], A[k,6*i+2], A[k,6*i+3], A[k,6*i+4], A[k,6*i+5] = replace(Akix/mass_i.mass,self,self.BHQSsus), replace(Akiy/mass_i.mass,self,self.BHQSsus), replace(Akiz/mass_i.mass,self,self.BHQSsus), replace(Akialphax/mass_i.Ixx,self,self.BHQSsus), replace(Akialphay/mass_i.Iyy,self,self.BHQSsus), replace(Akialphaz/mass_i.Izz,self,self.BHQSsus)
        print('A'+str(k)+str(i), Akix)

      else:
        Bix, Biy, Biz, Bialphax, Bialphay, Bialphaz = get_Aki_Bi(Forces, Torques, reference_frame, qk, var_glob)
        B[6*i,k-6*n_masses],B[6*i+1,k-6*n_masses],B[6*i+2,k-6*n_masses],B[6*i+3,k-6*n_masses],B[6*i+4,k-6*n_masses],B[6*i+5,k-6*n_masses] = replace(Bix/mass_i.mass,self,self.BHQSsus), replace(Biy/mass_i.mass,self,self.BHQSsus), replace(Biz/mass_i.mass,self,self.BHQSsus), replace(Bialphax/mass_i.Ixx,self,self.BHQSsus), replace(Bialphay/mass_i.Iyy,self,self.BHQSsus), replace(Bialphaz/mass_i.Izz,self,self.BHQSsus)

        print('Bix', Bix) # B[6*i,0],  B[6*i,0] = B[6*i,0] + Bix

  return A, B




def get_eq_pos(masses,dw, wires, var, var_input, reference_frame, g, self):

  flattened_var = ()
  for sub_var in var, var_input, (dw,):
    flattened_var += sub_var

  var_dw = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  F0 = zeros(len(var),1)

  for i, key_i in enumerate(masses):
    mass_i = masses[key_i]
    Forces = -mass_i.mass*g*reference_frame.z
    sum_vec_wire_top = 0*reference_frame.z
    sum_vec_wire_bot = 0*reference_frame.z
    torq_vec_wire_top = 0*reference_frame.z
    torq_vec_wire_bot = 0*reference_frame.z
    Torques = 0*reference_frame.x

    for j, key_j in enumerate(wires):
      wire_j = wires[key_j]
      #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
      #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
      #print('inv vec', inv_norm_vec_j)
      # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
      vec_j = monovar(wire_j.vector,dw,2,var_dw)
      if wire_j.mother_mass_number == mass_i.mass_number :
        #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
        #sum_vec_wire_top+=vec_j
        #print(vec_j)
        #sum_vec_wires += vec_j
        #print('mother',vec_j)
        Forcej = wire_j.K*(1-wire_j.L0*series_monovar(1/wire_j.vector.magnitude(),dw,2,var_dw))*vec_j
        #Forces = Forces+ Forcej
        XiBj= monovar(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),dw,2,var_dw)
        #torq_vec_wire_top+=XiBj.cross(vec_j)
        #print(inv_top)
        Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
      #print(Forces)
        Torques+= XiBj.cross(Forcej)
      elif wire_j.daughter_mass_number == mass_i.mass_number :

        #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
        #sum_vec_wire_bot+=vec_j
        #print('daughter',vec_j)

        Forcej = -wire_j.K*(1-wire_j.L0*series_monovar(1/wire_j.vector.magnitude(),dw,2,var_dw))*vec_j
        #Forces = Forces+ Forcej
        XiBj= monovar(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),dw,2,var_dw)
        #print(inv_bot)
        #print(XiBj)
        #torq_vec_wire_bot+=XiBj.cross(vec_j)

        Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
      #print(Forces)
        Torques+= XiBj.cross(Forcej)
      #print(Torques)
    #print(Forces)
    #print(Forces)
    #Forces = series_vec_monovar(Forces,reference_frame, qk,2,var_glob)
    #print(Forces)
    #Torques += inv_top*torq_vec_wire_top+inv_bot*torq_vec_wire_bot
    #Torques = series_vec_monovar(Torques,reference_frame, qk,2,var_glob)
    #print(Torques)
    #print(Torques)

    # print('lin forces',Forces)

    Forces  = Forces.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    Forces = Forces.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    Forces = Forces.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    Forces = Forces.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    Forces = series_vec_monovar(Forces,reference_frame,dw,2,var_dw)

    Torques  = Torques.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    Torques = Torques.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    Torques = Torques.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    Torques = Torques.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    Torques = series_vec_monovar(Torques,reference_frame,dw,2,var_dw)
    #print(Torques)
    #print(Forces)
    Forces = Forces
    F0[6*i] = Forces.dot(reference_frame.x).series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i}',F0[6*i])
    F0[6*i+1] = Forces.dot(reference_frame.y).series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i+1}',F0[6*i+1])
    F0[6*i+2] = Forces.dot(reference_frame.z).series(dw,0,2).removeO()/mass_i.mass
   # print(f'F0{6*i+2}',F0[6*i+2])
    F0[6*i+3] = Torques.dot(reference_frame.x).series(dw,0,2).removeO()/mass_i.Ixx
   # print(f'F0{6*i+3}',F0[6*i+3])
    F0[6*i+4] = Torques.dot(reference_frame.y).series(dw,0,2).removeO()/mass_i.Iyy
    #print(f'F0{6*i+4}',F0[6*i+4])
    F0[6*i+5] =Torques.dot(reference_frame.z).series(dw,0,2).removeO()/mass_i.Izz
    #print(f'F0{6*i+5}',F0[6*i+5])
  return F0


def get_forces(masses,dw, wires, var, var_input, reference_frame, g, self):

  flattened_var = ()
  for sub_var in var, var_input, (dw,):
    flattened_var += sub_var

  var_dw = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  F0 = zeros(len(var),1)

  for i, key_i in enumerate(masses):
    mass_i = masses[key_i]
    Forces = -mass_i.mass*g*reference_frame.z
    sum_vec_wire_top = 0*reference_frame.z
    sum_vec_wire_bot = 0*reference_frame.z
    torq_vec_wire_top = 0*reference_frame.z
    torq_vec_wire_bot = 0*reference_frame.z
    Torques = 0*reference_frame.x

    for j, key_j in enumerate(wires):
      wire_j = wires[key_j]
      #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
      #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
      #print('inv vec', inv_norm_vec_j)
      # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
      vec_j = wire_j.vector#monovar(wire_j.vector,dw,2,var_dw)
      if wire_j.mother_mass_number == mass_i.mass_number :
        #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
        #sum_vec_wire_top+=vec_j
        #print(vec_j)
        #sum_vec_wires += vec_j
        #print('mother',vec_j)
        Forcej = replace(wire_j.K*(1-wire_j.L0*1/wire_j.vector.magnitude())*vec_j,self,self.BHQSsus)
        #Forces = Forces+ Forcej
        XiBj= monovar(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),dw,2,var_dw)
        #torq_vec_wire_top+=XiBj.cross(vec_j)
        #print(inv_top)
        Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
      #print(Forces)
        Torques+= replace(XiBj.cross(Forcej),self,self.BHQSsus)
      elif wire_j.daughter_mass_number == mass_i.mass_number :

        #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
        #sum_vec_wire_bot+=vec_j
        #print('daughter',vec_j)

        #Forcej = -replace(wire_j.K*(1-wire_j.L0*series_monovar(replace(1/wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw))*vec_j,self,self.BHQSsus)
        #Forces = Forces+ Forcej
        Forcej = -replace(wire_j.K*(1-wire_j.L0*1/wire_j.vector.magnitude())*vec_j,self,self.BHQSsus)
        XiBj= monovar(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),dw,2,var_dw)
        #print(inv_bot)
        #print(XiBj)
        #torq_vec_wire_bot+=XiBj.cross(vec_j)

        Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
      #print(Forces)
        Torques+= replace(XiBj.cross(Forcej),self,self.BHQSsus)
      #print(Torques)
    #print(Forces)
    #print(Forces)
    #Forces = series_vec_monovar(Forces,reference_frame, qk,2,var_glob)
    #print(Forces)
    #Torques += inv_top*torq_vec_wire_top+inv_bot*torq_vec_wire_bot
    #Torques = series_vec_monovar(Torques,reference_frame, qk,2,var_glob)
    #print(Torques)
    #print(Torques)

    # print('lin forces',Forces)

    #Forces  = Forces.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    #Forces = Forces.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    #Forces = Forces.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    #Forces = Forces.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    #Forces = series_vec_monovar(Forces,reference_frame,dw,2,var_dw)

    #Torques  = Torques.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    #Torques = Torques.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    #Torques = Torques.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    #Torques = Torques.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    #Torques = series_vec_monovar(Torques,reference_frame,dw,2,var_dw)
   # print(Torques)
   # print(Forces)
    Forces = replace(Forces,self,self.BHQSsus)
    F0[6*i] = Forces.dot(reference_frame.x)#.series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i}',F0[6*i])
    F0[6*i+1] = Forces.dot(reference_frame.y)#.series(dw,0,2).removeO()/mass_i.mass
   # print(f'F0{6*i+1}',F0[6*i+1])
    F0[6*i+2] = Forces.dot(reference_frame.z)#.series(dw,0,2).removeO()/mass_i.mass
   # print(f'F0{6*i+2}',F0[6*i+2])
    F0[6*i+3] = Torques.dot(reference_frame.x)#.series(dw,0,2).removeO()/mass_i.Ixx
   # print(f'F0{6*i+3}',F0[6*i+3])
    F0[6*i+4] = Torques.dot(reference_frame.y)#.series(dw,0,2).removeO()/mass_i.Iyy
    #print(f'F0{6*i+4}',F0[6*i+4])
    F0[6*i+5] =Torques.dot(reference_frame.z)#.series(dw,0,2).removeO()/mass_i.Izz
    #print(f'F0{6*i+5}',F0[6*i+5])
  return F0




def replace(Expr, self, Model):
  Expr = Expr.subs({self.mass1.mass: Model.M1,self.mass2.mass: Model.M2,self.mass3.mass: Model.M3,self.mass4.mass: Model.M4, self.g : Model.g})
  Expr = Expr.subs({self.mass1.Ixx: Model.Ixx1,self.mass2.Ixx: Model.Ixx2,self.mass3.Ixx: Model.Ixx3,self.mass4.Ixx: Model.Ixx4})
  Expr = Expr.subs({self.mass1.Iyy: Model.Iyy1,self.mass2.Iyy: Model.Iyy2,self.mass3.Iyy: Model.Iyy3,self.mass4.Iyy: Model.Iyy4})
  Expr = Expr.subs({self.mass1.Izz: Model.Izz1,self.mass2.Izz: Model.Izz2,self.mass3.Izz: Model.Izz3,self.mass4.Izz: Model.Izz4})
  Expr = Expr.subs({self.Frame.wbot: Model.w0, self.mass1.wbot:Model.w2, self.mass2.wbot:Model.w4, self.mass3.wbot:Model.w6})
  Expr = Expr.subs({self.Frame.nbot: Model.n0, self.mass1.nbot:Model.n2, self.mass2.nbot:Model.n4, self.mass3.nbot:Model.n6})
  Expr = Expr.subs({self.Frame.dbot: Model.d0, self.mass1.dbot:Model.d2, self.mass2.dbot:Model.d4, self.mass3.dbot:Model.d6})
  Expr = Expr.subs({self.mass1.wtop:Model.w1, self.mass2.wtop:Model.w3, self.mass3.wtop:Model.w5, self.mass4.wtop:Model.w7})
  Expr = Expr.subs({self.mass1.ntop:Model.n1, self.mass2.ntop:Model.n3, self.mass3.ntop:Model.n5, self.mass4.ntop:Model.n7})
  Expr = Expr.subs({self.mass1.dtop:Model.d1, self.mass2.dtop:Model.d3, self.mass3.dtop:Model.d5, self.mass4.dtop:Model.d7})
  Expr = Expr.subs({self.wires['Bbot01Bup11'].K:Model.K1,self.wires['Bbot11Bup21'].K:Model.K2,self.wires['Bbot21Bup31'].K:Model.K3,self.wires['Bbot31Bup41'].K:Model.K4})
  Expr = Expr.subs({self.wires['Bbot01Bup11'].k:Model.k1,self.wires['Bbot11Bup21'].k:Model.k2,self.wires['Bbot21Bup31'].k:Model.k3,self.wires['Bbot31Bup41'].k:Model.k4})

  Expr = Expr.subs({self.wires['Bbot01Bup11'].L0:Model.L01,self.wires['Bbot11Bup21'].L0:Model.L02,self.wires['Bbot21Bup31'].L0:Model.L03,self.wires['Bbot31Bup41'].L0:Model.L04})
  Expr = Expr.subs({self.mass1.L : Model.L1+Model.d0+Model.d1, self.mass2.L : Model.L1+Model.L2+Model.d0+Model.d1+Model.d2+Model.d3, self.mass3.L : Model.L1+Model.L2+Model.L3+Model.d0+Model.d1+Model.d2+Model.d3+Model.d4+Model.d5, self.mass4.L : Model.L1+Model.L2+Model.L3+Model.L4+Model.d0+Model.d1+Model.d2+Model.d3+Model.d4+Model.d5+Model.d6+Model.d7})
  return Expr

def replace_all(Expr, self, Model):
  Expr = Expr.subs({self.var[0]:0,self.var[1]:0})
  Expr = Expr.subs({self.mass1.mass: Model.M1,self.mass2.mass: Model.M2,self.mass3.mass: Model.M3,self.mass4.mass: Model.M4})
  Expr = Expr.subs({self.mass1.Ixx: Model.Ixx1,self.mass2.Ixx: Model.Ixx2,self.mass3.Ixx: Model.Ixx3,self.mass4.Ixx: Model.Ixx4})
  Expr = Expr.subs({self.mass1.Iyy: Model.Iyy1,self.mass2.Iyy: Model.Iyy2,self.mass3.Iyy: Model.Iyy3,self.mass4.Iyy: Model.Iyy4})
  Expr = Expr.subs({self.mass1.Izz: Model.Izz1,self.mass2.Izz: Model.Izz2,self.mass3.Izz: Model.Izz3,self.mass4.Izz: Model.Izz4})
  Expr = Expr.subs({self.Frame.wbot: Model.w0, self.mass1.wbot:Model.w2, self.mass2.wbot:Model.w4, self.mass3.wbot:Model.w6})
  Expr = Expr.subs({self.Frame.nbot: Model.n0, self.mass1.nbot:Model.n2, self.mass2.nbot:Model.n4, self.mass3.nbot:Model.n6})
  Expr = Expr.subs({self.Frame.dbot: Model.d0, self.mass1.dbot:Model.d2, self.mass2.dbot:Model.d4, self.mass3.dbot:Model.d6})
  Expr = Expr.subs({self.mass1.wtop:Model.w1, self.mass2.wtop:Model.w3, self.mass3.wtop:Model.w5, self.mass4.wtop:Model.w7})
  Expr = Expr.subs({self.mass1.ntop:Model.n1, self.mass2.ntop:Model.n3, self.mass3.ntop:Model.n5, self.mass4.ntop:Model.n7})
  Expr = Expr.subs({self.mass1.dtop:Model.d1, self.mass2.dtop:Model.d3, self.mass3.dtop:Model.d5, self.mass4.dtop:Model.d7})
  Expr = Expr.subs({self.wires['Bbot01Bup11'].K:Model.K1,self.wires['Bbot11Bup21'].K:Model.K2,self.wires['Bbot21Bup31'].K:Model.K3,self.wires['Bbot31Bup41'].K:Model.K4})
  Expr = Expr.subs({self.wires['Bbot01Bup11'].k:Model.k1,self.wires['Bbot11Bup21'].k:Model.k2,self.wires['Bbot21Bup31'].k:Model.k3,self.wires['Bbot31Bup41'].k:Model.k4})
  Expr = Expr.subs({self.wires['Bbot01Bup11'].L0:Model.L01,self.wires['Bbot11Bup21'].L0:Model.L02,self.wires['Bbot21Bup31'].L0:Model.L03,self.wires['Bbot31Bup41'].L0:Model.L04})
  Expr = Expr.subs({self.mass1.L : Model.L1+Model.d0+Model.d1, self.mass2.L : Model.L1+Model.L2+Model.d0+Model.d1+Model.d2+Model.d3, self.mass3.L : Model.L1+Model.L2+Model.L3+Model.d0+Model.d1+Model.d2+Model.d3+Model.d4+Model.d5, self.mass4.L : Model.L1+Model.L2+Model.L3+Model.L4+Model.d0+Model.d1+Model.d2+Model.d3+Model.d4+Model.d5+Model.d6+Model.d7})
  return Expr



def get_forces_num(masses,dw, wires, var, var_input, reference_frame, g, self):

  flattened_var = ()
  for sub_var in var, var_input, (dw,):
    flattened_var += sub_var

  var_dw = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  F0 = zeros(len(var),1)

  for i, key_i in enumerate(masses):
    mass_i = masses[key_i]
    Forces = -mass_i.mass*g*reference_frame.z
    sum_vec_wire_top = 0*reference_frame.z
    sum_vec_wire_bot = 0*reference_frame.z
    torq_vec_wire_top = 0*reference_frame.z
    torq_vec_wire_bot = 0*reference_frame.z
    Torques = 0*reference_frame.x

    for j, key_j in enumerate(wires):
      wire_j = wires[key_j]
      MPx = replace_all(wire_j.vector.dot(mass_i.reference_frame.x),self,self.BHQSsus)
      MPy = replace_all(wire_j.vector.dot(mass_i.reference_frame.y),self,self.BHQSsus)
      MPz = replace_all(wire_j.vector.dot(mass_i.reference_frame.z),self,self.BHQSsus)
      K = wire_j.K
      k = wire_j.k
      L0 = wire_j.L0
      MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)),self,self.BHQSsus)
      MNvec = replace_all(MNz*mass_i.reference_frame.z,self,self.BHQSsus)
      NPz = replace_all(k*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)),self,self.BHQSsus)
      NPvec = replace_all(NPz*mass_i.reference_frame.z+MPy*mass_i.reference_frame.y+MPx*mass_i.reference_frame.x,self,self.BHQSsus)
      #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
      #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
      #print('inv vec', inv_norm_vec_j)
      # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
      vec_j = wire_j.vector#monovar(wire_j.vector,dw,2,var_dw)
      if wire_j.mother_mass_number == mass_i.mass_number :
        #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
        #sum_vec_wire_top+=vec_j
        #print(vec_j)
        #sum_vec_wires += vec_j
        #print('mother',vec_j)
        Forcej = replace_all(wire_j.K*(1-wire_j.L0*1/wire_j.vector.magnitude())*vec_j,self,self.BHQSsus)
        #Forces = Forces+ Forcej
        XiBj= replace_all(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),self,self.BHQSsus)
      #  XiBj_real = XiBj+
        #torq_vec_wire_top+=XiBj.cross(vec_j)
        #print(inv_top)
        Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
      #print(Forces)
        Torques+= replace_all(XiBj.cross(Forcej),self,self.BHQSsus)
      elif wire_j.daughter_mass_number == mass_i.mass_number :

        #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
        #sum_vec_wire_bot+=vec_j
        #print('daughter',vec_j)

        #Forcej = -replace(wire_j.K*(1-wire_j.L0*series_monovar(replace(1/wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw))*vec_j,self,self.BHQSsus)
        #Forces = Forces+ Forcej
        Forcej = -replace_all(wire_j.K*(1-wire_j.L0*1/wire_j.vector.magnitude())*vec_j,self,self.BHQSsus)
        XiBj= replace_all(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),self,self.BHQSsus)
        #print(inv_bot)
        #print(XiBj)
        #torq_vec_wire_bot+=XiBj.cross(vec_j)

        Forces += Forcej#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
      #print(Forces)
        Torques+= replace_all(XiBj.cross(Forcej),self,self.BHQSsus)
      #print(Torques)
    #print(Forces)
    #print(Forces)
    #Forces = series_vec_monovar(Forces,reference_frame, qk,2,var_glob)
    #print(Forces)
    #Torques += inv_top*torq_vec_wire_top+inv_bot*torq_vec_wire_bot
    #Torques = series_vec_monovar(Torques,reference_frame, qk,2,var_glob)
    #print(Torques)
    #print(Torques)

    # print('lin forces',Forces)

    #Forces  = Forces.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    #Forces = Forces.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    #Forces = Forces.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    #Forces = Forces.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    #Forces = series_vec_monovar(Forces,reference_frame,dw,2,var_dw)

    #Torques  = Torques.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    #Torques = Torques.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    #Torques = Torques.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    #Torques = Torques.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    #Torques = series_vec_monovar(Torques,reference_frame,dw,2,var_dw)
   # print(Torques)
    #print(Forces)
    Forces = replace_all(Forces,self,self.BHQSsus)
    F0[6*i] = Forces.dot(reference_frame.x)#.series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i}',F0[6*i])
    F0[6*i+1] = Forces.dot(reference_frame.y)#.series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i+1}',F0[6*i+1])
    F0[6*i+2] = Forces.dot(reference_frame.z)#.series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i+2}',F0[6*i+2])
    F0[6*i+3] = Torques.dot(reference_frame.x)#.series(dw,0,2).removeO()/mass_i.Ixx
    #print(f'F0{6*i+3}',F0[6*i+3])
    F0[6*i+4] = Torques.dot(reference_frame.y)#.series(dw,0,2).removeO()/mass_i.Iyy
    #print(f'F0{6*i+4}',F0[6*i+4])
    F0[6*i+5] =Torques.dot(reference_frame.z)#.series(dw,0,2).removeO()/mass_i.Izz
    #print(f'F0{6*i+5}',F0[6*i+5])
  return F0


def get_forces_num_bis(masses,dw, wires, var, var_input, reference_frame, g, self):

  flattened_var = ()
  for sub_var in var, var_input, (dw,):
    flattened_var += sub_var

  var_dw = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  F0 = zeros(len(var),1)

  for i, key_i in enumerate(masses):
    mass_i = masses[key_i]

    Forces = -mass_i.mass*g*reference_frame.z
    sum_vec_wire_top = 0*reference_frame.z
    sum_vec_wire_bot = 0*reference_frame.z
    torq_vec_wire_top = 0*reference_frame.z
    torq_vec_wire_bot = 0*reference_frame.z
    Torques = 0*reference_frame.x

    for j, key_j in enumerate(wires):
      wire_j = wires[key_j]
      MPx = replace_all(wire_j.vector.dot(mass_i.ReferenceFrame.x),self,self.BHQSsus)
      MPy = replace_all(wire_j.vector.dot(mass_i.ReferenceFrame.y),self,self.BHQSsus)
      MPz = replace_all(wire_j.vector.dot(mass_i.ReferenceFrame.z),self,self.BHQSsus)
      #print('MPz',MPz)
      delta_k = wire_j.delta_k
      K = wire_j.K
      k = wire_j.k+delta_k
      L0 = wire_j.L0



      if wire_j.mother_mass_number == mass_i.mass_number :
          #mass_i_d = masses[key_i+1]
          Bj = getattr(mass_i,str(wire_j.mother_point))
          M_star = set_point('1', [0,0,(mass_i.mass_hang-mass_i.mass)*g/4/k], Bj, mass_i.ReferenceFrame, mass_i.orient, 0)
          #print(mass_i.mass_hang-mass_i.mass)
          M_starM = pos(M_star,getattr(mass_i,str(wire_j.mother_point)),self.ReferenceFrame)
          M_starMz = M_starM.dot(mass_i.ReferenceFrame.z)
          #print('MstarMMOM', replace(M_starM,self,self.BHQSsus),'len',replace(mass_i.mass_hang*g/4/k,self,self.BHQSsus))
          #MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)/2*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
          M_starNz = replace_all(K*(1-L0/wire_j.vector.magnitude())*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)



         # MNvec = replace_all(MNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)
          M_starNvec = replace_all(M_starNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)
          MNvec = replace_all(M_starNvec-M_starM,self,self.BHQSsus)
          #NPz = replace_all(k*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
          NPz = replace_all(k*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)

          NPvec = replace_all(NPz*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
          #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
          #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
          #print('inv vec', inv_norm_vec_j)
          # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
          vec_j = wire_j.vector#monovar(wire_j.vector,dw,2,var_dw)
          #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
          #sum_vec_wire_top+=vec_j
          #print(vec_j)
          #sum_vec_wires += vec_j
          #print('mother',vec_j)
          #Forcej = wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))*vec_j
          if mass_i.mass_number==3:
            NPvec = wire_j.vector
            M_starNvec = (1-L0/wire_j.vector.magnitude())*wire_j.vector
            k=K
            M_starM =0
            MNvec = replace_all(M_starNvec-M_starM,self,self.BHQSsus)

          Forcej1 = M_starNvec*k
          Forcej2 = K*(1-L0/(NPvec).magnitude())*(MPx*mass_i.ReferenceFrame.x+MPy*mass_i.ReferenceFrame.y)
          NPn =NPvec.magnitude()
          MNn =MNvec.magnitude()
          deltalen = wire_j.vector.magnitude()**2/L0**2-1
          quad = QuadSus()
          flattened_var1 = ()

          for sub_var in quad.eq1, quad.eq2, quad.eq3, quad.eq4:
            flattened_var1 += sub_var

          eq = flattened_var

          for l in range(len(quad.var)):
            Forcej1 = Forcej1.subs({quad.var[l]:0})
            Forcej2 = Forcej2.subs({quad.var[l]:0})
            NPn = NPn.subs({quad.var[l]:0})
            MNn = MNn.subs({quad.var[l]:0})
            deltalen = deltalen.subs({quad.var[l]:0})
          # F = F.subs({eq[i]:eq_[i]})
          for l in range(len(quad.var_input)):
            Forcej1 = Forcej1.subs({quad.var_input[l]:0})
            Forcej2 = Forcej2.subs({quad.var_input[l]:0})
          #print('Forcej2 = ',Forcej2)
          #print('Forcej1 = ',Forcej1)
          #print('NPn = ',NPn)
          #print('MNn = ',MNn)
          #print('deltalen = ',replace_all(deltalen,self,self.BHQSsus))
          #Forces = Forces+ Forcej
          XiBj= replace_all(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),self,self.BHQSsus)
          Bj = getattr(mass_i,str(wire_j.mother_point))
          P1 = set_point('1', [0,0,mass_i.mass_hang*g/4/k], Bj, mass_i.ReferenceFrame, mass_i.orient, 0)
          XiBj_real = replace_all(pos(P1, mass_i.loc,reference_frame),self,self.BHQSsus)
          #def __init__(self, mass_number, reference_frame_origin, ReferenceFrame, eq, tol):
          #torq_vec_wire_top+=XiBj.cross(vec_j)

          #print(inv_top)
          #print('Forcej1: '+str(Forcej1))

          Forces += replace_all(Forcej1+Forcej2,self,self.BHQSsus)#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
          #print('Forces_mom: '+str(Forces))
        #print(Forces)
          XiBj_ = XiBj+MNvec
          Torques+= replace_all(XiBj.cross(Forcej1)+XiBj_.cross(Forcej2),self,self.BHQSsus)
      elif wire_j.daughter_mass_number == mass_i.mass_number :
          keys_masses = list(masses.keys())
          #print('i=',i)
          if i==0:
            mass_i_up = self.Frame
            Bj = getattr(mass_i_up,str(wire_j.mother_point))
            M_star = set_point('1', [0,0,mass_i_up.mass_hang*g/4/k], Bj, mass_i_up.ReferenceFrame, mass_i_up.orient, 0)
          elif mass_i.mass_number==4:
            mass_i_up = masses[keys_masses[i-1]]
            Bj = getattr(mass_i_up,str(wire_j.mother_point))
            M_star = Bj
          else:
            mass_i_up = masses[keys_masses[i-1]]
            #print(keys_masses[i-1])
            Bj = getattr(mass_i_up,str(wire_j.mother_point))
            M_star = set_point('1', [0,0,(mass_i_up.mass_hang-mass_i_up.mass)*g/4/k], Bj, mass_i_up.ReferenceFrame, mass_i_up.orient, 0)

          #print('massnumber',mass_i_up,'i',i)
         # Bj = getattr(mass_i_up,str(wire_j.mother_point))

          M_starM = pos(M_star,getattr(mass_i_up,str(wire_j.mother_point)),self.ReferenceFrame)
          #print('MstarM', M_starM)
          M_starMz = M_starM.dot(mass_i.ReferenceFrame.z)
          #print('MstarM', replace(M_starM,self,self.BHQSsus))
          M_starNz = replace_all(K*(1-L0/wire_j.vector.magnitude())*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
          NPz = replace_all(k*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
          M_starNvec = replace_all(M_starNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)

          if mass_i.mass_number==4:
            NPvec = M_starNvec
          else:
            NPvec = replace_all(NPz*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
          #MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)/2*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
          #MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)/2*MPz/(k+K*(2*MPz**2/L0**2+wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
          #MNvec = replace_all(MNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)
          #NPz = replace_all(k*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
          #NPz = replace_all((k+K*MPz**2/L0**2)*MPz/(k+K*(2*MPz**2/L0**2+wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
          #NPvec = replace_all(NPz*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
          #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
          #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
          #print('inv vec', inv_norm_vec_j)
          # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
          vec_j = wire_j.vector#monovar(wire_j.vector,dw,2,var_dw)
          Forcej3 = replace_all(-K*(1-L0/(NPvec).magnitude())*NPvec,self,self.BHQSsus)


          #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
          #sum_vec_wire_bot+=vec_j
          #print('daughter',vec_j)
         # Forcej = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))*vec_j
          #Forces = Forces+ Forcej
          XiBj=replace_all(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),self,self.BHQSsus)
          #print(inv_bot)
          #print(XiBj)
          #torq_vec_wire_bot+=XiBj.cross(vec_j)

          Forces += replace_all(Forcej3,self,self.BHQSsus)#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
          quad = QuadSus()
          flattened_var1 = ()
          for sub_var in quad.eq1, quad.eq2, quad.eq3, quad.eq4:
            flattened_var1 += sub_var

          eq = flattened_var

          for l in range(len(quad.var)):
            Forcej3 = Forcej3.subs({quad.var[l]:0})
          # F = F.subs({eq[i]:eq_[i]})
          for l in range(len(quad.var_input)):
            Forcej3 = Forcej3.subs({quad.var_input[l]:0})
          #print('Forcej3 = ',Forcej3)
        #print(Forces)
          Torques+= replace_all(XiBj.cross(Forcej3),self,self.BHQSsus)
      #print(Torques)
    #print(Forces)
    #print(Forces)
    #Forces = series_vec_monovar(Forces,reference_frame, qk,2,var_glob)
    #print(Forces)
    #Torques += inv_top*torq_vec_wire_top+inv_bot*torq_vec_wire_bot
    #Torques = series_vec_monovar(Torques,reference_frame, qk,2,var_glob)
    #print(Torques)
    #print(Torques)

    # print('lin forces',Forces)

    #Forces  = Forces.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    #Forces = Forces.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    #Forces = Forces.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    #Forces = Forces.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    #Forces = series_vec_monovar(Forces,reference_frame,dw,2,var_dw)

    #Torques  = Torques.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop, self.Frame.dbot:0,self.mass1.dtop:0})
    #Torques = Torques.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop, self.mass1.dbot:0,self.mass2.dtop:0})
    #Torques = Torques.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop, self.mass2.dbot:0,self.mass3.dtop:0})
    #Torques = Torques.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop, self.mass3.dbot:0,self.mass4.dtop:0})
    #Torques = series_vec_monovar(Torques,reference_frame,dw,2,var_dw)
    #print(Torques)
    #print(Forces)
    Forces = replace_all(Forces,self,self.BHQSsus)
    F0[6*i] = Forces.dot(reference_frame.x)#.series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i}',F0[6*i])
    F0[6*i+1] = Forces.dot(reference_frame.y)#.series(dw,0,2).removeO()/mass_i.mass
   # print(f'F0{6*i+1}',F0[6*i+1])
    F0[6*i+2] = Forces.dot(reference_frame.z)#.series(dw,0,2).removeO()/mass_i.mass
    #print(f'F0{6*i+2}',F0[6*i+2])
    F0[6*i+3] = Torques.dot(reference_frame.x)#.series(dw,0,2).removeO()/mass_i.Ixx
    #print(f'F0{6*i+3}',F0[6*i+3])
    F0[6*i+4] = Torques.dot(reference_frame.y)#.series(dw,0,2).removeO()/mass_i.Iyy
    #print(f'F0{6*i+4}',F0[6*i+4])
    F0[6*i+5] =Torques.dot(reference_frame.z)#.series(dw,0,2).removeO()/mass_i.Izz
   # print(f'F0{6*i+5}',F0[6*i+5])
  return F0




def get_ss_simp_all(masses, wires, var, var_input, reference_frame, g,self):

  flattened_var = ()
  for sub_var in var, var_input:
    flattened_var += sub_var
  var_glob = flattened_var


  n_masses = len(masses)
  n_wires = len(wires)

  A = zeros(n_masses*6,n_masses*6)
  B = zeros(n_masses*6,6)
  for kk, qk in enumerate(var_glob):

    for i, key_i in enumerate(masses):
      mass_i = masses[key_i]
      

      Forces = -mass_i.mass*g*reference_frame.z
      sum_vec_wire_top = 0*reference_frame.z
      sum_vec_wire_bot = 0*reference_frame.z
      torq_vec_wire_top = 0*reference_frame.z
      torq_vec_wire_bot = 0*reference_frame.z
      Torques = 0*reference_frame.x

      for j, key_j in enumerate(wires):
        
        wire_j = wires[key_j]
        MPx = replace(wire_j.vector.dot(mass_i.ReferenceFrame.x),self,self.BHQSsus)
        MPy = replace(wire_j.vector.dot(mass_i.ReferenceFrame.y),self,self.BHQSsus)
        MPz = replace(wire_j.vector.dot(mass_i.ReferenceFrame.z),self,self.BHQSsus)
        #print('MPz',MPz)
        delta_k = wire_j.delta_k
        K = wire_j.K
        k = wire_j.k+delta_k
        L0 = wire_j.L0



        if wire_j.mother_mass_number == mass_i.mass_number :
            #mass_i_d = masses[key_i+1]
            Bj = getattr(mass_i,str(wire_j.mother_point))
            M_star = set_point('1', [0,0,(mass_i.mass_hang-mass_i.mass)*g/4/k], Bj, mass_i.ReferenceFrame, mass_i.orient, 0)
            #print(mass_i.mass_hang-mass_i.mass)
            M_starM = pos(M_star,getattr(mass_i,str(wire_j.mother_point)),self.ReferenceFrame)
            M_starMz = M_starM.dot(mass_i.ReferenceFrame.z)
            M_starNz4xy = replace(K*(1-L0/wire_j.vector.magnitude())*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
            
            #print('MstarMMOM', replace(M_starM,self,self.BHQSsus),'len',replace(mass_i.mass_hang*g/4/k,self,self.BHQSsus))
            #MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)/2*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
           # M_starNz = replace(K*(1-L0/wire_j.vector.magnitude())*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
            M_starNz = replace(K*(MPz-M_starMz-L0)/(k+K),self,self.BHQSsus)


            #M_starNz = replace((mass_i.mass_hang-mass_i.mass)*g/4/k/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K)*(MPz-M_starMz)),self,self.BHQSsus)
            #M_starNz4xy = replace((mass_i.mass_hang-mass_i.mass)*g/4/k/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K)*(MPz-M_starMz)),self,self.BHQSsus)



          # MNvec = replace_all(MNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)
            M_starNvec = replace(M_starNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)
            M_starNvecxy = replace(M_starNz4xy*mass_i.ReferenceFrame.z,self,self.BHQSsus)
            MNvec = replace(M_starNvecxy-M_starM,self,self.BHQSsus)
            #NPz = replace_all(k*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
            NPz4xy = replace(k*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
            NPz = replace((k*(MPz-M_starMz)+K*L0)/(k+K),self,self.BHQSsus)


            #NPz4xy = replace((L0+(mass_i.mass_hang-mass_i.mass)*g/4/k)/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K)*(MPz-M_starMz)),self,self.BHQSsus)
            #NPz = replace((L0+(mass_i.mass_hang-mass_i.mass)*g/4/k)/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K)*(MPz-M_starMz)),self,self.BHQSsus)



           # NPz = serie_monovar()
            NPvec = replace(NPz*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
            NPvecxy = replace(NPz4xy*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
            #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
            #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
            #print('inv vec', inv_norm_vec_j)
            # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
            vec_j = wire_j.vector#monovar(wire_j.vector,dw,2,var_dw)
            #inv_top = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
            #sum_vec_wire_top+=vec_j
            #print(vec_j)
            #sum_vec_wires += vec_j
            #print('mother',vec_j)
            #Forcej = wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))*vec_j
            if mass_i.mass_number==3:
              NPvec = wire_j.vector
              NPvecxy = wire_j.vector
              M_starNvec = (1-L0/wire_j.vector.magnitude())*wire_j.vector.dot(mass_i.ReferenceFrame.z)*mass_i.ReferenceFrame.z
              k=K
              M_starM =0
              MNvec = replace(M_starNvec-M_starM,self,self.BHQSsus)

            Forcej1 = M_starNvec*k
            Forcej2 = K*(1-L0/(NPvecxy).magnitude())*(MPx*mass_i.ReferenceFrame.x+MPy*mass_i.ReferenceFrame.y)
            NPn =NPvec.magnitude()
            MNn =MNvec.magnitude()
            deltalen = wire_j.vector.magnitude()**2/L0**2-1
            #quad = QuadSus()
            #flattened_var1 = ()

            #for sub_var in quad.eq1, quad.eq2, quad.eq3, quad.eq4:
            #  flattened_var1 += sub_var

            #eq = flattened_var

            #for l in range(len(quad.var)):
            #  Forcej1 = Forcej1.subs({quad.var[l]:0})
            #  Forcej2 = Forcej2.subs({quad.var[l]:0})
            #  NPn = NPn.subs({quad.var[l]:0})
            #  MNn = MNn.subs({quad.var[l]:0})
            #  deltalen = deltalen.subs({quad.var[l]:0})
            # F = F.subs({eq[i]:eq_[i]})
            #for l in range(len(quad.var_input)):
            #  Forcej1 = Forcej1.subs({quad.var_input[l]:0})
            #  Forcej2 = Forcej2.subs({quad.var_input[l]:0})
            #print('Forcej2 = ',Forcej2)
            #print('Forcej1 = ',Forcej1)
            #print('NPn = ',NPn)
            #print('MNn = ',MNn)
            #print('deltalen = ',replace(deltalen,self,self.BHQSsus))
            #Forces = Forces+ Forcej
            XiBj= replace(pos(getattr(mass_i,str(wire_j.mother_point)), mass_i.loc,reference_frame),self,self.BHQSsus)
            Bj = getattr(mass_i,str(wire_j.mother_point))
            P1 = set_point('1', [0,0,mass_i.mass_hang*g/4/k], Bj, mass_i.ReferenceFrame, mass_i.orient, 0)
            XiBj_real = replace(pos(P1, mass_i.loc,reference_frame),self,self.BHQSsus)
            #def __init__(self, mass_number, reference_frame_origin, ReferenceFrame, eq, tol):
            #torq_vec_wire_top+=XiBj.cross(vec_j)

            #print(inv_top)
            #print('Forcej1: '+str(Forcej1))

            Forces += replace(Forcej1+Forcej2,self,self.BHQSsus)#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
            #print('Forces_mom: '+str(Forces))
          #print(Forces)
            XiBj_ = XiBj+MNvec
            Torques+= replace(XiBj.cross(Forcej1)+XiBj.cross(Forcej2),self,self.BHQSsus)
        elif wire_j.daughter_mass_number == mass_i.mass_number :
            keys_masses = list(masses.keys())
           # print('i=',i)
            if i==0:
              mass_i_up = self.Frame
              Bj = getattr(mass_i_up,str(wire_j.mother_point))
              M_star = set_point('1', [0,0,mass_i_up.mass_hang*g/4/k], Bj, mass_i_up.ReferenceFrame, mass_i_up.orient, 0)
            elif mass_i.mass_number==4:
              mass_i_up = masses[keys_masses[i-1]]
              Bj = getattr(mass_i_up,str(wire_j.mother_point))
              M_star = Bj
            else:
              mass_i_up = masses[keys_masses[i-1]]
             # print(keys_masses[i-1])
              Bj = getattr(mass_i_up,str(wire_j.mother_point))
              M_star = set_point('1', [0,0,(mass_i_up.mass_hang-mass_i_up.mass)*g/4/k], Bj, mass_i_up.ReferenceFrame, mass_i_up.orient, 0)

            #print('massnumber',mass_i_up,'i',i)
          # Bj = getattr(mass_i_up,str(wire_j.mother_point))

            M_starM = pos(M_star,getattr(mass_i_up,str(wire_j.mother_point)),self.ReferenceFrame)
            #print('MstarM', M_starM)
            M_starMz = M_starM.dot(mass_i.ReferenceFrame.z)
            #print('MstarM', replace(M_starM,self,self.BHQSsus))
            M_starNz4xy = replace(K*(1-L0/wire_j.vector.magnitude())*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
            M_starNz = replace(K*(MPz-M_starMz-L0)/(k+K),self,self.BHQSsus)

            NPz4xy = replace(k*(MPz-M_starMz)/(k+K*(1-L0/wire_j.vector.magnitude())),self,self.BHQSsus)
            NPz = replace((k*(MPz-M_starMz)+K*L0)/(k+K),self,self.BHQSsus)
            M_starNvec = replace(M_starNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)

            #M_starNz = replace((mass_i.mass_hang-mass_i.mass)*g/4/k/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K))*(MPz-M_starMz),self,self.BHQSsus)
            #M_starNz4xy = replace((mass_i.mass_hang-mass_i.mass)*g/4/k/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K))*(MPz-M_starMz),self,self.BHQSsus)

            #NPz4xy = replace((L0+(mass_i.mass_hang-mass_i.mass)*g/4/k)/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K))*(MPz-M_starMz),self,self.BHQSsus)
            #NPz = replace((L0+(mass_i.mass_hang-mass_i.mass)*g/4/k)/(L0+(mass_i.mass_hang-mass_i.mass)*g/4/k/K*(k+K))*(MPz-M_starMz),self,self.BHQSsus)
            if mass_i.mass_number==4:
              NPvec = wire_j.vector
              NPvecxy = wire_j.vector
            else:
              NPvec = replace(NPz*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
              NPvecxy = replace(NPz4xy*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
            #MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)/2*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
            #MNz = replace_all(K*(wire_j.vector.magnitude()**2/L0**2-1)/2*MPz/(k+K*(2*MPz**2/L0**2+wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
            #MNvec = replace_all(MNz*mass_i.ReferenceFrame.z,self,self.BHQSsus)
            #NPz = replace_all(k*MPz/(k+K*(wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
            #NPz = replace_all((k+K*MPz**2/L0**2)*MPz/(k+K*(2*MPz**2/L0**2+wire_j.vector.magnitude()**2/L0**2-1)/2),self,self.BHQSsus)
            #NPvec = replace_all(NPz*mass_i.ReferenceFrame.z+MPy*mass_i.ReferenceFrame.y+MPx*mass_i.ReferenceFrame.x,self,self.BHQSsus)
            #inv_norm_vec_j = factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var)))
            #inv_norm_vec_j = series_monovar(1/replace(wire_j.vector.magnitude(),self,self.BHQSsus),dw,2,var_dw)
            #print('inv vec', inv_norm_vec_j)
            # vec_j = series_vec_monovar(wire_j.vector,reference_frame,qk,2,var)
            vec_j = wire_j.vector#monovar(wire_j.vector,dw,2,var_dw)
            Forcej3 = replace(-K*(1-L0/(NPvec).magnitude())*NPvec,self,self.BHQSsus).dot(mass_i.ReferenceFrame.z)*mass_i.ReferenceFrame.z+ replace(-K*(1-L0/(NPvecxy).magnitude())*NPvecxy,self,self.BHQSsus).dot(mass_i.ReferenceFrame.x)*mass_i.ReferenceFrame.x+ replace(-K*(1-L0/(NPvecxy).magnitude())*NPvecxy,self,self.BHQSsus).dot(mass_i.ReferenceFrame.y)*mass_i.ReferenceFrame.y
            #Forcej34xy = 


            #inv_bot = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))
            #sum_vec_wire_bot+=vec_j
            #print('daughter',vec_j)
          # Forcej = -wire_j.K*(1-wire_j.L0*factor(cancel(series_monovar(1/wire_j.vector.magnitude(),qk,2,var_glob))))*vec_j
            #Forces = Forces+ Forcej
            XiBj=replace(pos(getattr(mass_i,str(wire_j.daughter_point)), mass_i.loc,reference_frame),self,self.BHQSsus)
            #print(inv_bot)
            #print(XiBj)
            #torq_vec_wire_bot+=XiBj.cross(vec_j)

            Forces += replace(Forcej3,self,self.BHQSsus)#inv_top*sum_vec_wire_top +inv_bot*sum_vec_wire_bot
            #quad = QuadSus()
            #flattened_var1 = ()
            #for sub_var in quad.eq1, quad.eq2, quad.eq3, quad.eq4:
            #  flattened_var1 += sub_var

            #eq = flattened_var

            #for l in range(len(quad.var)):
            #  Forcej3 = Forcej3.subs({quad.var[l]:0})
            # F = F.subs({eq[i]:eq_[i]})
            #for l in range(len(quad.var_input)):
            #  Forcej3 = Forcej3.subs({quad.var_input[l]:0})
            #print('Forcej3 = ',Forcej3)
          #print(Forces)
            Torques+= replace(XiBj.cross(Forcej3),self,self.BHQSsus)

      # print('lin forces',Forces)
      #print(Torques)
     # print(Forces)
      Forces = replace(Forces,self,self.BHQSsus)
      Torques = replace(Torques,self,self.BHQSsus)
      if qk not in var_input:
       # qk = var[-1]
        Akix, Akiy, Akiz, Akialphax, Akialphay, Akialphaz = get_Aki_Bi(Forces, Torques, reference_frame, qk, var_glob)
        A[kk,6*i], A[kk,6*i+1], A[kk,6*i+2], A[kk,6*i+3], A[kk,6*i+4], A[kk,6*i+5] = replace(Akix/mass_i.mass,self,self.BHQSsus), replace(Akiy/mass_i.mass,self,self.BHQSsus), replace(Akiz/mass_i.mass,self,self.BHQSsus), replace(Akialphax/mass_i.Ixx,self,self.BHQSsus), replace(Akialphay/mass_i.Iyy,self,self.BHQSsus), replace(Akialphaz/mass_i.Izz,self,self.BHQSsus)
        print('A'+str(kk)+str(i)+'x', Akix/replace(mass_i.mass,self,self.BHQSsus))
        print('A'+str(kk)+str(i)+'y', Akiy/replace(mass_i.mass,self,self.BHQSsus))
        print('A'+str(kk)+str(i)+'z', Akiz/replace(mass_i.mass,self,self.BHQSsus))
        print('A'+str(kk)+str(i)+'Rx', Akialphax/replace(mass_i.Ixx,self,self.BHQSsus))
        print('A'+str(kk)+str(i)+'Ry', Akialphay/replace(mass_i.Iyy,self,self.BHQSsus))
        print('A'+str(kk)+str(i)+'Rz', Akialphaz/replace(mass_i.Izz,self,self.BHQSsus))

      else:
        Bix, Biy, Biz, Bialphax, Bialphay, Bialphaz = get_Aki_Bi(Forces, Torques, reference_frame, qk, var_glob)
        B[6*i,kk-6*n_masses],B[6*i+1,kk-6*n_masses],B[6*i+2,kk-6*n_masses],B[6*i+3,kk-6*n_masses],B[6*i+4,kk-6*n_masses],B[6*i+5,kk-6*n_masses] = replace(Bix/mass_i.mass,self,self.BHQSsus), replace(Biy/mass_i.mass,self,self.BHQSsus), replace(Biz/mass_i.mass,self,self.BHQSsus), replace(Bialphax/mass_i.Ixx,self,self.BHQSsus), replace(Bialphay/mass_i.Iyy,self,self.BHQSsus), replace(Bialphaz/mass_i.Izz,self,self.BHQSsus)

        print('Bix', Bix) # B[6*i,0],  B[6*i,0] = B[6*i,0] + Bix

  return A, B





class Mass:

  def __init__(self, mass_number, reference_frame_origin, ReferenceFrame, eq, tol):
    self. mass_number = mass_number
    self. mass = symbols('M'+str(self.mass_number), real = True, positive = True)
    self. L = symbols('L'+str(self.mass_number), real = True, positive = True)
    self. ReferenceFrame = ReferenceFrame
    self. reference_frame_origin = reference_frame_origin
    qx, qy, qz, alphax, alphay, alphaz = symbols('q'+str(self.mass_number)+'x, q'+str(self.mass_number)+'y, q'+str(self.mass_number)+'z, alpha'+str(self.mass_number)+'x, alpha'+str(self.mass_number)+'y, alpha'+str(self.mass_number)+'z')
    self. var = qx,qy,qz,alphax, alphay, alphaz
    self.eq = eq

    qx0,qy0,qz0,alphax0,alphay0,alphaz0 = eq[0],eq[1],eq[2],eq[3],eq[4],eq[5]
    self. loc = set_point('X'+str(self.mass_number),[self.var[0]+qx0,self.var[1]+qy0,self.var[2]+qz0-self. L],self.reference_frame_origin, self.ReferenceFrame,eye(3),tol)
    self. orient =  Matrix([[1, -alphaz-alphaz0, alphay+alphay0],[alphaz+alphaz0,1,-alphax-alphax0],[-alphay-alphay0, alphax+alphax0, 1]])
    Ixx,Iyy,Izz = symbols('Ixx'+str(self.mass_number)+', Iyy'+str(self.mass_number)+', Izz'+str(self.mass_number), real = True, positive = True)
    self. Ixx = Ixx
    self. Iyy = Iyy
    self. Izz = Izz
    self.tol = tol


  def setup(self, mass, Inertia, coordinates, orientation):
    #setattr(self, 'mass_number', mass_number)
    #setattr(self, 'mass', mass,, real = True, positive = True)
    #setattr(self, 'Inertia', Inertia)
    #setattr(self, 'coordinates', coordinates)
    X = set_point('X'+str(self.mass_number),self.coordinates[0]*self.ReferenceFrame.x+ self.coordinates[1]*self.ReferenceFrame.y+ self.coordinates[2]*self.ReferenceFrame.z, self.reference_frame_origin, self.ReferenceFrame, False)
    #X = 'X'+str(self.mass_number)
    #X_ = Point(X)
    #X_.set_pos(self.reference_frame_origin, self.coordinates[0]*self.ReferenceFrame.x+ self.coordinates[1]*self.ReferenceFrame.y+ self.coordinates[2]*self.ReferenceFrame.z, tol = False)
    setattr(self, 'center_of_mass', X)
    setattr(self, 'orientation', orientation)
    #self. mass_number = mass_number
    #self. mass = mass
    #self. Inertia = Inertia
    #self. coordinates = coordinates



    #self. orientation = orientation
    #self.center_of_mass=X_



class Mass4x4(Mass):

  def __init__(self, *args,dw_tol=True,):
    super().__init__(*args)
    #self. TopAttachmendw = tPoints = []
    self. wtop  = symbols('w'+str(2*self.mass_number-1), real = True, positive = True)
    self. ntop  = symbols('n'+str(2*self.mass_number-1), real = True, positive = True)
    self. dtop  = symbols('d'+str(2*self.mass_number-1))
    #self. tol = tol
    self. dw = symbols('δw', real = True)
   # self.topRec = wtop,ntop,dtop

    self. wbot  = symbols('w'+str(2*self.mass_number), real = True, positive = True)
    self. nbot  = symbols('n'+str(2*self.mass_number), real = True, positive = True)
    self. dbot  = symbols('d'+str(2*self.mass_number))

    #self.botRec = wbot,nbot,dbot

  #def SetTopAttachmentPoints(self, w, n, d):
    B_up_1 = 'Bup'+str(self.mass_number)+'1'
    B_up_2 = 'Bup'+str(self.mass_number)+'2'
    B_up_3 = 'Bup'+str(self.mass_number)+'3'
    B_up_4 = 'Bup'+str(self.mass_number)+'4'

    P1,P2,P3,P4 = set_rectangle_points('Bup'+str(self.mass_number), self.loc, self.ReferenceFrame, self. wtop, self. ntop, self. dtop, self.orient, self.dw, self.tol)
    setattr(self,B_up_1, P1)
    setattr(self,B_up_2, P2)
    setattr(self,B_up_3, P3)
    setattr(self,B_up_4, P4)
    #self.tol = False
    #print('4 points have been created. they are located with respect to the center of mass '+str(self.center_of_mass)+' in\n')
   # print(B_up_1+' : ', pos(getattr(self,B_up_1),self.center_of_mass, self.ReferenceFrame))
   # print(B_up_2+' : ', pos(getattr(self,B_up_2),self.center_of_mass, self.ReferenceFrame))
   # print(B_up_3+' : ', pos(getattr(self,B_up_3),self.center_of_mass, self.ReferenceFrame))
   # print(B_up_4+' : ', pos(getattr(self,B_up_4),self.center_of_mass, self.ReferenceFrame))

  #def SetBottomAttachmentPoints(self, w, n, d):
    B_bot_1 = 'Bbot'+str(self.mass_number)+'1'
    B_bot_2 = 'Bbot'+str(self.mass_number)+'2'
    B_bot_3 = 'Bbot'+str(self.mass_number)+'3'
    B_bot_4 = 'Bbot'+str(self.mass_number)+'4'

    P1,P2,P3,P4 = set_rectangle_points('Bbot'+str(self.mass_number), self.loc, self.ReferenceFrame, self.wbot, self.nbot, -self.dbot, self.orient, self.dw, self.tol)
    setattr(self,B_bot_1, P1)
    setattr(self,B_bot_2, P2)
    setattr(self,B_bot_3, P3)
    setattr(self,B_bot_4, P4)

   # print('4 points have been created. they are located with respect to the center of mass '+str(self.center_of_mass)+ ' in\n')
   # print(B_bot_1+' : ', pos(getattr(self,B_bot_1),self.center_of_mass, self.ReferenceFrame))
   # print(B_bot_2+' : ', pos(getattr(self,B_bot_2),self.center_of_mass, self.ReferenceFrame))
   # print(B_bot_3+' : ', pos(getattr(self,B_bot_3),self.center_of_mass, self.ReferenceFrame))
   # print(B_bot_4+' : ', pos(getattr(self,B_bot_4),self.center_of_mass, self.ReferenceFrame))


class Mass4x0(Mass):

  def __init__(self, *args, dw_tol = False):
    super().__init__(*args)

    self. wtop  = symbols('w'+str(2*self.mass_number-1), real = True, positive = True)
    self. ntop  = symbols('n'+str(2*self.mass_number-1), real = True, positive = True)
    self. dtop  = symbols('d'+str(2*self.mass_number-1))
    self. dw = symbols('δw', real = True)
   # self.topRec = wtop,ntop,dtop

    #self. TopAttachmentPoints = []

 # def SetTopAttachmentPoints(self, self. wtop, self.ntop, self.dtop):
    B_up_1 = 'Bup'+str(self.mass_number)+'1'
    B_up_2 = 'Bup'+str(self.mass_number)+'2'
    B_up_3 = 'Bup'+str(self.mass_number)+'3'
    B_up_4 = 'Bup'+str(self.mass_number)+'4'

    P1,P2,P3,P4 = set_rectangle_points('Bup'+str(self.mass_number), self.loc, self.ReferenceFrame, self.wtop, self.ntop, self.dtop, self.orient, self.dw, self.tol)
    setattr(self,B_up_1, P1)
    setattr(self,B_up_2, P2)
    setattr(self,B_up_3, P3)
    setattr(self,B_up_4, P4)

   # print('4 points have been created. they are located with respect to the center of mass '+str(self.center_of_mass)+' in\n')
    #print(B_up_1+' : ', pos(getattr(self,B_up_1),self.center_of_mass, self.ReferenceFrame))
    #print(B_up_2+' : ', pos(getattr(self,B_up_2),self.center_of_mass, self.ReferenceFrame))
    #print(B_up_3+' : ', pos(getattr(self,B_up_3),self.center_of_mass, self.ReferenceFrame))
    #print(B_up_4+' : ', pos(getattr(self,B_up_4),self.center_of_mass, self.ReferenceFrame))


class Frame:
  def __init__(self, reference_frame_origin, ReferenceFrame, tol):
    self.mass_number = 0
    self.reference_frame_origin = reference_frame_origin
    self.ReferenceFrame = ReferenceFrame
    self.tol = tol
    qx, qy, qz, alphax, alphay, alphaz = symbols('q'+str(self.mass_number)+'x, q'+str(self.mass_number)+'y, q'+str(self.mass_number)+'z, alpha'+str(self.mass_number)+'x, alpha'+str(self.mass_number)+'y, alpha'+str(self.mass_number)+'z')
    self. var = qx,qy,qz,alphax, alphay, alphaz
    self. loc = set_point('X'+str(self.mass_number),[self.var[0],self.var[1],self.var[2]],self.reference_frame_origin, self.ReferenceFrame,eye(3),tol)
    self. orient =  Matrix([[1, -alphaz, alphay],[alphaz,1,-alphax],[-alphay, alphax, 1]])

    self. wbot  = symbols('w'+str(2*self.mass_number), real = True, positive = True)
    self. nbot  = symbols('n'+str(2*self.mass_number), real = True, positive = True)
    self. dbot  = symbols('d'+str(2*self.mass_number))
    self. dw = symbols('δw', real = True)

  #  self.topRec = wtop,ntop,dtop


  #def SetFrameAttachmentPoints(self,w,n,d):
    P01 = 'Bbot01'
    P02 = 'Bbot02'
    P03 = 'Bbot03'
    P04 = 'Bbot04'

    P1,P2,P3,P4 = set_rectangle_points('Bbot'+str(self.mass_number), self.loc, self.ReferenceFrame, self.wbot, self.nbot, 0, self.orient, self.dw, self.tol)
    setattr(self, P01, P1)
    setattr(self, P02, P2)
    setattr(self, P03, P3)
    setattr(self, P04, P4)
    #print(pos(self.Bbot01,self.reference_frame_origin,self.ReferenceFrame))

   # print('4 points have been created. they are located with respect to the Origin in\n')
   # print(P01+' : ', pos(P1,self.center_of_mass, self.ReferenceFrame))
   # print(P02+' : ', pos(P2,self.center_of_mass, self.ReferenceFrame))
   # print(P03+' : ', pos(P3,self.center_of_mass, self.ReferenceFrame))
   # print(P04+' : ', pos(P4,self.center_of_mass, self.ReferenceFrame))


#Definition of a Wire
class Wire:
  def __init__(self, mother_point, mother_mass_number, daughter_point, daughter_mass_number, ReferenceFrame ):
    self.ReferenceFrame = ReferenceFrame
    #X is the center of mass point
    if mother_mass_number<daughter_mass_number:
      top = mother_mass_number
      bot = daughter_mass_number
    else:
      bot = mother_mass_number
      top = daughter_mass_number
    self.L0 = symbols('L0'+str(bot), real = True, positive = True)
    self. K = symbols('K'+str(bot), real = True, positive = True)

    self. mother_point =  mother_point
    self. mother_mass_number = mother_mass_number
    self. daughter_point = daughter_point
    self. daughter_mass_number  =  daughter_mass_number
    self. vector = pos(daughter_point, mother_point, self.ReferenceFrame)
    self. vectornum = pos(daughter_point, mother_point, self.ReferenceFrame)
    self. dirvector = pos(daughter_point, mother_point,  self.ReferenceFrame, normalize=True)
    self. dirvectornum = pos(daughter_point, mother_point,  self.ReferenceFrame, normalize=True)
    self.name = str(mother_point)+str(daughter_point)
    self.k = symbols('k'+str(bot), real = True, positive = True)
    k_tol = np.random.uniform(-.05,0.05)
    self. delta_k = self. k*k_tol

class BHQS:
  def __init__(self):
    self. M1 = 117
    self. M2 = 83
    self. M3 = 100
    self. M4 = 100
    self. g = 9.81

    self. Ixx1 = 8.1
    self. Ixx2 = 3.6
    self. Ixx3 = 2.6
    self. Ixx4 = 2.6

    self. Iyy1 = 8.1
    self. Iyy2 = 3.3
    self. Iyy3 = 1.9
    self. Iyy4 = 1.9

    self. Izz1 = 15.5
    self. Izz2 = 6.7
    self. Izz3 = 1.9
    self. Izz4 = 1.9

    self. L1 = 0.34
    self. L2 = 0.34
    self. L3 = 0.34
    self. L4 = 0.60

    self. k1 = 7.2e3/2
    self. k2 = 6.5e3/2
    self. k3 = 3.6e3/2
    self. k4 = 0

    self.K1 = 1.18e6/2
    self.K2 = 4.8e5/2
    self.K3 = 3.9369e5/2
    self. Y4 = 72e9
    self. r4 = 220e-6
    self. K4 = self.Y4*np.pi*self.r4**2/self.L4-self.M4*self.g/4/self.L4
    #print(self.K4)

    self. d0 = 0
    self. d1 = 0
    self. d2 = 0
    self. d3 = 0
    self. d4 = 0
    self. d5 = 0
    self. d6 = 0
    self. d7 = 0

    self. w0 = 16e-2
    self. w1 = 16e-2
    self. w2 = 14e-2
    self. w3 = 14e-2
    self. w4 = 12.5e-2
    self. w5 = 12.5e-2
    self. w6 = 2.5e-2
    self. w7 = 2.5e-2


    self. n0 = 18e-2
    self. n1 = 18e-2
    self. n2 = 13.5e-2
    self. n3 = 13.5e-2
    self. n4 = 23e-2
    self. n5 = 23e-2
    self. n6 = 23e-2
    self. n7 = 23e-2

    self. L01 = self.L1-(self.M1+self.M2+self.M3+self.M4)*self.g/4/self.K1
    self. L02 = self.L2-(self.M2+self.M3+self.M4)*self.g/4/self.K2
    self. L03 = self.L3-(self.M3+self.M4)*self.g/4/self.K3
    self. L04 = self.L4-(self.M4)*self.g/4/self.K4









g=9.81
class QuadSus:

  def __init__(self, tol = False):
    self. masses_info = {}
    self. wires_info = {}
    self. masses = {}
    self. wires = {}
    self. tol = tol
    self.g = symbols('g', real=True, positive= True)
    N = ReferenceFrame('N')
    self. ReferenceFrame = N # this is the global frame
    N_a = ReferenceFrame('N_a') # this is the  frame of suspension point
    self.O = Point('O')
    self. Frame = Frame(self.O, self.ReferenceFrame, tol)
    self. eq1 = symbols('q1x0 q1y0 q1z0 alpha1x0 alpha1y0 alpha1z0')
    self.eq1 = 0,0,0,0,0,0
    self. mass1 = Mass4x4(1, self.O, self.ReferenceFrame, self.eq1,tol, dw_tol = False)
    self. eq2 = symbols('q2x0 q2y0 q2z0 alpha2x0 alpha2y0 alpha2z0')
    self.eq2 = 0,0,0,0,0,0
    self. mass2 = Mass4x4(2, self.O, self.ReferenceFrame,self.eq2, tol, dw_tol = False)
    self. eq3 = symbols('q3x0 q3y0 q3z0 alpha3x0 alpha3y0 alpha3z0')
    self.eq3 = 0,0,0,0,0,0
    self. mass3 = Mass4x4(3, self.O, self.ReferenceFrame,self.eq3, tol, dw_tol = False)
    self. eq4 = symbols('q4x0 q4y0 q4z0 alpha4x0 alpha4y0 alpha4z0')
    self.eq4 = 0,0,0,0,0,0
    self. mass4 = Mass4x0(4, self.O, self.ReferenceFrame, self.eq4,tol)

    self.mass1.mass_hang = self.mass1.mass+self.mass2.mass+self.mass3.mass+self.mass4.mass
    self.mass2.mass_hang = self.mass2.mass+self.mass3.mass+self.mass4.mass
    self.mass3.mass_hang = self.mass3.mass+self.mass4.mass
    self.mass4.mass_hang = self.mass4.mass
    self.Frame.mass_hang = self.mass1.mass+self.mass2.mass+self.mass3.mass+self.mass4.mass
    self. masses['mass1'] = self.mass1
    self. masses['mass2'] = self.mass2
    self. masses['mass3'] = self.mass3
    self. masses['mass4'] = self.mass4
    self.BHQSsus = BHQS()
    flattened_var = ()
    for sub_var in self.mass1.var, self.mass2.var, self.mass3.var, self.mass4.var:
      flattened_var += sub_var
    self.var = flattened_var
    self.var_input = self.Frame.var
    self.C = Matrix.hstack(eye(len(self.var)),zeros(len(self.var),len(self.var)))
    self. D = zeros(len(self.var),len(self.var_input))
    self. A_star = zeros(6*len(self.masses),6*len(self.masses))
    self. B_star = zeros(6*len(self.masses),6)
    self. A = zeros(2*len(self.var),2*len(self.var))
    self. B = zeros(2*len(self.var),len(self.var_input))

    print("The Quad suspension body has been created. It consists of 4 masses, each suspended by 4 wires.\n")
    #print("To define the position of the reference frame's attachment points, use the .Frame.SetFrameAttachmentPoints(w, n, d) method, where w is the half-length along the x axis, n the half-length along the y axis. \n")
    #print("To define the dynamic properties of mass 1, use the .mass1.setup(mass, Inertia, coordinates, orientation) method, where coordinates is a list [x,y,z] defined in the reference frame.\n")
    #print("To define the position of the top attachment points of mass 1, use method .mass1.SetTopAttachmentPoints(w,d,n) where [w,n,d] is the position of an attachment point relative to the mass1 reference located at its center of mass.\n")
    #print("To define the position of the lower attachment points of mass 1, use method .mass1.SetTopAttachmentPoints(w,d,n) or n<0.")
    #print("Repeat for the other masses.\n")
    #print("For a model including manufacturing errors, reload QuadSus(tol=...) by precising the tolerance of precision (in m). Then, reload all the following commands.\n")
    #print("When the suspension has been fully built, use .ComputeWiresTensions()")
  # def mass1(self,*args):
  #   setattr(self, 'mass1', Mass4x4(1,*args))
  #   self. masses['mass1'] = self.mass1

    connect4x4(self.Frame, self.mass1, self.wires, self.ReferenceFrame)
    connect4x4(self.mass1, self.mass2, self.wires, self.ReferenceFrame)
    connect4x4(self.mass2, self.mass3, self.wires, self.ReferenceFrame)
    connect4x4(self.mass3, self.mass4, self.wires, self.ReferenceFrame)

  def get_equilibrium(self):
      A_star,B_star = get_ss_simp_all(self.masses, self.wires, self.var, self.var_input, self.ReferenceFrame,self.g,self)
      self.A_star = A_star
      self.B_star = B_star

  def Hang(self):
    self.mass1.L_= self.wires['Bbot01Bup11'].L0+(self.mass1.mass+self.mass2.mass+self.mass3.mass+self.mass4.mass)*self.g/4/self.wires['Bbot01Bup11'].K
    self.mass2.L_= self.wires['Bbot11Bup21'].L0+(self.mass2.mass+self.mass3.mass+self.mass4.mass)*self.g/4/self.wires['Bbot11Bup21'].K-self.mass1.L_
    self.mass3.L_= self.wires['Bbot21Bup31'].L0+(self.mass3.mass+self.mass4.mass)*self.g/4/self.wires['Bbot21Bup31'].K-self.mass2.L_
    self.mass4.L_= self.wires['Bbot31Bup41'].L0+(self.mass4.mass)*self.g/4/self.wires['Bbot31Bup41'].K-self.mass3.L_

    A = self.A_star.subs({self.mass1.L: self.mass1.L_})
    A = A.subs({self.mass2.L: self.mass2.L_})
    A = A.subs({self.mass3.L: self.mass3.L_})
    A = A.subs({self.mass4.L: self.mass4.L_})
    self.A_star = A

    B = self.B_star.subs({self.mass1.L: self.mass1.L_})
    B = B.subs({self.mass2.L: self.mass2.L_})
    B = B.subs({self.mass3.L: self.mass3.L_})
    B = B.subs({self.mass4.L: self.mass4.L_})
    self.B_star = B


  def VerticalWires(self):
    A = self.A_star.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop})
    A = A.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop})
    A = A.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop})
    A = A.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop})
    self.A_star = A

    B = self.B_star.subs({self.Frame.wbot:self.mass1.wtop, self.Frame.nbot:self.mass1.ntop})
    B = B.subs({self.mass1.wbot:self.mass2.wtop, self.mass1.nbot:self.mass2.ntop})
    B = B.subs({self.mass2.wbot:self.mass3.wtop, self.mass2.nbot:self.mass3.ntop})
    B = B.subs({self.mass3.wbot:self.mass4.wtop, self.mass3.nbot:self.mass4.ntop})
    self.B_star = B

  def ConnectCenter(self):
    A = self.A_star.subs({self.mass1.dbot:0, self.mass1.dtop:0})
    A = A.subs({self.mass2.dbot:0, self.mass2.dtop:0})
    A = A.subs({self.mass3.dbot:0, self.mass3.dtop:0})
    A = A.subs({ self.mass4.dtop:0})
    self. A_star = A

    B = self.B_star.subs({self.mass1.dbot:0, self.mass1.dtop:0})
    B = B.subs({self.mass2.dbot:0, self.mass2.dtop:0})
    B = B.subs({self.mass3.dbot:0, self.mass3.dtop:0})
    B = B.subs({ self.mass4.dtop:0})
    self. B_star = B


  def SS_update(self):
    self. A = Matrix.vstack(Matrix.hstack(zeros(len(self.var),len(self.var)), eye(len(self.var))),Matrix.hstack(self.A_star,zeros(len(self.var),len(self.var))))
    self. B = Matrix.vstack(zeros(len(self.var),len(self.var_input)),self.B_star)

  def BHQS(self):
    BHQSsus = BHQS()
    self.A_star = replace(self.A_star, self, BHQSsus)
    self.B_star = replace(self.B_star, self, BHQSsus)

  def TF(self):
    self.A, self.B, self.C, self.D = np.float64(self.A), np.float64(self.B), np.float64(self.C), np.float64(self.D)
    H = ss2tf(self.A, self.B, self.C, self.D)
    self. H = H

  def get_eq(self):
    self.eq_var = get_eq_pos(self.masses,self.masses['mass1'].dw, self.wires, self.var, self.var_input, self.ReferenceFrame, self.g,self)

  def get_fT(self):
    F = get_forces(self.masses,self.mass1.dw, self.wires, self.var, self.var_input, self.ReferenceFrame, self.g, self)
    self.F = F

  def get_f_num(self):
    Fnum = get_forces_num(self.masses,self.mass1.dw, self.wires, self.var, self.var_input, self.ReferenceFrame, self.g,self)
    self.Fnum = Fnum
  def get_f_num_bis(self):
    Fnum = get_forces_num_bis(self.masses,self.mass1.dw, self.wires, self.var, self.var_input, self.ReferenceFrame, self.g,self)
    self.Fnum = Fnum

  def ComputeWireTensions(self):
    XB = pos(SusParts.masses['mass1'].Bup11,SusParts.masses['mass1'].center_of_mass,SusParts.masses['mass1'].ReferenceFrame )
    caracteristic_length = (XB.dot(XB))**0.5
    setattr(self, 'caracteristic_length', caracteristic_length)
    connect4x4(self.Frame, self.mass1, self.wires, self.ReferenceFrame)
    connect4x4(self.mass1, self.mass2, self.wires, self.ReferenceFrame)
    connect4x4(self.mass2, self.mass3, self.wires, self.ReferenceFrame)
    connect4x4(self.mass3, self.mass4, self.wires, self.ReferenceFrame)
    TensionMatrix = find_TensionMatrix(self.masses, self.wires, self.ReferenceFrame)
    setattr(self, 'TensionMatrix', TensionMatrix)
    Tensions, err_equilibrium = getTensions(self.TensionMatrix, self.masses, self.caracteristic_length)
    setattr(self,'Tensions', Tensions)
    setattr(self,'err_equilibrium', err_equilibrium)
    print('Use .Tensions to acces to the Tensions. Use .TensionMatrix to access to the Tension Matrix')




import pickle
with open('A.pickle', 'rb') as inf:
   A= pickle.loads(inf.read())
import pickle
with open('B.pickle', 'rb') as inf:
   B= pickle.loads(inf.read())

B_real = np.transpose(B)
B_int = B_real[0:6,24:48]


A_int = A[24:48,0:24]


A_star_dic_bs = dict()
B_star_dic_bs = dict()
for j in range(20):
  print('/n /n Matrox set :',j,'/n /n')
  quad = QuadSus(tol=0.0)
  quad.get_equilibrium()

  A_star_dic_bs[f'A_star_{j}'] = np.transpose(np.complex128(quad.A_star)-np.transpose(A_int))
  B_star_dic_bs[f'B_star_{j}'] = np.transpose(np.complex128(quad.B_star)-np.transpose(B_int))


import pickle
with open('A_star_dic_bs5.pickle', 'wb') as outf:
    outf.write(pickle.dumps(A_star_dic_bs))
with open('B_star_dic_bs5.pickle', 'wb') as outf:
    outf.write(pickle.dumps(B_star_dic_bs))







