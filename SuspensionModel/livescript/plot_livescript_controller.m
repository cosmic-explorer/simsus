%HglobFree = feedback(Gglob,double(cavity_logic)*Kopt+double(ASC_logic)*CascP+double(LSC_logic)*ClscM+double(loc_cont_logic)*Cloc+double(ASCY_logic)*CascY);


if ismember(Controller,{'CascP_hard','CascP_soft'}) 
   HglobFree =  feedback(Gglob,double(cavity_logic)*Kopt+double(LSC_logic)*ClscM+double(loc_cont_logic)*Cloc+double(ASCY_logic)*CascY);
elseif ismember(Controller,{'CascY_hard','CascY_soft'})    
   HglobFree = feedback(Gglob,double(cavity_logic)*Kopt+double(ASC_logic)*CascP+double(LSC_logic)*ClscM+double(loc_cont_logic)*Cloc);
elseif ismember(Controller,{'Cloc_L','Cloc_T','Cloc_V','Cloc_R','Cloc_P','Cloc_Y'}) 
    HglobFree = feedback(Gglob,double(cavity_logic)*Kopt+double(ASC_logic)*CascP+double(LSC_logic)*ClscM+double(ASCY_logic)*CascY);
elseif ismember(Controller,{'Clsc'}) 
    HglobFree = feedback(Gglob,double(cavity_logic)*Kopt+double(ASC_logic)*CascP+double(loc_cont_logic)*Cloc+double(ASCY_logic)*CascY);

end









Cloc.OutputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
Cloc.InputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};



CascPsh.InputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};
CascPsh.OutputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','sMP1','sMY1','eFx2','eFy2','eFz2','eMR2','sMP2','sMY2','eFx3','eFy3','eFz3','eMR3','sMP3','sMY3','eFx4','eFy4','eFz4','eMR4','sMP4','sMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','hMP1','hMY1','iFx2','iFy2','iFz2','iMR2','hMP2','hMY2','iFx3','iFy3','iFz3','iMR3','hMP3','hMY3','iFx4','iFy4','iFz4','iMR4','hMP4','hMY4'};

CascYsh.InputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};
CascYsh.OutputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','sMP1','sMY1','eFx2','eFy2','eFz2','eMR2','sMP2','sMY2','eFx3','eFy3','eFz3','eMR3','sMP3','sMY3','eFx4','eFy4','eFz4','eMR4','sMP4','sMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','hMP1','hMY1','iFx2','iFy2','iFz2','iMR2','hMP2','hMY2','iFx3','iFy3','iFz3','iMR3','hMP3','hMY3','iFx4','iFy4','iFz4','iMR4','hMP4','hMY4'};


ClscM.OutputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
ClscM.InputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};

if ismember(Controller,{'Cloc_L','Cloc_T','Cloc_V','Cloc_R','Cloc_P','Cloc_Y'}) 

HglobFree.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
HglobFree.OutputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};

HglobOpenLoop = HglobFree*Cloc;

HglobOpenLoop.InputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};
C_part2 = Cloc(input,output);
Hglob_i_o = Hglob(output,input);
elseif ismember(Controller,{'CascP_hard','CascP_soft','CascY_hard','CascY_soft'}) 

HglobFree = Sp2*HglobFree*Sp1inv;
HglobFree.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','sMP1','sMY1','eFx2','eFy2','eFz2','eMR2','sMP2','sMY2','eFx3','eFy3','eFz3','eMR3','sMP3','sMY3','eFx4','eFy4','eFz4','eMR4','sMP4','sMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','hMP1','hMY1','iFx2','iFy2','iFz2','iMR2','hMP2','hMY2','iFx3','iFy3','iFz3','iMR3','hMP3','hMY3','iFx4','iFy4','iFz4','iMR4','hMP4','hMY4'};

HglobFree.OutputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};
if ismember(Controller,{'CascP_hard','CascP_soft'})
HglobOpenLoop = HglobFree*Sp1*CascP*Sp2inv;
C_part2 = CascPsh(input,output);
elseif ismember(Controller,{'CascY_hard','CascY_soft'})
    HglobOpenLoop = HglobFree*Sp1*CascY*Sp2inv;
    C_part2 = CascYsh(input,output);
end


HglobOpenLoop.InputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};

Hglobfsh = Sp2*Hglob*Sp1inv;
Hglobfsh.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','sMP1','sMY1','eFx2','eFy2','eFz2','eMR2','sMP2','sMY2','eFx3','eFy3','eFz3','eMR3','sMP3','sMY3','eFx4','eFy4','eFz4','eMR4','sMP4','sMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','hMP1','hMY1','iFx2','iFy2','iFz2','iMR2','hMP2','hMY2','iFx3','iFy3','iFz3','iMR3','hMP3','hMY3','iFx4','iFy4','iFz4','iMR4','hMP4','hMY4'};

Hglobfsh.OutputName = {'sx1','sy1','sz1','sR1','sP1','sY1','sx2','sy2','sz2','sR2','sP2','sY2','sx3','sy3','sz3','sR3','sP3','sY3','sx4','sy4','sz4','sR4','sP4','sY4','hx1','hy1','hz1','hR1','hP1','hY1','hx2','hy2','hz2','hR2','hP2','hY2','hx3','hy3','hz3','hR3','hP3','hY3','hx4','hy4','hz4','hR4','hP4','hY4'};



Hglob_i_o = Hglobfsh(output,input);
elseif ismember(Controller,{'Clsc'}) 
HglobFree.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
HglobFree.OutputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};

HglobOpenLoop = HglobFree*ClscM;

HglobOpenLoop.InputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};
C_part2 = ClscM(input,output);
Hglob_i_o = Hglob(output,input);
end




HglobFree_i_o = HglobFree(output,input);
HglobOpenLoop_i_o = HglobOpenLoop(output,output);
