% Define filter specifications
N = 2;  % Filter order

% Define cutoff frequencies for the filters
cutoff_low = 0.1*2*pi;      % Low-pass cutoff frequency (Hz)
cutoff_band = [0.1*2*pi 6*2*pi]; % Band-pass cutoff frequencies (Hz)
cutoff_high = 0.1*2*pi;    % High-pass cutoff frequency (Hz)

% Design low-pass filter using Butterworth
[b_lp, a_lp] = butter(N, cutoff_low, 'low', 's');

% Design high-pass filter using Butterworth
[b_hp, a_hp] = butter(N, cutoff_high, 'high', 's');

% Design band-pass filter using Butterworth
[b_bp, a_bp] = butter(N, cutoff_band, 'bandpass', 's');

% Create transfer function objects
LPF = tf(b_lp, a_lp);
HPF = tf(b_hp, a_hp);
BPF = tf(b_bp, a_bp);
