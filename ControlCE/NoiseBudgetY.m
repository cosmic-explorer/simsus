function S_P = NoiseBudgetY(Hglob)

y0e = zeros(60,1);
y0e(6) = 1;

y0i = zeros(60,1);
y0i(36) = 1;

%%particular TF

%Effect on i
Hi = Hglob*y0i;
He = Hglob*y0e;
Hii = minreal(Hi(24),[],false);
Hei = minreal(He(24),[],false);

n1i = zeros(48,1);
n1i(6) = 1;


n1e = zeros(48,1);
n1e(30) = 1;


Hn = Hglob*Cloc;

Hn1i = Hn*n1i;
Hn1e = Hn*n1e;

Hn1ii = minreal(Hn1i(24),[],false);
Hn1ei = minreal(Hn1e(24),[],false);


n2i = zeros(48,1);
n2i(12) = 1;


n2e = zeros(48,1);
n2e(36) = 1;

Hn2i = Hn*n2i;
Hn2e = Hn*n2e;

Hn2ii = minreal(Hn2i(24),[],false);
Hn2ei = minreal(Hn2e(24),[],false);

%effect on e


Hie = minreal(Hi(48),[],false);
Hee = minreal(He(48),[],false);

Hn1ie = minreal(Hn1i(48),[],false);
Hn1ee = minreal(Hn1e(48),[],false);

Hn2ie = minreal(Hn2i(48),[],false);
Hn2ee = minreal(Hn2e(48),[],false);

S_theta_i = sqrt(multiply(Hii,w,d_ISIY).^2+multiply(Hei,w,d_ISIY).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn1ei,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hn2ei,w,N_shot).^2);
S_theta_e = sqrt(multiply(Hie,w,d_ISIY).^2+multiply(Hee,w,d_ISIY).^2+multiply(Hn1ie,w,N_shot).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ie,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2);

theta_i_RMS = RMS(S_theta_i);
theta_e_RMS = RMS(S_theta_e);

d_i_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_RMS^2+theta_i_RMS^2);
d_e_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_RMS^2+bhqs.gi^2*theta_e_RMS^2);

S_P = sqrt(2*(d_i_RMS^2*S_theta_i.^2+d_e_RMS^2*S_theta_e.^2));



end

