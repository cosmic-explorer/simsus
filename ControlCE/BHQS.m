classdef BHQS
    properties
        R;
        Spitch;
        Syaw;
        Kopt;

        M1 = 117;
        M2 = 83;
        M3 = 100;
        M4 = 100;
        g = 9.81;
        
        Ixx1 = 8.1;
        Ixx2 = 3.6;
        Ixx3 = 2.6;
        Ixx4 = 2.6;
        
        Iyy1 = 8.1;
        Iyy2 = 3.3;
        Iyy3 = 1.9;
        Iyy4 = 1.9;
        
        Izz1 = 15.5;
        Izz2 = 6.7;
        Izz3 = 1.9;
        Izz4 = 1.9;
        
        L1 = 0.34;
        L2 = 0.34;
        L3 = 0.34;
        L4 = 0.60;
        
        K1;
        K2;
        K3;
        K4;
        
        d0 = 0;
        d1 = 0;
        d2 = 0;
        d3 = 0;
        d4 = 0;
        d5 = 0;
        d6 = 0;
        d7 = 0;
        
        w0 = 16e-2;
        w1 = 16e-2;
        w2 = 14e-2;
        w3 = 14e-2;
        w4 = 12.5e-2;
        w5 = 12.5e-2;
        w6 = 2.5e-2;
        w7 = 2.5e-2;
        
        n0 = 18e-2;
        n1 = 18e-2;
        n2 = 13.5e-2;
        n3 = 13.5e-2;
        n4 = 23e-2;
        n5 = 23e-2;
        n6 = 23e-2;
        n7 = 23e-2;
        
        L01;
        L02;
        L03;
        L04;
        Y4 = 72e9;
        r4 = 220e-6;




        ge =- 0.7795;
        gi = -1.0657;

        ks = -20;%-10.57; %N.m
        kh = 456;%232.96; %N.m
        Lcav = 3994.5;
 

    end
    
    methods
        function obj = BHQS()
            obj.K1 = 7.2e3 / 2;
            obj.K2 = 6.5e3 / 2;
            obj.K3 = 3.6e3 / 2;
            
            obj.K4 = obj.Y4 * pi * obj.r4^2 / obj.L4 - obj.M4 * obj.g / 4 / obj.L4;
            
            obj.L01 = obj.L1 - (obj.M1 + obj.M2 + obj.M3 + obj.M4) * obj.g / 4 / obj.K1;
            obj.L02 = obj.L2 - (obj.M2 + obj.M3 + obj.M4) * obj.g / 4 / obj.K2;
            obj.L03 = obj.L3 - (obj.M3 + obj.M4) * obj.g / 4 / obj.K3;
            obj.L04 = obj.L4 - obj.M4 * obj.g / 4 / obj.K4;
            obj.R = obj.getr();
            obj.Spitch = [[1 obj.R];[obj.R -1]];
            obj.Syaw = [[1 obj.R];[obj.R -1]];
            obj.Kopt = [[obj.ks 0]; [0 obj.kh]];
        end
        function R = getr(obj)
            R = r(obj.gi,obj.ge);
        end
    end
end