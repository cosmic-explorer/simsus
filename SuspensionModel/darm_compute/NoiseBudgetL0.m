%function S_L0 = NoiseBudgetL0(Hglob)
Kglob = feedback(Gglob,Kopt+Cloc+CascP);
L0e = zeros(60,1);
L0e(1) = 1;

L0i = zeros(60,1);
L0i(31) = 1;

%%particular TF

%Effect on i
Hi = Kglob*L0i;% M?
He = Kglob*L0e;
Hii = minreal(zpk(Hi(43)),[],false);
%Hei = minreal(He(23),[],false);

n1e = zeros(48,1);
n1e(1) = 1;


n1i = zeros(48,1);
n1i(25) = 1;


Hn = Kglob*Cloc;

Hn1i = Hn*n1i;
Hn1e = Hn*n1e;

Hn1ii = minreal(zpk(Hn1i(43)),1e-12,false);
%Hn1ei = minreal(Hn1e(23),[],false);


n2i = zeros(48,1);
n2i(7) = 1;


n2e = zeros(48,1);
n2e(31) = 1;

Hn2i = Hn*n2i;
Hn2e = Hn*n2e;
Hn2ii = minreal(Hn2i(43),[],false);
%Hn2ei = minreal(Hn2e(23),[],false);

%effect on e


%Hie = minreal(Hi(47),[],false);
Hee = minreal(zpk(He(43-24)),[],false);

%Hn1ie = minreal(Hn1i(47),[],false);
Hn1ee = minreal(zpk(Hn1e(43-24)),1e-12,false);

%Hn2ie = minreal(Hn2i(47),[],false);
Hn2ee = minreal(Hn2e(43-24),[],false);

S_Lisii =  multiply(Hii,w,d_ISIL);
S_Lisie =  multiply(Hee,w,d_ISIL);
S_Lni = multiply(Hn1ii,w,N_shot);
S_Lne = multiply(Hn1ee,w,N_shot);


S_isi = multiply(Hii,w,d_ISIL);
S_osem = multiply(Hn1ii,w,N_shot);
S_dL = multiply(Hii,w,d_ISIL).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hee,w,d_ISIL).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2;
%S_L_e2 = multiply(Hee,w,d_ISIL).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2;
S_L0i = sqrt(multiply(Hii,w,d_ISIL).^2+multiply(Hn1ii,w,N_shot).^2);
S_L0e = sqrt(multiply(Hee,w,d_ISIL).^2+multiply(Hn1ee,w,N_shot).^2);
%S_L0=sqrt(2)*S_isi;
%S_L0 = sqrt(2*(S_L0i.^2+S_L0e.^2));
S_L0 = sqrt(S_L0i.^2+S_L0e.^2);
%S_L0 = sqrt(S_dL);
%end



%Iglob = M*Hglob;
%Iglob*