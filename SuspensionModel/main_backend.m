%run('functions_utilities\load_workspace.m')
addpath A_plus\;
addpath BHQS\;
addpath complementary_filter\;
addpath controller\;
addpath darm_compute\;
addpath functions_utilities\;
addpath inputs\;
addpath SS_BHQS\;
addpath variables\;
addpath livescript\;
run('ISI_inputs.m')
run('Noise_inputs.m')
%Execute Noise_inputs.m
run('freq.m')
run('load_inputs.m')

%%%MODEL TYPE

if strcmp('A_plus',modelname)
load('A_plus/G_Aplus.mat','G_Aplus')
G = G_Aplus;
model = APLUS(lv_Pcavity,0,0,0,0);

elseif strcmp('BHQS',modelname)

    M10 = 117;
    M20 = 83;
    M30 = 100;
    M40 = 100;

            model = BHQS(lv_Pcavity,lv_Iyy1,lv_Iyy2,lv_Iyy3,lv_Iyy4);

        


    if strcmp('perfect',tolerance)
        

        [A,B,C,D]=get_SSBHQS_M(model,M10,M20,300-M10-M20);
        G = minreal(ss(A,B,C,D),[],false);
    
    else
        [A,B,C,D]=get_SSBHQS_M(model,M10,M20,300-M10-M20);
        if strcmp("tol_ap01",tolerance)
       % save('variables\bhqs.mat','bhqs');

        load('SS_BHQS\A_star_dic_ap01.mat')
        load('SS_BHQS\B_star_dic_ap01.mat')

        elseif strcmp("tol_ap05",tolerance)
        %save('variables\bhqs.mat','bhqs');

        load('SS_BHQS\A_star_dic_ap05.mat')
        load('SS_BHQS\B_star_dic_ap05.mat')
        elseif strcmp("tol_ap1",tolerance)
        %save('variables\bhqs.mat','bhqs');

        load('SS_BHQS\A_star_dic_ap1.mat')
        load('SS_BHQS\B_star_dic_ap1.mat')
        end
Gperfect = minreal(ss(A,B,C,D),[],false);
A_res = A_star_dic_ap{1,1};
B_res = B_star_dic_ap{1,1};

fields_A = fieldnames(A_res);
fields_B = fieldnames(B_res);


    fieldName_A = fields_A{1}; %%%TAKE THE FIRST non perfect suspension generated
    A_res_i = A_res.(fieldName_A);
    fieldName_B = fields_B{1};
    B_res_i = B_res.(fieldName_B);
    A_res_full = zeros(48, 48);
    A_res_full(25:48, 1:24) = A_res_i;
    B_res_full = zeros(48, 30);% Lower left block of A is A_star_BHQS
    B_res_full(25:30,1:6) = B_res_i(1:6,1:6).';
    %B_star(1:6,1:6)
    [A,B,C,D]=get_SSBHQS_M(model,M10,M20,300-M10-M20);
    G = minreal(ss(A+A_res_full,B+B_res_full,C,D),[],false);
        
        
       % G = minreal(ss(A,B,C,D),[],false);
    
    end
    
end

G.InputName={'x0','y0','z0','R0','P0','Y0','Fx1','Fy1','Fz1','MR1','MP1','MY1','Fx2','Fy2','Fz2','MR2','MP2','MY2','Fx3','Fy3','Fz3','MR3','MP3','MY3','Fx4','Fy4','Fz4','MR4','MP4','MY4'};
G.OutputName = {'x1','y1','z1','R1','P1','Y1','x2','y2','z2','R2','P2','Y2','x3','y3','z3','R3','P3','Y3','x4','y4','z4','R4','P4','Y4'};

Gglob = [[G,zeros(24,30)];[zeros(24,30),G]];
Gglob.InputName={'ex0','ey0','ez0','eR0','eP0','eY0','eFx1','eFy1','eFz1','eMR1','eMP1','eMY1','eFx2','eFy2','eFz2','eMR2','eMP2','eMY2','eFx3','eFy3','eFz3','eMR3','eMP3','eMY3','eFx4','eFy4','eFz4','eMR4','eMP4','eMY4','ix0','iy0','iz0','iR0','iP0','iY0','iFx1','iFy1','iFz1','iMR1','iMP1','iMY1','iFx2','iFy2','iFz2','iMR2','iMP2','iMY2','iFx3','iFy3','iFz3','iMR3','iMP3','iMY3','iFx4','iFy4','iFz4','iMR4','iMP4','iMY4'};
Gglob.OutputName = {'ex1','ey1','ez1','eR1','eP1','eY1','ex2','ey2','ez2','eR2','eP2','eY2','ex3','ey3','ez3','eR3','eP3','eY3','ex4','ey4','ez4','eR4','eP4','eY4','ix1','iy1','iz1','iR1','iP1','iY1','ix2','iy2','iz2','iR2','iP2','iY2','ix3','iy3','iz3','iR3','iP3','iY3','ix4','iy4','iz4','iR4','iP4','iY4'};



Kopt_mini = inv(model.Spitch)*[[model.ks,0];[0,model.kh]]*model.Spitch; 
Kopt(29,23) = Kopt_mini(1,1);
Kopt(29,47) = Kopt_mini(1,2);
Kopt(59,23) = Kopt_mini(2,1);
Kopt(59,47) = Kopt_mini(2,2);

%%YAW
Kopt_miniY = inv(model.Syaw)*[[model.ks,0];[0,model.kh]]*model.Syaw; 
Kopt(30,24) = Kopt_miniY(1,1);%/bhqs.Izz4;
Kopt(30,48) = Kopt_miniY(1,2);%/bhqs.Izz4;
Kopt(60,24) = Kopt_miniY(2,1);%/bhqs.Izz4;
Kopt(60,48) = Kopt_miniY(2,2);%/bhqs.Izz4;

run('getSp_livescript.m');

%%%% LOCAL CONTROLLER

if strcmp('no_loc_controller',loc_controller)

Cloc = ss(zeros(60,48));

elseif strcmp('loc_vermeer',loc_controller)
%load(...)
if strcmp('BHQS',modelname)
    load('ClocVermeerBHQS.mat')
elseif strcmp('A_plus',modelname)
    load('ClocVermeerAplus.mat')
end
elseif strcmp('custom',loc_controller)

Cloc = get_Cloc(CL,CT,CV,CR,CP,CY);

end


%%%% ASCP CONTROLLER

if strcmp('no_asc_controller',ASC_controller)

CascP = ss(zeros(60,48));
CascPsh = ss(zeros(60,48));
elseif strcmp('asc_vermeer',ASC_controller)

if strcmp('BHQS',modelname)
    load('CascPVermeerBHQS.mat')
    CascPsh = Sp1*CascP*Sp2inv;
    ch = CascPsh(59,47);
    cs = CascPsh(59,47);
elseif strcmp('A_plus',modelname)
    load('CascPVermeerAplus.mat')
    CascPsh = Sp1*CascP*Sp2inv;
    ch = CascPsh(59,47);
    cs = CascPsh(59,47);
end
elseif strcmp('custom',ASC_controller)

CascP = get_CascP(Ch,Cs);
CascPsh = Sp1*CascP*Sp2inv;
ch = CascPsh(59,47);
cs = CascPsh(29,23);

end


%%%% ASC Y CONTROLLER

if strcmp('no_ascY_controller',ASCY_controller)

CascY = ss(zeros(60,48));
CascYsh = ss(zeros(60,48));
elseif strcmp('custom',ASCY_controller)

CascY = get_CascY(ChY,CsY,model);
CascYsh = Sp1*CascY*Sp2inv;
chY = CascYsh(60,48);
csY = CascYsh(30,24);
elseif strcmp('ascY_vermeer',ASCY_controller)

if strcmp('BHQS',modelname)
    load('CascYVermeerBHQS.mat')
    CascYsh = Sp1*CascY*Sp2inv;
    chY = CascPsh(60,48);
    csY = CascPsh(60,48);
elseif strcmp('A_plus',modelname) %%%% TO DO
    CascY = ss(zeros(60,48));
CascYsh = ss(zeros(60,48));
end
end


%%%% LSC CONTROLLER
if strcmp('no_lsc_controller',LSC_controller)

ClscM = ss(zeros(60,48));

elseif strcmp('lsc_vermeer',LSC_controller)
%load(...)
if strcmp('BHQS',modelname)
    load('ClscMVermeerBHQS.mat')
elseif strcmp('A_plus',modelname)
    load('ClscMVermeerAplus.mat')
end
elseif strcmp('custom',LSC_controller)

ClscM = get_Clsc(CLSC);

end

[~,~,Sp1red,Sp2red,Sp2invred,Sp1invred,Sp1,Sp1inv,Sp2,Sp2inv,I48,Dd] = some_var_fun(model,Gglob,Cloc);

Hglob = feedback(Gglob,Cloc+double(cavity_status)*Kopt+ClscM+CascP+CascY);




%disp(['The suspension generated is ',)



