function[W]=generateWF(args)
 %createWeight
%
 %Syntax:[W] =generateWeight(args)
 %
 %Inputs:
 %-n-WeightOrder(integer)
 %-G0-LowfrequencyGain
 %-G1-HighfrequencyGain
 %-Gc-Gainoftheweightatfrequencyw0
 %-w0-Frequencyatwhich|W(jw0)|=Gc[rad/s]
 %
 %Outputs:
 %-W-GeneratedWeightingFunction

%%Argumentvalidation
 arguments
 args.n (1,1) double {mustBeInteger,mustBePositive}=1
 args.G0 (1,1)double{mustBeNumeric,mustBePositive}=0.1
 args.Ginf (1,1)double{mustBeNumeric,mustBePositive}=10
 args.Gc (1,1)double{mustBeNumeric,mustBePositive}=1
 args.w0 (1,1)double{mustBeNumeric,mustBePositive}=1
 end
 %VerificationofcorrectrelationbetweenG0,GcandGinf
 mustBeBetween(args.G0,args.Gc,args.Ginf);
 %%Initialize theLaplacevariable
 s=zpk('s');
 %%Createthe weightingfunctionaccordingtoformula
 W=(((1/args.w0)*sqrt((1-(args.G0/args.Gc)^(2/args.n))/(1-(args.Gc/args.Ginf)^(2/args.n)))*s+...
 (args.G0/args.Gc)^(1/args.n))/...
 ((1/args.Ginf)^(1/args.n)*(1/args.w0)*sqrt((1-(args.G0/args.Gc)^(2/args.n))/(1-(args.Gc/args.Ginf)^(2/args.n)))*s+...
 (1/args.Gc)^(1/args.n)))^args.n;
 %%Customvalidationfunction
function mustBeBetween(a,b,c)
 if~((a> b&&b>c)||(c>b&&b>a))
 eid='createWeight:inputError';
 msg='GcshouldbebetweenG0andGinf.';
 throwAsCaller(MException(eid,msg))
 end