%function S_P = NoiseBudgetP(Hglob)
%run('Clocal.m');
%tic
p0i = zeros(60,1);
p0i(5) = 1;

p0e = zeros(60,1);
p0e(35) = 1;

%%particular TF

%Effect on i
tic
Hi = Hglob*p0i;
He = Hglob*p0e;
Hii = minreal(Hi(23),1e-12,false);
Hei = minreal(He(23),1e-12,false);
toc
n1i = zeros(48,1);
n1i(5) = 1;


n1e = zeros(48,1);
n1e(29) = 1;


Hn = Hglob*Cloc;

nASCs = zeros(48,1);
nASCs(23) = 1;
nASCh = zeros(48,1);
nASCh(47) = 1;
tic
HnASC = Hglob*CascP*Sp2inv;

HnASCs = HnASC*nASCs;
toc
%HnASCsi = minreal(HnASC(23,23),[],false);

%HnASCse = minreal(HnASC(47,23),[],false);
%HnASChi = minreal(HnASC(23,47),[],false);
%HnASChe = minreal(HnASC(47,47),[],false);
tic
HnASCsi = minreal(HnASCs(23),[],false);

HnASCse = minreal(HnASCs(47),[],false);

HnASCh = HnASC*nASCh;
toc

tic
HnASChi = minreal(HnASCh(23),[],false);
HnASChe = minreal(HnASCh(47),[],false);
toc
Hn1i = Hn*n1i;

Hn1e = Hn*n1e;

 
tic
Hn1ii = minreal(Hn1i(23),1e-12,false);%,[],false);
Hn1ei = minreal(Hn1e(23),1e-12,false);%,[],false);
toc

n2i = zeros(48,1);
n2i(11) = 1;


n2e = zeros(48,1);
n2e(41) = 1;

Hn2i = Hn*n2i;
Hn2e = Hn*n2e;
tic
Hn2ii = minreal(Hn2i(23),[],false);
Hn2ei = minreal(Hn2e(23),[],false);
toc
%effect on e

tic
Hie = minreal(Hi(47),1e-12,false);
Hee = minreal(He(47),1e-12,false);

Hn1ie = minreal(Hn1i(47),1e-12,false);%,[],false);
Hn1ee = minreal(Hn1e(47),1e-12,false);%,[],false);

Hn2ie = minreal(Hn2i(47),[],false);
Hn2ee = minreal(Hn2e(47),[],false);
toc
%tic
%S_Pisii=multiply(Hii,w,d_ISIP);

%S_Pisei=multiply(Hei,w,d_ISIP);

%S_Pnii=multiply(Hn1ii,w,N_shot);

%S_Pnei=multiply(Hn1ei,w,N_shot);


%S_Pascsi=multiply(HnASCsi,w,N_ASC);
%S_Paschi=multiply(HnASChi,w,N_ASC);

%S_Pascse=multiply(HnASCse,w,N_ASC);
%S_Pasche=multiply(HnASChe,w,N_ASC);
%toc
tic
S_theta_i = sqrt(multiply(Hii,w,d_ISIP).^2+multiply(Hei,w,d_ISIP).^2+multiply(Hn1ii,w,N_shot).^2+multiply(Hn1ei,w,N_shot).^2+multiply(Hn2ii,w,N_shot).^2+multiply(Hn2ei,w,N_shot).^2+multiply(HnASCsi,w,N_ASC).^2+multiply(HnASChi,w,N_ASC).^2);



S_theta_e = sqrt(multiply(Hie,w,d_ISIP).^2+multiply(Hee,w,d_ISIP).^2+multiply(Hn1ie,w,N_shot).^2+multiply(Hn1ee,w,N_shot).^2+multiply(Hn2ie,w,N_shot).^2+multiply(Hn2ee,w,N_shot).^2+multiply(HnASCse,w,N_ASC).^2+multiply(HnASChe,w,N_ASC).^2);
toc
tic
theta_i_RMS = RMS(S_theta_i,f,0.001,300);
theta_e_RMS = RMS(S_theta_e,f,0.001,300);
toc
tic
d_i_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(bhqs.ge^2*theta_e_RMS^2+theta_i_RMS^2)+0.001;
d_e_RMS = abs(bhqs.Lcav/(1-bhqs.ge*bhqs.gi))*sqrt(theta_i_RMS^2+bhqs.gi^2*theta_e_RMS^2)+0.001;

S_P = sqrt(2*(d_i_RMS^2*S_theta_i.^2+d_e_RMS^2*S_theta_e.^2));
toc
%toc

%a=a;

%end

